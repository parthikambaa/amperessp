package com.ampere.bike.ampere.views.fragment.enquiry;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.RetrofitService;
import com.ampere.bike.ampere.model.enquiry.ModelLeadsInfo;
import com.ampere.bike.ampere.model.enquiry.ModelSalesInsert;
import com.ampere.bike.ampere.model.enquiry.cityBasedOnState.ModelCityBasedOnState;
import com.ampere.bike.ampere.model.enquiry.spares.ModelVehicleModelBaseSpares;
import com.ampere.bike.ampere.model.enquiry.spares.SparesGetter;
import com.ampere.bike.ampere.model.enquiry.spares.addMoreSpare.AddedMoreSpares;
import com.ampere.bike.ampere.model.enquiry.task.ModelTaskList;
import com.ampere.bike.ampere.model.enquiry.task.Tasklist;
import com.ampere.bike.ampere.model.leads.ModelAssign;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.CallLogDate;
import com.ampere.bike.ampere.utils.DateUtils;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.utils.NetworkTester;
import com.ampere.bike.ampere.views.fragment.enquiry.calllog.CallLogActivity;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity;
import com.ampere.vehicles.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import customviews.CustomButton;
import customviews.CustomEditText;
import customviews.CustomTextEditView;
import customviews.CustomTextView;
import modifyviews.MyTextView_Lato_Bold;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.ampere.bike.ampere.shared.LocalSession.BEARER;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_NAME;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_USER_NAME;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_USER_TYPE;
import static com.ampere.bike.ampere.shared.LocalSession.T_CORPORATE_SALES;
import static com.ampere.bike.ampere.shared.LocalSession.T_CORPORATE_SERVICE;
import static com.ampere.bike.ampere.shared.LocalSession.T_CRM;
import static com.ampere.bike.ampere.shared.LocalSession.T_DEALER;
import static com.ampere.bike.ampere.utils.CallLogDate.UNKNOWN;
import static com.ampere.bike.ampere.utils.MessageUtils.showErrorCodeToast;
import static com.ampere.bike.ampere.utils.MessageUtils.showSnackBar;
import static com.ampere.bike.ampere.utils.MyUtils.isSelect;


public class Enquiry extends Fragment implements View.OnClickListener {

    private ArrayList<AddedMoreSpares> addedMoreSpares = new ArrayList<>();
    public static final int LOCATION_PERMISSION_CODE = 23;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private CustomButton mSubmitBtn, mCancelBtn;
    private CustomTextEditView mName, mEmail, mCity, mMobileNo, mAddress, mMessage, mChassisNo, mSparesEdt, mEdtSelfDate, mEdtSelfTime, mWarrantEdt, mValues, mServiceIssue;
    private TextInputLayout mVechileInputLayout, mDatePicInputLayout, mMeassageInputlayout;
    private RelativeLayout mWarrantIPL;
    private DatePickerDialog datePickerDialog;
    private CustomTextEditView mDate, mTime, mDateTwo, mEdttask, mTimeTwo;
    private ImageView mCallLog;
    private View mSnackView;
    private int year, month, day;
    private CustomTextEditView mVehicleModel;
    private ArrayList<String> mCarList;
    private RadioGroup mRgEnquiryFor, mRgAssigned;
    private String selection = "";
    private RadioButton rb1, rb2, rb3;
    private RetrofitService retrofitService;
    private Dialog dialog;
    private String patternStr = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private Spinner mSpLeadSource, mSpVehicleModel, mSpAssignedTask, mSpSpares, mSpAssignedTaskCD, mEdtSelfTaskSp, mServiceIssueSp, mServiceTypeSp;
    private RelativeLayout mServiceIssueLay;
    private LinearLayout mLLCD, mLLCustomType, mLLAssignedTo, mLlBelowMsg, mLLCommon;
    private RelativeLayout mLLVicheleModel/*, mLLSpares*/;
    private String loginUserDes = "all";
    private RadioButton radioDealer, radioCorporate;
    private Snackbar snackbar;
    ArrayList<String> taskArray = new ArrayList<>();
    ArrayList<String> cityArray = new ArrayList<>();
    ArrayList<String> statesArray = new ArrayList<>();
    private ScrollView mEnquiryScroll;
    private LinearLayout mNoDataLayout, mBottom;
    private CustomTextView mNoDataAPI;
    private AutoCompleteTextView mCityAuto, mAutoTask;
    private boolean doubleBackToExitPressedOnce = false;
    private String noLeadSource = "Select Lead Source", noVehileModel = "Select Vehicle Model", noAssignedto = "Select Assign to", noTask = "Select Task", noServiceIssue = "No Service Issue Found";
    //for Dealer
    private ArrayList<ModelAssign.Data.Dealers> dealers = new ArrayList<>();
    //for Corporate
    private ArrayList<ModelAssign.Data.Corporate> corporates = new ArrayList<>();
    private String selectAssign, assigned = "";
    private ModelAssign modelVehicleModel;
    private ModelVehicleModelBaseSpares modelVehicleModelBaseSpares;
    private ArrayList<SparesGetter> sparesGetters = new ArrayList<>();
    String sparesId = "", sparesName, modelName, assignedId;
    private Spinner mSpinnerPinCode, mSpState;

    private AppCompatCheckBox mCheckSelfAssign;
    private LinearLayout mSelfLayout, mLLSpareAll, mLLServiceAndSapre, mLLAddMoreSpare, mSparesValidate;
    private CustomTextEditView mTxtSelfTask, mState, mSpareQty, mVehicleColor, mAddress2, mLocality;
    private String mSelfTask;
    private CustomEditText mPincode, mLeadSourceEdt;
    private TextInputLayout mIPLPinCode;
    private ImageView mAddOrRemove;
    ArrayList<String> spareList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(getString(R.string.enquiry));

        return inflater.inflate(R.layout.activity_enquiry, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        retrofitService = MyUtils.getInstance();

        mCarList = new ArrayList<>();
        sparesGetters.clear();
        mLLCommon = view.findViewById(R.id.common_lays);
        mLLSpareAll = view.findViewById(R.id.ll_spares_all);
        mSpareQty = view.findViewById(R.id.edt_spares_qty);
        mLLServiceAndSapre = view.findViewById(R.id.service_spare);
        mLLServiceAndSapre.setVisibility(View.GONE);
        mVehicleColor = view.findViewById(R.id.apnt_edt_vechile_color);
        mLLCommon.setVisibility(View.VISIBLE);
        mSubmitBtn = view.findViewById(R.id.apnt_done);
        mCancelBtn = view.findViewById(R.id.apnt_cancel);
        mName = view.findViewById(R.id.apnt_name);
        mEmail = view.findViewById(R.id.apnt_email);
        mMobileNo = view.findViewById(R.id.apnt_mobile_number);
        mAddress = view.findViewById(R.id.apnt_address);
        mAddress2 = view.findViewById(R.id.apnt_address_2);
        mLocality = view.findViewById(R.id.apnt_locallity);
        mDate = view.findViewById(R.id.apnt_date);
        mTime = view.findViewById(R.id.apnt_time);
        mTimeTwo = view.findViewById(R.id.apnt_time_two);
        mCity = view.findViewById(R.id.apnt_city);
        mCityAuto = view.findViewById(R.id.txt_auto_complet);
        mMessage = view.findViewById(R.id.apnt_message);
        mSnackView = view.findViewById(R.id.activity_main);
        mState = view.findViewById(R.id.apnt_edt_state);
        mSpState = view.findViewById(R.id.sp_state);

        mLeadSourceEdt = view.findViewById(R.id.edt_lead_source);
        mCallLog = view.findViewById(R.id.call_log);
        mSpinnerPinCode = view.findViewById(R.id.spinnerpincode);
        mPincode = view.findViewById(R.id.pincode);
        mIPLPinCode = view.findViewById(R.id.pincode_imp);
        mAddOrRemove = view.findViewById(R.id.add_remove);
        mLLAddMoreSpare = view.findViewById(R.id.l_add_item);
        mLLAddMoreSpare.setVisibility(View.GONE);
        mChassisNo = view.findViewById(R.id.apnt_chassis_no);
        mVechileInputLayout = view.findViewById(R.id.apnt_vechile_no_input_layout);

        mValues = view.findViewById(R.id.apnt_edt_values);
        mWarrantIPL = view.findViewById(R.id.apnt_warrent_details);
        mWarrantEdt = view.findViewById(R.id.apnt_edt_warrent_details);
        mRgEnquiryFor = view.findViewById(R.id.radioGroup);
        rb1 = view.findViewById(R.id.rb1);
        rb2 = view.findViewById(R.id.rb2);
        rb3 = view.findViewById(R.id.rb3);

        mDatePicInputLayout = view.findViewById(R.id.txt_date_pic_two);
        mMeassageInputlayout = view.findViewById(R.id.message_inputlayout);
        mVehicleModel = view.findViewById(R.id.apnt_txt_vehicle_model);
        mSpLeadSource = view.findViewById(R.id.sp_lead_source);
        mSpVehicleModel = view.findViewById(R.id.sp_vehicle_model);
        mSpAssignedTask = view.findViewById(R.id.sp_assgined_task);
        mSpAssignedTaskCD = view.findViewById(R.id.sp_assgined_to_d_c);
        mSpSpares = view.findViewById(R.id.sp_spares_for);
        mSparesValidate = view.findViewById(R.id.check_spares_count);
        mSparesValidate.setVisibility(View.GONE);
        mSparesEdt = view.findViewById(R.id.apnt_edt_spares);

        mCheckSelfAssign = view.findViewById(R.id.assign_check);
        mSelfLayout = view.findViewById(R.id.self_assing_layout);
        mTxtSelfTask = view.findViewById(R.id.edt_self_task);
        mEdtSelfDate = view.findViewById(R.id.apnt_self_date);
        mEdtSelfTime = view.findViewById(R.id.apnt_self_time);
        mEdtSelfTaskSp = view.findViewById(R.id.sp_self_assgined_task);

        mServiceIssueLay = view.findViewById(R.id.apnt_service_issue_lay);
        mServiceIssue = view.findViewById(R.id.apnt_edt_service_issue);
        mServiceIssueSp = view.findViewById(R.id.apnt_service_issue_sp);
        mServiceTypeSp = view.findViewById(R.id.apnt_service_type);

        Log.d("Who_loged_in", "" + LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE));

      /*  if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CORPORATE_SALES) || LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CORPORATE_SERVICE)) {
            mCheckSelfAssign.setVisibility(View.VISIBLE);
            mSelfLayout.setVisibility(View.GONE);
        } else if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_DEALER)) {

            mCheckSelfAssign.setVisibility(View.GONE);
            mSelfLayout.setVisibility(View.GONE);
        } else {
            mCheckSelfAssign.setVisibility(View.GONE);
            mSelfLayout.setVisibility(View.GONE);
        }*/


        mEnquiryScroll = view.findViewById(R.id.enquiry_scroll);
        mNoDataLayout = view.findViewById(R.id.lay_no_data);
        mBottom = view.findViewById(R.id.mBottom);
        mNoDataAPI = view.findViewById(R.id.mNoDataAPI);

        radioDealer = view.findViewById(R.id.dealer);
        radioCorporate = view.findViewById(R.id.corporate);
        mRgAssigned = view.findViewById(R.id.assigned);
        mDateTwo = view.findViewById(R.id.apnt_date_for_what);

    /*    mLLSpares = view.findViewById(R.id.ll_spares);
        mLLSpares.setVisibility(View.GONE);*/

        mLlBelowMsg = view.findViewById(R.id.ll_below_msg);
        mLlBelowMsg.setVisibility(View.GONE);

        mLLCD = view.findViewById(R.id.ll_CD_spinner);
        mLLCD.setVisibility(View.GONE);

        mLLVicheleModel = view.findViewById(R.id.ll_vechile_model);
        mLLCustomType = view.findViewById(R.id.ll_custom_type);
        mLLAssignedTo = view.findViewById(R.id.ll_assigned_to);


        mEdttask = view.findViewById(R.id.edt_task);
        mAutoTask = view.findViewById(R.id.auto_task);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, cityArray);

        mCityAuto.setThreshold(1);//will start working from first character
        mCityAuto.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
        mCityAuto.setTextColor(getActivity().getResources().getColor(R.color.black));

        mDateTwo.setText("" + DateUtils.getCurrentDate(DateUtils.DD_MM_YYYY));

        mAutoTask.setThreshold(1);//will start working from first character
        // mAutoTask.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, taskArray));//setting the adapter data into the AutoCompleteTextView

        if (NetworkTester.isNetworkAvailable(getActivity())) {
            dialog = MessageUtils.showDialog(getActivity());
            //MessageUtils.dismissDialog(dialog);
            onLoadLeadsDetails(retrofitService);


           /* //{"user_name":"dealer1@ampere.com","enquiry_id":"46"}
            Intent intent = new Intent(getActivity(), CRMDetailsActivity.class);
            //46 sales
            //12 service
            //13 spares
            intent.putExtra("id", "" + 12);
            intent.putExtra("user_name", "" + LocalSession.getUserInfo(getActivity(), KEY_USER_NAME));
            intent.putExtra("task_name", "Spares");
            getActivity().startActivity(intent);*/

            //new LoadCars().execute();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setCancelable(false);
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Internet is not enabled in your phone. Do you want to goto settings menu?");
            alertDialog.setPositiveButton("Enable",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(intent);
                        }
                    });
            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            getActivity().finish();

                        }
                    });
            alertDialog.show();
        }

        mCallLog.setOnClickListener(this);
        mDate.setOnClickListener(this);
        mTime.setOnClickListener(this);

        // mVehicleModel.setOnClickListener(this);
        mDateTwo.setOnClickListener(this);
        mSubmitBtn.setOnClickListener(this);
        mCancelBtn.setOnClickListener(this);
        mTimeTwo.setOnClickListener(this);
        mEdtSelfDate.setOnClickListener(this);
        mEdtSelfTime.setOnClickListener(this);
        mEdttask.setOnClickListener(this);
        mTxtSelfTask.setOnClickListener(this);
        mAddOrRemove.setOnClickListener(this);
        final String[] pins = {"Select Pincode", "61407", "256982", "54689", "12157", "23323", "232323"};
        ArrayAdapter<String> areaadapter = new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, pins);
        mSpinnerPinCode.setAdapter(areaadapter);
        mCheckSelfAssign.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mSelfLayout.setVisibility(View.VISIBLE);
                } else {
                    mTxtSelfTask.setText("");
                    mEdtSelfTime.setText("");
                    mEdtSelfDate.setText("");
                    mSelfLayout.setVisibility(View.GONE);
                }
            }
        });

        mSpinnerPinCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("asdfsdffg", mSpinnerPinCode.getSelectedItem().toString());
                if (!"Select Pincode".equals(mSpinnerPinCode.getSelectedItem().toString())) {
                    if (pins != null) {
/*                        ArrayAdapter<String> areaadapter = new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, pins);
                        mSpinnerPinCode.setAdapter(areaadapter);*/
                        mPincode.setText("" + mSpinnerPinCode.getSelectedItem().toString());
                        // pincode.setTextColor(getResources().getColor(R.color.trans));

                    } else {
                        mPincode.setText(" ");
                        //pincode.setTextColor(getResources().getColor(R.color.trans));
                    }
                } else {
                    /*String[] locality = {"Select Locality"};
                    ArrayAdapter<String> areaadapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_dropdown_item, R.id.text1, locality);
                    areaadapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    slocality.setAdapter(areaadapter);*/
                    mPincode.setText(" ");
                    //pincode.setTextColor(getResources().getColor(R.color.trans));

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mRgEnquiryFor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                     @Override
                                                     public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                         mLLServiceAndSapre.setVisibility(View.VISIBLE);
                                                         String city = mCity.getText().toString().trim();
                                                         String mobile = mMobileNo.getText().toString();
                                                         /*String message = mMessage.getText().toString().trim();
                                                         String model = mSpVehicleModel.getSelectedItem().toString() != null ? mSpVehicleModel.getSelectedItem().toString() : noVehileModel;*/
                                                         Log.d("aslkalska", "sdsjdksjd  " + LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE) + city + "  " + mobile);
                                                         /*if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_DEALER)) {

                                                             if (checkedId != -1) {
                                                                 RadioButton radioSexButton = view.findViewById(checkedId);
                                                                 selection = radioSexButton.getText().toString();
                                                             }

                                                             dialog = MessageUtils.showDialog(getActivity());

                                                             onVehicleModel(assigned, city, mobile, selection);

                                                             mMessage.setText("");

                                                         } else {*/

                                                         Log.d("T_DEALERsdsd", "" + T_DEALER + "  " + LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE));
                                                         /*if (mobile.length() == 10) {
                                                             if (!city.isEmpty()) {*/

                                                         mEdttask.setText("");
                                                         mTxtSelfTask.setText("");
                                                         /*mRgAssigned.clearCheck();
                                                         int selectedId = mRgAssigned.getCheckedRadioButtonId();
                                                         if (selectedId == -1) {
                                                             mLLCD.setVisibility(View.GONE);
                                                         }*/
                                                         RadioButton radioSexButton = view.findViewById(checkedId);
                                                         selection = radioSexButton.getText().toString();
                                                         try {
                                                             if (selection.equals(getString(R.string.sales))) {

                                                                 mLLSpareAll.setVisibility(View.GONE);
                                                                 mChassisNo.setVisibility(View.GONE);
                                                                 mServiceIssueLay.setVisibility(View.GONE);
                                                                 mLLAddMoreSpare.setVisibility(View.GONE);

                                                                 try {

                                                                     addedMoreSpares.clear();
                                                                     mLLAddMoreSpare.removeAllViews();
                                                                 } catch (NullPointerException e) {

                                                                     e.printStackTrace();
                                                                 }

                                                             } else if (selection.equals(getString(R.string.service))) {
                                                                 mLLAddMoreSpare.setVisibility(View.GONE);
                                                                 mLLSpareAll.setVisibility(View.GONE);
                                                                 mChassisNo.setVisibility(View.VISIBLE);
                                                                 mServiceIssueLay.setVisibility(View.VISIBLE);


                                                                 try {
                                                                     addedMoreSpares.clear();
                                                                     mLLAddMoreSpare.removeAllViews();
                                                                 } catch (NullPointerException e) {
                                                                     e.printStackTrace();
                                                                 }


                                                             } else if (selection.equals(getString(R.string.spares))) {

                                                                 mLLAddMoreSpare.setVisibility(View.VISIBLE);
                                                                 mChassisNo.setVisibility(View.GONE);
                                                                 mLLSpareAll.setVisibility(View.VISIBLE);
                                                                 mServiceIssueLay.setVisibility(View.GONE);

                                                             }
                                                         } catch (Exception e) {
                                                             e.printStackTrace();
                                                         }

                                                         // Log.d("KEY_USER_TYPE", LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE));

                                                         //show assigned details is hidden and show only CRM UserIntentModel
                                                         if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CRM)) {
                                                             //mSelfLayout.setVisibility(View.GONE);
                                                             mLlBelowMsg.setVisibility(View.VISIBLE);
                                                         } else {
                                                             //mSelfLayout.setVisibility(View.VISIBLE);
                                                             mLlBelowMsg.setVisibility(View.GONE);
                                                         }


                                                         //onVehicleModel(assigned, city, mobile, selection, model);

                                                         onLoadTaskList(selection);

                                                         mMessage.setText("");

                                                         /* } else {

                                                          *//*rb1.setChecked(false);
                                                                 rb2.setChecked(false);
                                                                 rb3.setChecked(false);*//*
                                                                 snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Enter City Name");
                                                             }*/
                                                         /*} else {

                                                          *//*rb1.setChecked(false);
                                                             rb2.setChecked(false);
                                                             rb3.setChecked(false);*//*
                                                             snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Enter valid 10 digit mobile number");
                                                         }*/
                                                     }
                                                     //}
                                                 }

        );


        mSpLeadSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("leads", mSpLeadSource.getSelectedItem().toString());
                if (!"Select Lead Source".equals(mSpLeadSource.getSelectedItem().toString())) {
                    mLeadSourceEdt.setText("" + mSpLeadSource.getSelectedItem().toString());
                } else {
                    mLeadSourceEdt.setText(" ");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mSpState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cityArray.clear();
                Log.e("leadsasasa", mSpState.getSelectedItem().toString() + " \n" + BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN));
                mCity.setText("");
                mCityAuto.setText("");
                if (!"Select State".equals(mSpState.getSelectedItem().toString())) {
                    mState.setText("" + mSpState.getSelectedItem().toString());
                    String state = mState.getText().toString();
                    HashMap<String, String> map = new HashMap<>();
                    map.put("state", state);

                    if (NetworkTester.isNetworkAvailable(getActivity())) {
                        Call<ModelCityBasedOnState> modelCitysBasedOnStateCall = retrofitService.onLoadCity(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), map);

                        dialog = MessageUtils.showDialog(getActivity());

                        modelCitysBasedOnStateCall.enqueue(new Callback<ModelCityBasedOnState>() {
                            @Override
                            public void onResponse(Call<ModelCityBasedOnState> call, Response<ModelCityBasedOnState> response) {
                                MessageUtils.dismissDialog(dialog);
                                if (response.isSuccessful()) {
                                    ModelCityBasedOnState modelCitysBasedOnState = response.body();
                                    if (modelCitysBasedOnState.getSuccess()) {
                                        int cityCount = modelCitysBasedOnState.getData().getCities() != null ? modelCitysBasedOnState.getData().getCities().size() : 0;
                                        //Log.d("modelCitysBasedOnStatesds", "" + cityCount);
                                        if (cityCount != 0) {
                                            for (int i = 0; i <= cityCount - 1; i++) {
                                                cityArray.add(modelCitysBasedOnState.getData().getCities().get(i).getCity());
                                            }
                                            //cityArray.add(0, "Select City");
                                            // Log.d("cityArraysds", "" + cityArray.size());
                                            mCityAuto.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, cityArray));
                                        } else {
                                            cityArray.add("City Not Found");
                                            mCityAuto.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, cityArray));
                                        }
                                    } else {
                                        showSnackBar(getActivity(), mSnackView, modelCitysBasedOnState.getMessage());
                                    }

                                } else {

                                    snackbar = MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                                }
                            }

                            @Override
                            public void onFailure(Call<ModelCityBasedOnState> call, Throwable t) {
                                MessageUtils.dismissDialog(dialog);
                                snackbar = showSnackBar(getActivity(), mSnackView, MessageUtils.showFailureToast(t.getMessage()));
                            }
                        });
                    } else {
                        MessageUtils.showSnackBarAction(getActivity(), mSnackView, getActivity().getString(R.string.check_internet));
                    }
                } else {
                    mState.setText(" ");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mEdtSelfTaskSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String taskSelf = mEdtSelfTaskSp.getSelectedItem().toString();
                if (taskSelf.equals("Select Task")) {
                    mTxtSelfTask.setText("");
                } else {
                    mTxtSelfTask.setText("" + taskSelf);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mCityAuto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String input = s.toString();
                if (input.isEmpty()) {
                    mCity.setText("");
                } else {
                    mCity.setText("" + input);
                }
                radioCorporate.setChecked(false);
                radioDealer.setChecked(false);
                mSpAssignedTaskCD.setAdapter(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mAutoTask.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String input = s.toString();
                if (input.isEmpty()) {
                    mEdttask.setText("");
                } else {
                    mEdttask.setText("" + input);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mSpSpares.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sparesName = mSpSpares.getSelectedItem().toString();
                Log.d("sparesNamesdsd", "" + sparesName);
                if (sparesGetters.size() > 1) {
                    sparesId = sparesGetters.get(i).sparesId;
                }

                if (sparesName.equals("Select Spare")) {
                    mSparesEdt.setText(" ");
                } else {
                    mSparesEdt.setText(sparesName);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mServiceIssueSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String input = mServiceIssueSp.getSelectedItem().toString();
                Log.d("input ", "" + input);
                if (input.equals(noServiceIssue)) {
                    Log.d("input ", "1254 ");
                    mServiceIssue.setText("");
                } else {
                    Log.d("input ", "1111 ");
                    mServiceIssue.setText(input);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mServiceTypeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String input = mServiceTypeSp.getSelectedItem().toString();
                Log.d("input ", "" + input);
                if (input.equals(noServiceIssue)) {
                    Log.d("input ", "1254 ");
                    mWarrantEdt.setText("");
                } else {
                    Log.d("input ", "1111 ");
                    mWarrantEdt.setText(input);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mSpAssignedTask.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String input = mSpAssignedTask.getSelectedItem().toString();
                if (input.isEmpty()) {
                    mEdttask.setText("");
                } else if (input.equals("Others")) {
                    mEdttask.setText("");
                    mSpAssignedTask.setVisibility(View.GONE);

                    final Dialog dialogAdd = new Dialog(getActivity());
                    dialogAdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogAdd.setContentView(R.layout.dialog_others);
                    dialogAdd.setCancelable(false);
                    dialogAdd.setTitle(null);
                    dialogAdd.show();
                    dialogAdd.getWindow().setWindowAnimations(R.style.grow);
                    final CustomEditText mEdtTaskName = dialogAdd.findViewById(R.id.dia_task_name_edt);
                    final LinearLayout mSnack = dialogAdd.findViewById(R.id.dia_snack);
                    CustomButton mBtnAdd = dialogAdd.findViewById(R.id.btn_add);
                    CustomButton mBtnCancel = dialogAdd.findViewById(R.id.btn_call);
                    mBtnAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String task = mEdtTaskName.getText().toString().trim();
                            if (!task.isEmpty()) {
                                dialogAdd.dismiss();
                                mSpAssignedTask.setVisibility(View.GONE);
                                mEdttask.setTextColor(getActivity().getResources().getColor(android.R.color.background_dark));
                                mEdttask.setText("" + task);
                            } else {
                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnack, "Task Name can't be empty");
                            }
                        }
                    });

                    mBtnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogAdd.dismiss();
                            mSpAssignedTask.setVisibility(View.VISIBLE);
                            mEdttask.setText("");

                            mEdttask.setTextColor(getActivity().getResources().getColor(android.R.color.transparent));

                        }
                    });

                } else {
                    mEdttask.setText("" + input);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpVehicleModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    MyTextView_Lato_Bold vehicleName = view.findViewById(R.id.text1);
                    modelName = vehicleName.getText().toString();
                    mVehicleModel.setText(modelName);
                    Log.d("selectionsdsd", "" + selection);
                    if (selection.equals(getActivity().getString(R.string.spares))) {
                        if (!modelName.equals("Select Vehicle Model")) {

                            mLLSpareAll.setVisibility(View.VISIBLE);
                            if (NetworkTester.isNetworkAvailable(getActivity())) {
                                dialog = MessageUtils.showDialog(getActivity());
                                HashMap<String, String> map = new HashMap<>();
                                map.put("vehicleModel", modelName);
                                map.put("enquiry_for", selection);

                                retrofitService.onLoadSparesInEnquiry(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), map).enqueue(new Callback<ModelVehicleModelBaseSpares>() {
                                    @Override
                                    public void onResponse(Call<ModelVehicleModelBaseSpares> call, Response<ModelVehicleModelBaseSpares> response) {
                                        MessageUtils.dismissDialog(dialog);
                                        spareList.clear();
                                        spareList = new ArrayList<>();
                                        if (response.isSuccessful()) {
                                            modelVehicleModelBaseSpares = response.body();
                                            if (modelVehicleModelBaseSpares != null && modelVehicleModelBaseSpares.success) {
                                                //spareList.add(getActivity().getString(R.string.select_spare));
                                                if (modelVehicleModelBaseSpares.data != null && modelVehicleModelBaseSpares.data.sparesLists.size() >= 1) {
                                                    sparesGetters.clear();
                                                    for (ModelVehicleModelBaseSpares.SparesList sparesList : modelVehicleModelBaseSpares.data.sparesLists) {
                                                        spareList.add(sparesList.spares);
                                                        SparesGetter sparesGetter = new SparesGetter(sparesList.id, sparesList.spares, sparesList.spare_id, sparesList.spare_code, sparesList.vehicle_model_id, sparesList.model_varient_id);
                                                        sparesGetters.add(sparesGetter);
                                                    }

                                                    if (spareList.size() != 0) {
                                                        SparesGetter sparesGetter = new SparesGetter();
                                                        spareList.add(0, "Select Spare");
                                                        sparesGetters.add(0, sparesGetter);

                                                        mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, spareList));
                                                        mSparesValidate.setVisibility(View.VISIBLE);
                                                    } else {
                                                        mSparesValidate.setVisibility(View.GONE);
                                                    }
                                                } else {
                                                    mSparesValidate.setVisibility(View.GONE);
                                                }
                                            } else {
                                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Response not found");
                                            }
                                        } else {
                                            snackbar = MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ModelVehicleModelBaseSpares> call, Throwable t) {
                                        MessageUtils.dismissDialog(dialog);
                                        Log.d("EHATS", "" + t.getMessage());
                                        snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
                                    }
                                });
                            } else {
                                MessageUtils.showSnackBarAction(getActivity(), mSnackView, getActivity().getString(R.string.check_internet));
                            }
                        } else {
                            Log.d("vehicleNameCllApi", "No Way");
                            snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, "Vehicle Model Selection Missing");
                        }
                    } else {
                        Log.d("vehicleNameCllApi", "No Way  " + selection);
                        //snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, "Enquiry Selection Missing");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mSpAssignedTaskCD.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectAssign = mSpAssignedTaskCD.getSelectedItem().toString();
                Log.d("slklskd", selectAssign.length() + "  " + dealers.size() + " ddd " + corporates.size());
                try {
                    if (!selectAssign.trim().equals("Select Dealer".trim()) && !selectAssign.trim().equals("Select Corporate".trim()) && !selectAssign.equals("Select ")) {
                        if (assigned.equals(getActivity().getResources().getString(R.string.dealer))) {
                            assigned = dealers.get(position).username;
                            assignedId = dealers.get(position).id;
                        } else {
                            assigned = corporates.get(position).username;
                            assignedId = corporates.get(position).id;
                        }
                    }

                    Log.d("selectAssign", "" /*+ selectAssign.length() + "  " + selection + "Select Dealer".length()*/ + "   " + assigned);
                    if (!selectAssign.trim().equals("Select Dealer".trim()) && !selectAssign.trim().equals("Select Corporate".trim())) {
                        if (selection.equals(getActivity().getString(R.string.spares)))
                            mLLSpareAll.setVisibility(View.VISIBLE);
                        Log.d("sparess", "noo");
                    } else {
                        Log.d("sparess", "yesss");
                        //mLLSpareAll.setVisibility(View.GONE);
                    }
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mRgAssigned.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                   @Override
                                                   public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                       try {
                                                           String city = mCity.getText().toString();
                                                           String mobile = mMobileNo.getText().toString();
                                                           if (mobile.length() == 10) {
                                                               //if (!city.isEmpty()) {
                                                               if (checkedId != -1) {
                                                                   RadioButton radioSexButton = view.findViewById(checkedId);
                                                                   assigned = radioSexButton.getText().toString();
                                                                   if (isValidateModel(modelVehicleModel)) {
                                                                       ArrayList<String> assignBy = new ArrayList<>();
                                                                       corporates.clear();
                                                                       dealers.clear();
                                                                       if (assigned.equals("Dealer")) {
                                                                           assignBy.clear();
                                                                           assignBy.add("Select " + assigned);
                                                                           for (ModelAssign.Data.Dealers dealer : modelVehicleModel.data.dealers) {
                                                                               assignBy.add(dealer.name);
                                                                               dealers.add(dealer);
                                                                           }
                                                                           dealers.add(0, new ModelAssign.Data.Dealers());
                                                                       } else {
                                                                           assignBy.clear();
                                                                           assignBy.add("Select " + assigned);
                                                                           for (ModelAssign.Data.Corporate corporate : modelVehicleModel.data.corporate) {
                                                                               assignBy.add(corporate.name);
                                                                               corporates.add(corporate);
                                                                           }
                                                                           corporates.add(0, new ModelAssign.Data.Corporate());
                                                                       }
                                                                       mSpAssignedTaskCD.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, assignBy));
                                                                   }
                                                                   mLLCustomType.setVisibility(View.VISIBLE);
                                                                   mLLCD.setVisibility(View.VISIBLE);
                                                               }
                                                           /*} else {
                                                               radioDealer.setChecked(false);
                                                               radioCorporate.setChecked(false);
                                                               snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Please enter City Name");
                                                           }*/
                                                           } else {
                                                               rb1.setChecked(false);
                                                               rb2.setChecked(false);
                                                               rb3.setChecked(false);
                                                               snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Enter valid 10 digit mobile number");
                                                           }
                                                       } catch (Exception e) {
                                                           e.printStackTrace();
                                                       }
                                                   }
                                               }

        );

    /*
        mMessage.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                               public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                                   if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                                                       validation();
                                                   }
                                                   return false;
                                               }
                                           }

        );
        mMobileNo.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mMobileNo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                                                        String date = mDate.getText().toString();
                                                        if (date == "") {
                                                            showDateDailog(1);
                                                        }
                                                    }
                                                    return false;
                                                }
                                            }

        );
*/
    }

    private void onLoadTaskList(String enquiry_for) {
        if (NetworkTester.isNetworkAvailable(getActivity())) {
            dialog = MessageUtils.showDialog(getActivity());
            HashMap<String, String> map = new HashMap<>();
            map.put("enquiry_for", enquiry_for);
            final Call<ModelTaskList> modelTaskListCall = retrofitService.onLoadTask(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), map);
            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    MessageUtils.dismissDialog(dialog);
                    modelTaskListCall.cancel();
                    return false;
                }
            });
            modelTaskListCall.enqueue(new Callback<ModelTaskList>() {
                @Override
                public void onResponse(Call<ModelTaskList> call, Response<ModelTaskList> response) {
                    Log.d("taskLoader", "" + response.message());
                    MessageUtils.dismissDialog(dialog);
                    taskArray.clear();
                    if (response.isSuccessful()) {
                        ModelTaskList modelTaskList = response.body();
                        if (modelTaskList != null) {
                            int count = modelTaskList.getData().getTasklist() != null ? modelTaskList.getData().getTasklist().size() : 0;
                            if (count != 0) {

                                for (Tasklist tasks : modelTaskList.getData().getTasklist()) {
                                    //if (selection.equals(tasks.task_type)) {
                                    taskArray.add(tasks.getTask());
                                    //}
                                }
                                taskArray.add("Others");
                            }
                            Log.d("taskArraySet", "" + taskArray.size());

                            mEdttask.setText("");
                        } else {
                            snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, modelTaskList.getMessage());
                        }
                    } else {
                        snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, showErrorCodeToast(response.code()));
                    }
                }

                @Override
                public void onFailure(Call<ModelTaskList> call, Throwable t) {
                    MessageUtils.dismissDialog(dialog);
                    Log.d("taskLoader789", "" + t.getMessage());
                    snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
                }
            });
        } else {
            MessageUtils.showSnackBarAction(getActivity(), mSnackView, getActivity().getString(R.string.check_internet));
        }
    }

    private void setTaskDialog(final int taskInt) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setWindowAnimations(R.style.grow);
        dialog.setContentView(R.layout.dialog_drop_down);
        CustomTextView status = dialog.findViewById(R.id.status);
        status.setText("Select Task ");
        ListView listView = dialog.findViewById(R.id.dialog_list);
        listView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, taskArray));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String input = taskArray.get(position).toString();
                MessageUtils.dismissDialog(dialog);
                if (input.isEmpty()) {
                    if (taskInt == 1) {
                        mEdttask.setText("");
                    } else {
                        mTxtSelfTask.setText("");
                    }
                } else if (input.equals("Others")) {
                    if (taskInt == 1) {
                        mEdttask.setText("");
                    } else {
                        mTxtSelfTask.setText("");
                    }
                    final Dialog dialogAdd = new Dialog(getActivity());
                    dialogAdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogAdd.setContentView(R.layout.dialog_others);
                    dialogAdd.setCancelable(false);
                    dialogAdd.getWindow().setWindowAnimations(R.style.grow);
                    final CustomEditText mEdtTaskName = dialogAdd.findViewById(R.id.dia_task_name_edt);
                    final LinearLayout mSnack = dialogAdd.findViewById(R.id.dia_snack);
                    CustomButton mBtnAdd = dialogAdd.findViewById(R.id.btn_add);
                    CustomButton mBtnCancel = dialogAdd.findViewById(R.id.btn_call);
                    mBtnAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String task = mEdtTaskName.getText().toString().trim();
                            if (!task.isEmpty()) {
                                dialogAdd.dismiss();
                                if (taskInt == 1) {
                                    mEdttask.setTextColor(getActivity().getResources().getColor(android.R.color.background_dark));
                                    mEdttask.setText("" + task);
                                } else {
                                    mTxtSelfTask.setTextColor(getActivity().getResources().getColor(android.R.color.background_dark));
                                    mTxtSelfTask.setText("" + task);
                                }
                            } else {
                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnack, "Task Name can't be empty");
                            }
                        }
                    });

                    mBtnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogAdd.dismiss();
                            if (taskInt == 1) {
                                mEdttask.setText("");
                                mEdttask.setTextColor(getActivity().getResources().getColor(android.R.color.background_dark));
                            } else {
                                mTxtSelfTask.setTextColor(getActivity().getResources().getColor(android.R.color.background_dark));
                                mTxtSelfTask.setText("");
                            }
                        }
                    });
                    dialogAdd.show();
                } else {
                    if (taskInt == 1) {
                        mEdttask.setText("" + input);
                    } else {
                        mTxtSelfTask.setText("" + input);
                    }
                }
            }
        });


        dialog.show();
    }
/*

    class AddNextTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            return null;
        }
    }
*/

    /*
    Get Vehicle Model
     */
    private void onVehicleModel(final String assigned, String city, String mobile, final String enquiry_for, String model) {
        if (NetworkTester.isNetworkAvailable(getActivity())) {
            HashMap<String, String> stringStringHashMap = new HashMap<>();
            stringStringHashMap.put("city", city);
            stringStringHashMap.put("enquiry_for", enquiry_for);
            stringStringHashMap.put("mobile_no", mobile);
            stringStringHashMap.put("vehicleModel", model);

        /*Gson gson = new Gson();
        String result = "";

        TaskDetailsModel taskDetailsModel = gson.fromJson(result, TaskDetailsModel.class);*/

       /* AddNextTask addNextTask = new AddNextTask();
        addNextTask.execute();*/
            //final Call<ModelAssign> modelVehicleModel = retrofitService.onVehicleModel(LocalSession.getUserInfo(getActivity(), KEY_NAME), assigned, city, mobile, "Bearer " + enquiry_for);
            final Call<ModelAssign> modelAssignCall = retrofitService.onAssign(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), stringStringHashMap);
            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialo, int keyCode, KeyEvent event) {
                    MessageUtils.dismissDialog(dialog);
                    modelAssignCall.cancel();
                    return false;
                }
            });
            modelAssignCall.enqueue(new Callback<ModelAssign>() {
                @Override
                public void onResponse(Call<ModelAssign> call, Response<ModelAssign> response) {
                    MessageUtils.dismissDialog(dialog);
                    if (response.isSuccessful()) {
                        modelVehicleModel = response.body();
                        if (modelVehicleModel != null) {
                            if (modelVehicleModel.isStatus) {
                                taskArray.clear();
                                //Chassis No edtiview changes
                                if (!modelVehicleModel.data.chassis_no.isEmpty()) {
                                    mChassisNo.setText("" + modelVehicleModel.data.chassis_no);
                                    mWarrantEdt.setText("" + modelVehicleModel.data.chassis_no);

                                /*MyUtils.focusable(mChassisNo, false);
                                MyUtils.focusable(mWarrantEdt, false);*/
/*
                                mChassisNo.setFocusableInTouchMode(false);
                                mChassisNo.setLongClickable(false);
                                mChassisNo.setClickable(false);*/

                                } else {

                                /*mChassisNo.setFocusableInTouchMode(true);
                                mChassisNo.setLongClickable(true);
                                mChassisNo.setClickable(true);*/
                               /* MyUtils.focusable(mWarrantEdt, true);
                                MyUtils.focusable(mChassisNo, true);*/
                                }

                                //Vehicle Model Spinner and Textview changes
                                if (!modelVehicleModel.data.vehicle_model.isEmpty()) {
                                    mVehicleModel.setText("" + modelVehicleModel.data.vehicle_model);
                                    mVehicleModel.setVisibility(View.VISIBLE);
                                    mVehicleModel.setTextColor(getResources().getColor(R.color.black));
                                    mSpVehicleModel.setVisibility(View.GONE);
                                } else {
                                    mSpVehicleModel.setVisibility(View.VISIBLE);
                                }


                                if (modelVehicleModel.data.tasks.size() >= 1) {

                                    for (ModelAssign.Data.Tasks tasks : modelVehicleModel.data.tasks) {
                                        //if (selection.equals(tasks.task_type)) {
                                        taskArray.add(tasks.task);
                                        //}
                                    }
                                    taskArray.add("Others");
                                }
                                Log.d("taskArraySet", "" + taskArray.size());
                                mEdttask.setText("");
                                ArrayList<String> spareList = new ArrayList<>();


                                // spareList.add(getActivity().getString(R.string.select_spare));
                                sparesGetters.clear();

                            /*for (DataItem dataItem : modelVehicleModel.data.dataItems) {
                                spareList.add(dataItem.name);

                                SparesGetter sparesGetter = new SparesGetter(dataItem.id, dataItem.name, dataItem.batchNo, dataItem.serialNo, dataItem.manufacturingDate
                                        , dataItem.lotNumber, dataItem.supplierDetails, dataItem.supplierInvoiceDetails);
                                sparesGetters.add(sparesGetter);
                            }
*/

                                if (enquiry_for.equals(getActivity().getString(R.string.spares)) && !mSpVehicleModel.getSelectedItem().toString().equals(noVehileModel)) {
                                    mSpSpares.setVisibility(View.VISIBLE);
                                }

                                mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, spareList));
                                try {
                                    ArrayList<String> serviceIssue = new ArrayList<>();
                                    for (ModelAssign.ServiceIssue serviceIssue1 : modelVehicleModel.data.serviceIssues) {
                                        serviceIssue.add(serviceIssue1.service_issue);
                                    }

                                    if (serviceIssue.size() == 0) {
                                        serviceIssue.add(noServiceIssue);
                                    }
                                    mServiceIssueSp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, serviceIssue));

                                    ArrayList<String> serviceType = new ArrayList<>();
                                    for (ModelAssign.ServiceType serviceIssue1 : modelVehicleModel.data.serviceTypes) {
                                        serviceType.add(serviceIssue1.status);
                                    }

                                    if (serviceType.size() == 0) {
                                        serviceType.add(noServiceIssue);
                                    }
                                    mServiceTypeSp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, serviceType));


                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                                //setTaskDialog();

                            } else {
                                radioCorporate.setChecked(false);
                                radioDealer.setChecked(false);
                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, modelVehicleModel.message);
                            }

                        } else {
                            radioCorporate.setChecked(false);
                            radioDealer.setChecked(false);
                            snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.api_response_not_found));
                        }
                    } else {
                        radioCorporate.setChecked(false);
                        radioDealer.setChecked(false);
                        snackbar = MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                    }

                }

                @Override
                public void onFailure(Call<ModelAssign> call, Throwable t) {
                    MessageUtils.dismissDialog(dialog);
                    radioCorporate.setChecked(false);
                    radioDealer.setChecked(false);
                    snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
                }
            });
        } else {
            MessageUtils.showSnackBarAction(getActivity(), mSnackView, getActivity().getString(R.string.check_internet));
        }
    }

    private boolean isValidateModel(ModelAssign modelVehicleModel) {

        if (modelVehicleModel.data == null) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "No data found");
            return false;
        } else if (modelVehicleModel.data.dealers == null) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, modelVehicleModel.message);
            return false;
        } else if (modelVehicleModel.data.dealers.size() == 0) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "No data found");
            return false;
        }
        return true;
    }


    /**
     * Load Assigned to (Dealer and Corporate based on City)
     *
     * @param assigned
     * @param city
     * @param enquiry_for
     */
    private void onLoadAssigned(String assigned, String city, String enquiry_for) {
        if (NetworkTester.isNetworkAvailable(getActivity())) {
            final Call<ModelAssign> modelAssignCall = retrofitService.onLoadAssigned(assigned, city, enquiry_for, LocalSession.getUserInfo(getActivity(), KEY_NAME), BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN));
            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialo, int keyCode, KeyEvent event) {
                    MessageUtils.dismissDialog(dialog);
                    modelAssignCall.cancel();
                    return false;
                }
            });
            modelAssignCall.enqueue(new Callback<ModelAssign>() {
                @Override
                public void onResponse(Call<ModelAssign> call, Response<ModelAssign> response) {
                    MessageUtils.dismissDialog(dialog);
                    if (response.isSuccessful()) {
                        ModelAssign modelVehicleModel = response.body();
                        if (modelVehicleModel != null) {
                            if (modelVehicleModel.isStatus) {

                            } else {
                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, modelVehicleModel.message);
                            }
                        } else {
                            snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Response not found");
                        }
                    } else {
                        snackbar = MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                    }
                }

                @Override
                public void onFailure(Call<ModelAssign> call, Throwable t) {
                    MessageUtils.dismissDialog(dialog);
                    snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
                }
            });
        } else {
            MessageUtils.showSnackBarAction(getActivity(), mSnackView, getActivity().getString(R.string.check_internet));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        Calendar calendar = Calendar.getInstance();
        switch (v.getId()) {
            case R.id.call_log:
                Log.d("Logidss", "onClick");
                if (isReadLocationAllowed()) {
                    Log.d("Logidss", "ifff");
                    Intent intent = new Intent(getActivity(), CallLogActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    Log.d("Logidss", "elase");
                    requestLocationPermission();
                }

                break;
            case R.id.apnt_date:
                if (!isSelect) {
                    showDateDailog(1);
                }
                break;

            case R.id.edt_task:
                if (taskArray.size() >= 1) {
                    setTaskDialog(1);
                } else {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, "No task found or Select Enquiry For");
                }
                break;

            case R.id.edt_self_task:
                if (taskArray.size() >= 1) {
                    setTaskDialog(2);
                } else {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, "No task found or Select Enquiry For");
                }
                break;

            case R.id.apnt_time:
                if (!isSelect) {

                    String date = mDate.getText().toString();
                    if (date != "") {
                        TimePickerDialog d = new TimePickerDialog(getActivity(), getTimeFromPicker(Enquiry.this, mTime), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                        d.getWindow().setWindowAnimations(R.style.grow);
                        d.show();

                    } else {
                        showSnackBar(getActivity(), mSnackView, "Date Can't be Empty.");
                    }
                }
                break;
            case R.id.apnt_time_two:

                TimePickerDialog d = new TimePickerDialog(getActivity(), getTimeFromPicker(Enquiry.this, mTimeTwo), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                d.getWindow().setWindowAnimations(R.style.grow);
                d.show();
                break;

            case R.id.apnt_txt_vehicle_model:
                // selectCar(1);
                break;
            case R.id.apnt_date_for_what:
                showDateDailog(2);
                break;
            case R.id.apnt_self_time:
                d = new TimePickerDialog(getActivity(), getTimeFromPicker(Enquiry.this, mEdtSelfTime), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                d.getWindow().setWindowAnimations(R.style.grow);
                d.show();
                break;
            case R.id.apnt_self_date:
                showDateDailog(3);
                break;
            case R.id.add_remove:
                String spareQty = mSpareQty.getText().toString();
                if (validateSpareMore(sparesName, spareQty)) {
                    mLLAddMoreSpare.setVisibility(View.VISIBLE);
                    addMoreSpares(sparesId, spareQty);
                }

                break;
            case R.id.apnt_done:

                String name = mName.getText().toString().trim();
                String email = mEmail.getText().toString().trim();
                String mobileNo = mMobileNo.getText().toString().trim();
                String date = mDate.getText().toString().trim();
                String time = mTime.getText().toString().trim();
                String city = mCity.getText().toString().trim();
                String cityAuto = mCityAuto.getText().toString().trim();
                String leadSource = mSpLeadSource.getSelectedItem().toString();
               /* String vehicleMode = mSpVehicleModel.getSelectedItem().toString();
                Log.d("vehicleMode", "One " + vehicleMode);*/
                String vehicleMode = mVehicleModel.getText().toString();
                Log.d("vehicleMode", "" + vehicleMode);
                String message = mMessage.getText().toString().trim();
                int intEnquiry = mRgEnquiryFor.getCheckedRadioButtonId();
                int selectedAssign = mRgAssigned.getCheckedRadioButtonId();
                String chassis_no = mChassisNo.getText().toString();

                String values = mValues.getText().toString();
                String warranty = mWarrantEdt.getText().toString();
                String address = mAddress.getText().toString();
                String address1 = mAddress2.getText().toString();
                String locality = mLocality.getText().toString();
                String vehicle_color = mVehicleColor.getText().toString();
                String state = mState.getText().toString();
                String pincode = mPincode.getText().toString();
                /*String serviceIssue = mServiceIssueSp.getSelectedItem().toString();



                Log.d("ServieIssue", serviceIssue);*/

                String serviceIssue = mServiceIssue.getText().toString().toLowerCase();

                Log.d("ServieIssue", "EditText " + serviceIssue);

                Log.d("selectsss", "  intEnquiry  " + intEnquiry + " selectedAssign " + selectedAssign + " vehicleMode " + vehicleMode + "  leadSource " + leadSource + "  message  " + message);

                if (validateCredentials(name, email, mobileNo, date, time, state, cityAuto)) {
                    Log.d("sdsdssdsd", "1");
                    if (!leadSource.equals(noLeadSource)) {
                        Log.d("sdsdssdsd", "12");
                        if (intEnquiry != -1) {
                            Log.d("sdsdssdsd", "333");
                            RadioButton rbenquiryBtn = mRgEnquiryFor.findViewById(intEnquiry);
                            String enquiry = rbenquiryBtn.getText().toString();
                            //if (!values.isEmpty()) {
                            if (!vehicleMode.equals(noVehileModel)) {
                                Log.d("sdsdssdsd", "444");
                                if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CRM)) {
                                    if (selectedAssign != -1) {
                                        RadioButton radioSexButton = mRgAssigned.findViewById(selectedAssign);
                                        String assigneto = radioSexButton.getText().toString();
                                        Log.d("enquiry", assigneto + "  enquiry " + enquiry + "   assigned  " + assigned);
                                        if (!assigned.equals("Select Dealer") && !assigned.equals("Select Corporate")) {
                                            String dateTwo = mDateTwo.getText().toString();
                                            String timeTwo = mTimeTwo.getText().toString();
                                            if (!dateTwo.isEmpty()) {
                                                if (!timeTwo.isEmpty()) {
                                                    String task = mEdttask.getText().toString();
                                                    if (!task.isEmpty()) {
                                                        HashMap<String, Object> salesInsert = new HashMap<>();
                                                        salesInsert.put("user_type", LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE));
                                                        salesInsert.put("user_name", LocalSession.getUserInfo(getActivity(), KEY_ID));
                                                        salesInsert.put("customer_name", name);
                                                        salesInsert.put("email", email);
                                                        salesInsert.put("mobile_no", mobileNo);
                                                        salesInsert.put("date", date);
                                                        salesInsert.put("time", time);
                                                        salesInsert.put("city", cityAuto);
                                                        salesInsert.put("leadSource", leadSource);
                                                        salesInsert.put("enquiry", enquiry);
                                                        salesInsert.put("vehicleMode", vehicleMode);
                                                        salesInsert.put("message", message);
                                                        salesInsert.put("assigneto", assigneto);
                                                        salesInsert.put("assigned", assigned);
                                                        salesInsert.put("dateTwo", dateTwo + " " + timeTwo);
                                                        salesInsert.put("timeTwo", timeTwo);
                                                        salesInsert.put("task", task);

                                                        salesInsert.put("spares_id", sparesId);
                                                        salesInsert.put("spares_name", sparesName);
                                                        salesInsert.put("assignedId", assignedId);

                                                        salesInsert.put("service_issue", serviceIssue);
                                                        salesInsert.put("enquiry_value", values);
                                                        salesInsert.put("warranty", warranty);
                                                        salesInsert.put("address1", address);
                                                        salesInsert.put("address2", address1);
                                                        salesInsert.put("locality", locality);
                                                        salesInsert.put("pincode", pincode);
                                                        salesInsert.put("vehicle_color", vehicle_color);
                                                        salesInsert.put("state", state);

                                                        //onSales(name, email, mobileNo, date, time, cityAuto, leadSource, enquiry, vehicleMode, message, assigneto, assigned, dateTwo, task);
                                                        if (enquiry.equals(getString(R.string.service))) {
                                                            if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, chassis_no, "Chassis Number Missing")) {
                                                                // if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, warranty, "Warranty Type Missing")) {
                                                                salesInsert.put("chassis_no", chassis_no);
                                                                onSales(salesInsert);
                                                                //}
                                                            }

                                                        } else {
                                                            onSales(salesInsert);
                                                        }

                                                    } else {
                                                        snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Task can't be empty");
                                                    }
                                                } else {
                                                    snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Assigned Time is Missing");
                                                }
                                            } else {
                                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Assigned Date is Missing");
                                            }
                                        } else {
                                            snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Assigned to Person Name");
                                        }
                                    } else {
                                        snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Assign To");
                                    }

                                } else {
                                    String selfTask = mTxtSelfTask.getText().toString();
                                    String selfDate = mEdtSelfDate.getText().toString();
                                    String selfTime = mEdtSelfTime.getText().toString();
                                    HashMap<String, Object> map = new HashMap<>();
                                    map.put("user_name", LocalSession.getUserInfo(getActivity(), KEY_ID));
                                    map.put("user_type", LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE));
                                    map.put("customer_name", name);
                                    map.put("mobile_no", mobileNo);
                                    map.put("date", date);
                                    map.put("time", time);
                                    map.put("city", cityAuto);
                                    map.put("email", email);
                                    map.put("leadSource", leadSource);
                                    map.put("enquiry", enquiry);
                                    map.put("vehicleMode", vehicleMode);
                                    map.put("message", message);
                                    map.put("assignedId", assignedId);
                                    map.put("assigneto", LocalSession.getUserInfo(getActivity(), KEY_ID));

                                    map.put("service_issue", serviceIssue);
                                    map.put("enquiry_value", values);
                                    map.put("warranty", warranty);
                                    map.put("address1", address);
                                    map.put("address2", address1);
                                    map.put("locality", locality);
                                    map.put("pincode", pincode);
                                    map.put("vehicle_color", vehicle_color);
                                    map.put("state", state);
                                    Log.d("mapsdsd", "" + map);

                                    if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_DEALER)) {

                                        Log.d("selfTime", selfTask + "  " + selfTime + "  " + selfDate);

                                        if (enquiry.equals(getString(R.string.service))) {
                                            if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, chassis_no, "Chassis Number Missing")) {
                                                if (checkSetlTask(selfTask, selfDate, selfTime)) {
                                                    map.put("dateTwo", selfDate + " " + selfTime);
                                                    map.put("timeTwo", selfTime);
                                                    map.put("task", selfTask);
                                                    map.put("self", "false");
                                                    //if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, warranty, "Warranty Type Missing")) {
                                                    map.put("chassis_no", chassis_no);
                                                    onSales(map);
                                                    //}
                                                }
                                            }
                                        } else {
                                            if (checkSetlTask(selfTask, selfDate, selfTime)) {
                                                map.put("dateTwo", selfDate + " " + selfTime);
                                                map.put("timeTwo", selfTime);
                                                map.put("task", selfTask);
                                                map.put("self", "false");
                                                onSales(map);
                                            }
                                        }

                                    } else if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CORPORATE_SALES) ||
                                            LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CORPORATE_SERVICE)) {
                                        if (mCheckSelfAssign.isChecked()) {

                                            Log.d("selfTime", selfTask + "  " + selfTime + "  " + selfDate);

                                            if (enquiry.equals(getString(R.string.service))) {
                                                if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, chassis_no, "Chassis Number Missing")) {
                                                    //if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, warranty, "Warranty Type Missing")) {
                                                    if (checkSetlTask(selfTask, selfDate, selfTime)) {
                                                        map.put("dateTwo", selfDate + " " + selfTime);
                                                        map.put("timeTwo", selfTime);
                                                        map.put("task", selfTask);
                                                        map.put("self", "" + mCheckSelfAssign.isChecked());

                                                        map.put("chassis_no", chassis_no);
                                                        onSales(map);
                                                    }
                                                    //}
                                                }

                                            } else {
                                                if (checkSetlTask(selfTask, selfDate, selfTime)) {
                                                    map.put("dateTwo", selfDate + " " + selfTime);
                                                    map.put("timeTwo", selfTime);
                                                    map.put("task", selfTask);
                                                    map.put("self", "" + mCheckSelfAssign.isChecked());

                                                    onSales(map);
                                                }
                                            }


                                        } else {
                                            map.put("self", "" + mCheckSelfAssign.isChecked());

                                            if (enquiry.equals(getString(R.string.service))) {
                                                if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, chassis_no, "Chassis Number Missing")) {
                                                    //if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, warranty, "Warranty Type Missing")) {
                                                    map.put("chassis_no", chassis_no);
                                                    if (checkSetlTask(selfTask, selfDate, selfTime)) {
                                                        map.put("dateTwo", selfDate + " " + selfTime);
                                                        map.put("timeTwo", selfTime);
                                                        map.put("task", selfTask);
                                                        map.put("self", "" + mCheckSelfAssign.isChecked());
                                                        onSales(map);
                                                    }
                                                    // }
                                                }

                                            } else {

                                                if (checkSetlTask(selfTask, selfDate, selfTime)) {
                                                    map.put("dateTwo", selfDate + " " + selfTime);
                                                    map.put("timeTwo", selfTime);
                                                    map.put("task", selfTask);
                                                    map.put("self", "" + mCheckSelfAssign.isChecked());
                                                    onSales(map);
                                                }
                                            }
                                        }

                                    } else {
                                        map.put("self", "false");
                                        if (enquiry.equals(getString(R.string.service))) {
                                            if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, chassis_no, "Chassis Number Missing")) {
                                                //if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, warranty, "Warranty Type Missing")) {
                                                if (checkSetlTask(selfTask, selfDate, selfTime)) {
                                                    map.put("dateTwo", selfDate + " " + selfTime);
                                                    map.put("timeTwo", selfTime);
                                                    map.put("task", selfTask);
                                                    map.put("self", "" + mCheckSelfAssign.isChecked());
                                                    map.put("chassis_no", chassis_no);
                                                    onSales(map);
                                                }
                                                // }
                                            }
                                        } else {
                                            if (checkSetlTask(selfTask, selfDate, selfTime)) {
                                                map.put("dateTwo", selfDate + " " + selfTime);
                                                map.put("timeTwo", selfTime);
                                                map.put("task", selfTask);
                                                map.put("self", "" + mCheckSelfAssign.isChecked());
                                                onSales(map);
                                            }
                                        }
                                        //LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_SPECIAL_OFFICER)
                                    }
                                }
                            } else {
                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Vehicle Model");
                            }
                           /* } else {
                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Enter Values");
                            }*/
                        } else {
                            snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Enquiry For");
                        }
                    } else {
                        snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Lead Source");
                    }
                }

                //  validation();
                break;
            case R.id.apnt_cancel:
                getActivity().onBackPressed();
                break;
            default:
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void addMoreSpares(String sparesName, String spareQty) {
        try {
            if (spareQty != null && !spareQty.isEmpty()) {
                AddedMoreSpares addedMoreSpare = new AddedMoreSpares(sparesName.trim(), spareQty.trim());
                addedMoreSpares.add(addedMoreSpare);
                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.more_spares, null);
                addView.setBackground(getActivity().getResources().getDrawable(R.drawable.normal_ripple));

                final Spinner mSpareSP = addView.findViewById(R.id.sp_spares_for);
                mSpareSP.setVisibility(View.GONE);

                final CustomTextEditView mEditSpare = addView.findViewById(R.id.apnt_edt_spares);
                mEditSpare.setTextColor(getActivity().getResources().getColor(R.color.black));
                CustomTextEditView mEditSpareQty = addView.findViewById(R.id.edt_spares_qty);
                ImageView mImAddOrRemove = addView.findViewById(R.id.add_remove);
                //mSpareSP.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, spareList));
                mImAddOrRemove.setImageResource(R.drawable.ic_remove_circle);
                mEditSpare.setText(sparesName);
                mEditSpareQty.setText(spareQty);
                mEditSpareQty.setFocusableInTouchMode(false);
                mEditSpare.setClickable(false);
                mEditSpareQty.setLongClickable(false);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(8, 8, 8, 8);
                addView.setLayoutParams(lp);
                addView.setElevation(8);

                mImAddOrRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int position = mLLAddMoreSpare.indexOfChild(addView);
                        //IndentModel indentModel = new IndentModel(type, vehicleSelection, sparesSelection, qty);s
                        try {
                            addedMoreSpares.remove(position);
                        } catch (NullPointerException e) {

                            e.printStackTrace();
                        }
                        ((LinearLayout) addView.getParent()).removeView(addView);
                    }
                });
                mLLAddMoreSpare.addView(addView);
                mSpareQty.setText(" ");
                mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, spareList));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean validateSpareMore(String sparesName, String spareQty) {
        try {
            Log.d("sparesName", "" + sparesName + " " + spareQty);
            if (sparesName != null && !sparesName.isEmpty() && spareQty != null && !spareQty.isEmpty()) {
                if (sparesName.equals("Select Spare")) {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Spare");
                    return false;
                } else if (spareQty.isEmpty()) {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, "Enter Your Quantity");
                    return false;
                } else if (spareQty.equals("0")) {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, "Enter Valid Quantity");
                    return false;
                }
            } else {
                MessageUtils.showSnackBar(getActivity(), mSnackView, "Spare Field Selection is Missing");
                return false;
            }
        } catch (Exception e) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Some Fields are Missing");
            return false;

        }
        return true;

    }

    private boolean checkSetlTask(String selfTask, String selfDate, String selfTime) {
        if (selfTask.isEmpty()) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Task");
            return false;
        } else if (selfDate.isEmpty()) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Activity Date");
            return false;
        } else if (selfTime.isEmpty()) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Activity Time");
            return false;
        }

        return true;
    }

    private void onSales(HashMap<String, Object> salesInsert) {
        if (NetworkTester.isNetworkAvailable(getActivity())) {
            String qty = mSpareQty.getText().toString();
            if (sparesId != null && !sparesId.isEmpty() && !qty.isEmpty()) {

                AddedMoreSpares addedMoreSpare = new AddedMoreSpares(sparesId, qty);
                addedMoreSpares.add(addedMoreSpare);

                //addedMoreSpares = new ArrayList<>(addedMoreSpares);
                Log.d("addedMoreSpares_after", "" + addedMoreSpares.size());
                HashMap<String, Object> jsonObject = new HashMap<String, Object>();
                jsonObject.put("spare", addedMoreSpares);
                salesInsert.put("spares", jsonObject);
                salesInsert.put("sparesItems", "true");

            } else {
                salesInsert.put("sparesItems", "false");
            }
            dialog = MessageUtils.showDialog(getActivity());

            final Call<ModelSalesInsert> modelSalesInsertCall = retrofitService.onSales(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), salesInsert);

            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface diaog, int keyCode, KeyEvent event) {
                    MessageUtils.dismissDialog(dialog);
                    modelSalesInsertCall.cancel();
                    return false;
                }
            });

            modelSalesInsertCall.enqueue(new Callback<ModelSalesInsert>() {
                @Override
                public void onResponse(Call<ModelSalesInsert> call, Response<ModelSalesInsert> response) {
                    MessageUtils.dismissDialog(dialog);
                    if (response.isSuccessful()) {
                        ModelSalesInsert modelSalesInsert = response.body();
                        if (modelSalesInsert != null) {
                            if (modelSalesInsert.status) {
                                //MyUtils.passFragmentWithoutBackStack(getActivity(), new CRM());
                                MessageUtils.showToastMessage(getActivity(), "" + modelSalesInsert.message);


                                //getActivity().onBackPressed();
                                if (NetworkTester.isNetworkAvailable(getActivity())) {
                                    Intent intent = new Intent(getActivity(), CRMDetailsActivity.class);
                                    intent.putExtra("id", "" + modelSalesInsert.enqiryId);
                                    intent.putExtra("user_name", "" + LocalSession.getUserInfo(getActivity(), KEY_USER_NAME));
                                    intent.putExtra("task_name", "" + selection);
                                    getActivity().startActivity(intent);
                                }

                                //snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, modelSalesInsert.message);
                            } else {
                                snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, modelSalesInsert.message);
                            }
                        } else {
                            snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Response not found");
                        }
                    } else {
                        snackbar = MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                    }

                }

                @Override
                public void onFailure(Call<ModelSalesInsert> call, Throwable t) {
                    MessageUtils.dismissDialog(dialog);
                    snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
                }
            });
        } else {
            MessageUtils.showSnackBarAction(getActivity(), mSnackView, getActivity().getString(R.string.check_internet));
        }
    }

    private boolean validateCredentials(String name, String email, String mobileNo, String date, String time, String state, String cityAuto) {

        if (name.isEmpty() || name.length() == 0) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Name can't be empty");
            return false;
        } else if (mobileNo.isEmpty() || mobileNo.length() < 10) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Mobile Number can't be empty");
            return false;
        } else if (date.isEmpty() || date.length() == 0) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Date can't be empty");
            return false;
        } else if (time.isEmpty() || time.length() == 0) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Time can't be empty");
            return false;
        } else if (state.equals("Select State")) {
            snackbar = showSnackBar(getActivity(), mSnackView, "State Selection Missing");
            return false;
        }/* else if (cityAuto.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "City can't be empty");
            return false;
        }*/ else if (email.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Email ID can't be empty");
            return false;
        } else if (!Pattern.matches(patternStr, email)) {
            snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Invalid Email ID");
            return false;
        }
        return true;
    }

    /**
     * Load Initial Lead Source Details
     *
     * @param retrofitService
     */
    private void onLoadLeadsDetails(RetrofitService retrofitService) {


        final Call<ModelLeadsInfo> modelLeadsInfoCallback = retrofitService.onLoadLeadsDetails(LocalSession.getUserInfo(getActivity(), KEY_NAME), LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE), BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN));
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialo, int keyCode, KeyEvent event) {
                MessageUtils.dismissDialog(dialog);
                modelLeadsInfoCallback.cancel();
                return false;
            }
        });
        modelLeadsInfoCallback.enqueue(new Callback<ModelLeadsInfo>() {
            @Override
            public void onResponse(Call<ModelLeadsInfo> call, Response<ModelLeadsInfo> response) {
                MessageUtils.dismissDialog(dialog);
                if (response.isSuccessful()) {
                    ModelLeadsInfo modelLeadsInfo = response.body();
                    Log.d("sizeOfras", " dfert " + modelLeadsInfo.data);
                    if (modelLeadsInfo.data != null) {
                        if (modelLeadsInfo.isStatus) {
                            mEnquiryScroll.setVisibility(View.VISIBLE);
                            mBottom.setVisibility(View.VISIBLE);
                            mNoDataLayout.setVisibility(View.GONE);
                            //selectCar();

                            if (modelLeadsInfo.data.leadeSources.size() >= 1) {
                                ArrayList<String> strings = new ArrayList<>();
                                strings.add("Select Lead Source");
                                for (ModelLeadsInfo.ResultObj.LeadData leadeSource : modelLeadsInfo.data.leadeSources) {
                                    strings.add(leadeSource.leadsource);
                                }

                                mSpLeadSource.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, strings));
                            }

                            if (modelLeadsInfo.data.vehiclemodel.size() >= 1) {
                                ArrayList<String> vehicleModelArr = new ArrayList<>();
                                vehicleModelArr.add("Select Vehicle Model");
                                for (ModelLeadsInfo.ResultObj.VehicleData vehicleModel : modelLeadsInfo.data.vehiclemodel) {
                                    vehicleModelArr.add(vehicleModel.vehicle_model);
                                }
                                mSpVehicleModel.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, vehicleModelArr));
                            }
/*
                            if (modelLeadsInfo.data.tasks.size() >= 1) {

                                for (ModelLeadsInfo.Tasks tasks : modelLeadsInfo.data.tasks) {
                                    //if (selection.equals(tasks.task_type)) {
                                    taskArray.add(tasks.task);
                                    //}
                                }
                                taskArray.add("Others");
                            }
                            */
                            try {
                                if (modelLeadsInfo.data.states.size() >= 1) {
                                    ModelLeadsInfo.States states = new ModelLeadsInfo.States();
                                    states.state_code = "NA";
                                    states.state_name = "Select State";
                                    states.id = "0";
                                    states.tin_number = "NA";
                                    modelLeadsInfo.data.states.add(0, states);

                                    for (ModelLeadsInfo.States state : modelLeadsInfo.data.states) {
                                        statesArray.add(state.state_name);
                                    }
                                    mSpState.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, statesArray));

                                    //mCityAuto.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, cityArray));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            ArrayList<String> serviceIssue = new ArrayList<>();
                            for (ModelLeadsInfo.ServiceIssue serviceIssue1 : modelLeadsInfo.data.serviceIssues) {
                                serviceIssue.add(serviceIssue1.service_issue);
                            }

                            if (serviceIssue.size() == 0) {
                                serviceIssue.add(noServiceIssue);
                            } else {
                                serviceIssue.add(0, "Select Service Issue");
                            }
                            mServiceIssueSp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_dialog, R.id.text1, serviceIssue));


                        } else {
                            mEnquiryScroll.setVisibility(View.GONE);
                            mBottom.setVisibility(View.GONE);
                            mNoDataLayout.setVisibility(View.VISIBLE);
                            snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, modelLeadsInfo.message);
                        }
                    } else {
                        mEnquiryScroll.setVisibility(View.GONE);
                        mNoDataLayout.setVisibility(View.VISIBLE);
                        mBottom.setVisibility(View.GONE);
                        snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Try again, Because leads not found");
                    }
                } else {
                    snackbar = MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                }
            }

            @Override
            public void onFailure(Call<ModelLeadsInfo> call, Throwable t) {
                Log.d("tsdsdsds", "" + t.getMessage());
                MessageUtils.dismissDialog(dialog);
                mEnquiryScroll.setVisibility(View.GONE);
                mBottom.setVisibility(View.GONE);
                mNoDataLayout.setVisibility(View.VISIBLE);
                snackbar = MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
            }
        });
    }


    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.SEND_SMS);
        int locationPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (CallLogDate.NUMBER == "") {
            mName.requestFocus();
            mMobileNo.setMaxEms(10);
        } else {
            mMobileNo.setMaxEms(20);
            mDate.setText("" + CallLogDate.DATE);
            mTime.setText("" + CallLogDate.TIME);
            mMobileNo.setText("" + CallLogDate.NUMBER);
            int email = mEmail.getText().toString() != null ? mEmail.getText().toString().length() : 0;
            mEmail.setSelection(email);
            mEmail.requestFocus();
        }

        if (CallLogDate.CONTACT_NAME.isEmpty() && CallLogDate.CONTACT_NAME.equals(UNKNOWN)) {
            mName.setText("");
        } else {
            mName.setText(CallLogDate.CONTACT_NAME);
        }
    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_CALL_LOG)) {
            Toast.makeText(getActivity(), "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        }
        requestPermissions(new String[]{Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_CONTACTS}, LOCATION_PERMISSION_CODE);
    }

    private boolean isReadLocationAllowed() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALL_LOG);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS);
        int CALL_PHONE = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if (result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        //Checking the request code of our request

        if (requestCode == LOCATION_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(getActivity(), CallLogActivity.class);
                startActivity(intent);
            } else {

                //Displaying another toast if permission is not granted

            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("intentsds", "" + resultCode + "  " + resultCode + "  " + data);
        if (resultCode == RESULT_OK) {
            if (data != null) {
                Bundle res = data.getExtras();
                String number = res.getString("number");
                String date = res.getString("date");
                String duration = res.getString("duration");

                Log.d("datedate", "" + number + "  " + date + "  " + duration);
            /*Uri uri = data.getData();

            if (uri != null) {
                Cursor c = null;
                try {
                    c = getContentResolver().query(uri, new String[]{
                                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                                    ContactsContract.CommonDataKinds.Phone.TYPE},
                            null, null, null);

                    if (c != null && c.moveToFirst()) {
                        String number = c.getString(0);
                        int type = c.getInt(1);
                        showSelectedNumber(type, number);
                    }
                } finally {
                    if (c != null) {
                        c.close();
                    }
                }
            }*/
            }
        }
    }


    private void showDateDailog(final int datePic) {

        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDate) {

                year = selectedYear;
                month = selectedMonth;
                day = selectedDate;
                SimpleDateFormat dateFormatter = new SimpleDateFormat(DateUtils.DD_MM_YYYY, Locale.US);
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, day);
                if (datePic == 1) {
                    mDate.setText(dateFormatter.format(newDate.getTime()));
                } else if (datePic == 2) {
                    mDateTwo.setText(dateFormatter.format(newDate.getTime()));
                } else {
                    mEdtSelfDate.setText(dateFormatter.format(newDate.getTime()));
                }
                Calendar calendar = Calendar.getInstance();
                String time = mTime.getText().toString();
                if (time == "") {
                    TimePickerDialog d = new TimePickerDialog(getActivity(), getTimeFromPicker(Enquiry.this, mTime), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                    d.getWindow().setWindowAnimations(R.style.grow);
                    d.show();
                }

            }
        }, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.getWindow().setWindowAnimations(R.style.grow);
        datePickerDialog.show();
    }

    private boolean validateCredentials(String name, String email, String mobileNo, String
            address, String date, String time, String message, String car, String selectionNew, String
                                                leadSource) {
        Log.d("leadSource", "" + leadSource);
        if (name.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Name can't be empty");
            return false;
        } else if (mobileNo.isEmpty() || mobileNo.length() < 10) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Mobile Number can't be empty");
            return false;
        } else if (date.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Date can't be empty");
            return false;
        } else if (time.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Time can't be empty");
            return false;
        } else if (email.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Email ID can't be empty");
            return false;
        } else if (!Pattern.matches(patternStr, email)) {
            snackbar = MessageUtils.showSnackBar(getActivity(), mSnackView, "Invalid Email ID");
            return false;
        } else if (leadSource.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Select Lead Source");
            return false;
        } else if (selectionNew.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Select EnquiryModel For");
            return false;
        } else if (car.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Choose New");
            return false;
        } else if (message.isEmpty()) {
            snackbar = showSnackBar(getActivity(), mSnackView, "Message can't be empty");
            return false;
        }
        return true;
    }


    private TimePickerDialog.OnTimeSetListener getTimeFromPicker(Enquiry Enquiry, final TextView mTime) {
        TimePickerDialog.OnTimeSetListener timeEndSetListener = new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Log.d("TimefromTimePicker", "" + view + " " + hourOfDay + "  " + minute);

                String AM_PM = " AM";
                String mm_precede = "";
                int hourOfDayPlusTwo = hourOfDay + 2;
                //if (hourOfDay >= 9 && 22 > hourOfDay) {
                if (hourOfDay >= 12) {
                    AM_PM = " PM";
                    if (hourOfDay >= 13 && hourOfDay < 24) {
                        hourOfDay -= 12;
                    } else {
                        hourOfDay = 12;
                    }
                } else if (hourOfDay == 0) {
                    hourOfDay = 12;
                }
                if (minute < 10) {
                    mm_precede = "0";
                }

                mTime.setText("" + hourOfDay + ":" + mm_precede + minute + "" + AM_PM);


            }
        };

        return timeEndSetListener;
    }


    public static long getTimeInMilliSeconds(String date) {

        SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_DD_MM_YYYY_HH_MM_AP);
        long timeInMilliseconds = 0;
        try {
            Date mDate = sdf.parse(date);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    public void getDateFromPicker(final FragmentActivity activity, final TextView mDate,
                                  final Calendar calendar) {
        datePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                SimpleDateFormat dateFormatter = new SimpleDateFormat(DateUtils.DD_MM_YYYY, Locale.US);
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat mSetDate = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
                String mCurrentDate = mSetDate.format(new Date());
                Date dateCurrent = null;
                try {
                    dateCurrent = mSetDate.parse(mCurrentDate);
                    datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String apponitentDate = dateFormatter.format(newDate.getTime());
                Date d1 = null;
                mDate.setText(String.valueOf(apponitentDate));
              /*  try {
                    d1 = dateFormatter.parse(apponitentDate);
                    int isSunday = UtilsDate.getSundays(d1, d1);
                    if (isSunday == 0) {
                        mDate.setText(String.valueOf(apponitentDate));
                    } else {
                        UIUtils.showSnackBar(activity, mView, activity.getString(R.string.workings_days), activity.getColor(R.color.red));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }*/

            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isSelect = false;
        CallLogDate.NUMBER = "";
        CallLogDate.DURATION = "";
        CallLogDate.DATE = "";
        CallLogDate.TIME = "";
        CallLogDate.CONTACT_NAME = "";
        if (snackbar != null) {
            snackbar.dismiss();
        }

        MessageUtils.dismissDialog(dialog);
    }


    class AdapterState extends BaseAdapter {

        private final ArrayList<ModelLeadsInfo.States> states;
        private final FragmentActivity context;

        // R.layout.item_dialog, R.id.text1
        public AdapterState(FragmentActivity activity, ArrayList<ModelLeadsInfo.States> states) {
            this.states = states;
            this.context = activity;
        }

        @Override
        public int getCount() {
            return states.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_dialog, viewGroup, false);
            }

            // get the TextView for item name and item description
            TextView textViewItemName = (TextView)
                    convertView.findViewById(R.id.text1);


            //sets the text for item name and item description from the current item object
            textViewItemName.setText(states.get(i).state_name);

            /*convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("leadsasasa", mSpState.getSelectedItem().toString());
                    if (!"Select State".equals(mSpState.getSelectedItem().toString())) {
                        mState.setText("" + mSpState.getSelectedItem().toString());
                    } else {
                        mState.setText(" ");
                    }
                }
            });*/
            return convertView;
        }
    }
}

