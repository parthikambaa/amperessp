package com.ampere.bike.ampere.model.invoiceGenerate.chassisNumber

data class Data(
        val chassis: List<Chassi>
)