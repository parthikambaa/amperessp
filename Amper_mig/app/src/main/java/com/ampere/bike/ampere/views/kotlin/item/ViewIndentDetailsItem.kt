package com.ampere.bike.ampere.views.kotlin.item

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.model.indent.history.IndentdetailsItem
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.item_indent_details.view.*

class ViewIndentDetailsItem(var activity: FragmentActivity?, var indentdetails: List<IndentdetailsItem>) : RecyclerView.Adapter<ViewIndentDetailsItem.HoldeIndent>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoldeIndent {
        return HoldeIndent(View.inflate(activity, R.layout.item_indent_details, null))
    }

    override fun getItemCount(): Int {
        return indentdetails.size
    }

    override fun onBindViewHolder(holder: HoldeIndent, position: Int) {
        holder.spareName.text = indentdetails[position].spareName
        holder.itemDate.text = DateUtils.convertDates(indentdetails[position].createdAt, "yyyy-MM-dd hh:mm:ss", "dd MMM yy")
        holder.modelNo.text = indentdetails[position].vehicleModel
        holder.qty.text = indentdetails[position].qty
        holder.i_type.text = indentdetails[position].indentType

    }

    class HoldeIndent(view: View) : RecyclerView.ViewHolder(view) {
        val spareName = view.in_spare_name
        val itemDate = view.in_date
        val modelNo = view.in_model_no
        val qty = view.in_qty
        val i_type = view.in_indent_type
        val snack_view = view.in_snack_view
    }

}
