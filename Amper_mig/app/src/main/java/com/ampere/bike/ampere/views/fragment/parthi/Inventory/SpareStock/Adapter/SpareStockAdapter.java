package com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.Pojos.SpareStockPojos;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class SpareStockAdapter extends RecyclerView.Adapter<SpareStockAdapter.ViewHolder> {
    ArrayList<SpareStockPojos> spareStockPojosArrayList;
    Context context;

    public SpareStockAdapter(ArrayList<SpareStockPojos> spareStockPojosArrayList, Context context) {
        this.spareStockPojosArrayList = spareStockPojosArrayList;
        this.context = context;
    }

    public  void setList(  ArrayList<SpareStockPojos> spareStockPojosArrayList){
        this.spareStockPojosArrayList = spareStockPojosArrayList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_inventory_spare_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.srlNo.setText(String.valueOf(position+1));
        holder.spareID.setText(String.valueOf(spareStockPojosArrayList.get(position).getId()));
        holder.spareVariant.setText(spareStockPojosArrayList.get(position).getModelVarient());
        holder.spareName.setText(spareStockPojosArrayList.get(position).getSpareName());
        holder.spareInvoiceDate.setText(spareStockPojosArrayList.get(position).getIndentInvoiceDate());
        holder.spareStatus.setText(spareStockPojosArrayList.get(position).getStatus());

    }

    @Override
    public int getItemCount() {
        return spareStockPojosArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView srlNo,spareID,spareName,spareInvoiceDate,spareStatus,spareVariant;
        public ViewHolder(View itemView) {
            super(itemView);
            srlNo =itemView.findViewById(R.id.srl_NUM);
            spareID =itemView.findViewById(R.id.spare_id);
            spareName =itemView.findViewById(R.id.spare_name);
            spareInvoiceDate = itemView.findViewById(R.id.spare_invoice_date);
            spareStatus =itemView.findViewById(R.id.spare_status);
            spareVariant =itemView.findViewById(R.id.spare_variant);
        }
    }
}
