package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.Pojos.Warrantycomplaint;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class WarrantyComplaintsAdapter extends RecyclerView.Adapter<WarrantyComplaintsAdapter.Viewholder> {
    Context context;
    ArrayList<Warrantycomplaint> warrantycomplaintsArrayList;
    ConfigClickEvent configposition;

    public WarrantyComplaintsAdapter(Context context, ArrayList<Warrantycomplaint> warrantycomplaintsArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.warrantycomplaintsArrayList = warrantycomplaintsArrayList;
        this.configposition = configposition;
    }

    public WarrantyComplaintsAdapter() {
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_warranty_complaints, parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, final int position) {
        String i = String.valueOf((position+1));
        holder.sNo.setText(i);
        holder.chasNo.setText(warrantycomplaintsArrayList.get(position).getChassisNo());
        holder.dAte.setText(warrantycomplaintsArrayList.get(position).getApprovedDate());
        holder.sErialNo.setText(warrantycomplaintsArrayList.get(position).getComplaint());
        holder.sTatus.setText(warrantycomplaintsArrayList.get(position).getStatus());
        holder.sErialNo.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.sErialNo.setSingleLine(true);
        holder.sErialNo.setMarqueeRepeatLimit(2);
        holder.sErialNo.setSelected(true);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configposition.connectposition(position,warrantycomplaintsArrayList.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return warrantycomplaintsArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView sNo,chasNo,dAte,sErialNo,sTatus;
        RelativeLayout relativeLayout;

        public Viewholder(View itemView) {
            super(itemView);
            sNo = itemView.findViewById(R.id.s_no);
            chasNo =itemView.findViewById(R.id.chas_no);
            dAte =itemView.findViewById(R.id.date);
            sErialNo =itemView.findViewById(R.id.cmplnt);
            sTatus=itemView.findViewById(R.id.status);
            relativeLayout=itemView.findViewById(R.id.wranty_cmplnt_position);
        }
    }
}
