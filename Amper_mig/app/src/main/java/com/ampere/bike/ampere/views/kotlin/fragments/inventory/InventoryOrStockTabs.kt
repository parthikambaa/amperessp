package com.ampere.bike.ampere.views.kotlin.fragments.inventory

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.utils.MyUtils.*
import com.ampere.bike.ampere.views.kotlin.model.customersales.Data
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.InventorySpareModel
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle.InventoryModel
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.frag_inventory.*
import kotlinx.android.synthetic.main.frag_inventory_tabs.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class InventoryOrStockTabs : Fragment() {
    var viewItemType: String = ""

    /*  @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
      override fun onCreate(savedInstanceState: Bundle?) {
          super.onCreate(savedInstanceState)
          setContentView(R.layout.frag_inventory_tabs)
          supportActionBar?.setTitle(VIEW_ITEM)
          supportActionBar?.setHomeButtonEnabled(true)
          supportActionBar?.setDisplayHomeAsUpEnabled(true)

          //supportActionBar?.setStackedBackgroundDrawable(getDrawable(R.drawable.ic_arrow_back))

          var bundle = intent
          var viewItemType: String = bundle.getStringExtra("viewItemType")
          Log.d("viewItemTypesdsd", "" + viewItemType);

          setTabLayouts()
          if (viewItemType.equals("Vehicle")) {
              loadInventoryDetailsForVehicle()
          } else {
              onInventoryDetailsForSpare()
          }

      }*/

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        var bundle = arguments
        viewItemType = bundle!!.getString("viewItemType")
        (activity as CustomNavigationDuo).disbleNavigationView(viewItemType)
        return inflater.inflate(R.layout.frag_inventory_tabs, null)

    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTabLayouts()
        if (viewItemType.equals("Vehicle")) {
            loadInventoryDetailsForVehicle()
        } else {
            onInventoryDetailsForSpare()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        super.onOptionsItemSelected(item)
        if (item?.getItemId() == android.R.id.home) {
            activity!!.finish()
            return true
        } else {
            return false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }


    private fun onInventoryDetailsForSpare() {
        var map: HashMap<String, String> = HashMap()
        map.put("username", "" + LocalSession.getUserInfo(activity!!, LocalSession.KEY_ID))
        var models = MyUtils.getRetroService().onInventoryDetailsForSpare(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
        var dialog = MessageUtils.showDialog(activity!!)
        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            models.cancel()
            MessageUtils.dismissDialog(dialog);
            false
        }
        models.clone().enqueue(object : Callback<InventorySpareModel> {
            override fun onResponse(call: Call<InventorySpareModel>?, response: Response<InventorySpareModel>?) {
                MessageUtils.dismissDialog(dialog);

                if (response != null) {
                    if (response.isSuccessful) {
                        var stockDetailsModel: InventorySpareModel = response.body()!!
                        if (stockDetailsModel != null) {

                            if (stockDetailsModel.success) {
                                DISPATCHED_VHICLE.clear()
                                DELIVER_VEHICLE.clear()

                                DISPATCHED_SPARE = stockDetailsModel.data.sparedespatched
                                DELIVER_SPARE = stockDetailsModel.data.sparedelivered
/*
                                for (inss: Int in 0..20) {
                                    DELIVER_SPARE.add(DELIVER_SPARE[0])
                                    DISPATCHED_SPARE.add(DISPATCHED_SPARE[0])
                                }*/
                                Log.d("DELIVER_SPARE_++++++++", "" + DISPATCHED_SPARE.size + " DELIVER_SPARE " + DELIVER_SPARE.size)
                                setTabLayouts()

                            }
                        }
                    } else {
                        var msg = MessageUtils.showErrorCodeToast(response.code())

                    }
                } else {
                    MessageUtils.showSnackBar(activity!!.applicationContext, invent_snack_view, getString(R.string.api_response_not_found))
                }
            }


            override fun onFailure(call: Call<InventorySpareModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);
                var msg = MessageUtils.showFailureToast(t!!.message)
                MessageUtils.showSnackBar(activity!!.applicationContext, invent_snack_view, msg)
            }

        })
    }

    @SuppressLint("ResourceType")
    private fun setTabLayouts() {
        Log.d("sdsds", "asddsds")
        val fragmentAdapter = InventoryTab(activity!!.supportFragmentManager)
        invent_viewpager_main.adapter = fragmentAdapter
        invent_tabs_main.setupWithViewPager(invent_viewpager_main)
        invent_tabs_main.rotationX = -1F

        for (i: Int in 0..invent_tabs_main.getTabCount() - 1) {
            //noinspection ConstantConditions
            var tv = TextView(activity!!)
            tv.setTypeface(MyUtils.getTypeface(activity!!, "fonts/Lato-Bold.ttf"));
            if (i == 0) {
                tv.setText("PENDING")
            } else {
                tv.setText("STOCKS")
            }

            tv.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            //tv.setTextColor(R.color.white)
            tv.gravity = Gravity.CENTER
            tv.rotationX = 180F

            invent_tabs_main.getTabAt(i)!!.setCustomView(tv);

        }

        //invent_tabs_main.setTabTextColors(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector));
    }


    private fun loadInventoryDetailsForVehicle() {
        var map: HashMap<String, String> = HashMap()
        map.put("username", "" + LocalSession.getUserInfo(activity!!, LocalSession.KEY_ID))
        var models = MyUtils.getRetroService().onInventoryDetailsForVehicle(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
        var dialog = MessageUtils.showDialog(activity!!)
        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            models.cancel()
            MessageUtils.dismissDialog(dialog);
            false
        }

        models.clone().enqueue(object : retrofit2.Callback<InventoryModel> {

            override fun onResponse(call: Call<InventoryModel>?, response: Response<InventoryModel>?) {
                MessageUtils.dismissDialog(dialog);

                if (response != null) {
                    if (response.isSuccessful) {
                        var stockDetailsModel: InventoryModel = response.body()!!
                        if (stockDetailsModel != null) {

                            if (stockDetailsModel.success) {
                                DISPATCHED_VHICLE.clear()
                                DELIVER_VEHICLE.clear()

                                DISPATCHED_VHICLE = stockDetailsModel.data.vehicledespatched
                                DELIVER_VEHICLE = stockDetailsModel.data.vehicledelivered

                               /* for (inss: Int in 0..30) {
                                    DELIVER_VEHICLE.add(DELIVER_VEHICLE[0])
                                    DISPATCHED_VHICLE.add(DISPATCHED_VHICLE[0])
                                }*/


                                Log.d("DISPATCHED_VHICLE", "" + DISPATCHED_VHICLE.size + " now " + DELIVER_VEHICLE.size)

                                setTabLayouts()

                            }
                        }
                    } else {
                        var msg = MessageUtils.showErrorCodeToast(response.code())

                    }
                } else {
                    MessageUtils.showSnackBar(activity!!.applicationContext, invent_snack_view, getString(R.string.api_response_not_found))
                }
            }

            override fun onFailure(call: Call<InventoryModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);
                var msg = MessageUtils.showFailureToast(t!!.message)
                MessageUtils.showSnackBar(activity!!.applicationContext, invent_snack_view, msg)
                /* invent_no_data.text = msg
                 invent_no_data.visibility = View.VISIBLE*/
            }
        })
    }

    class ItemCustomerSales(val activity: FragmentActivity, val stockItems: List<Data>) : RecyclerView.Adapter<ItemCustomerSale>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCustomerSale {
            return ItemCustomerSale(View.inflate(activity, R.layout.item_crm_view_details, null))
        }

        override fun getItemCount(): Int {
            return stockItems.size
        }

        override fun onBindViewHolder(holder: ItemCustomerSale, position: Int) {

        }

    }

    class ItemCustomerSale(view: View) : RecyclerView.ViewHolder(view) {

    }

    class InventoryTab(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            Log.d("view_items", "stock Detail " + position)

            return when (position) {
                0 -> {
                    return StocksAcceptFragment()
                }
                1 -> {
                    return PurchaseStocks()
                }
                else -> {
                    return PurchaseStocks()
                }
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> {
                    return "Stocks"
                }
                1 -> {
                    return "Purchase Stocks"
                }
                else -> {
                    return "Purchase Stocks"
                }
            }
        }
    }
}