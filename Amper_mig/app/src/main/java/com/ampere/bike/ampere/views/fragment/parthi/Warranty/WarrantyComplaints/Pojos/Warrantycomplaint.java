package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Warrantycomplaint {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("enquiry_id")
    @Expose
    private Integer enquiryId;
    @SerializedName("chassis_no")
    @Expose
    private String chassisNo;
    @SerializedName("complaint")
    @Expose
    private String complaint;
    @SerializedName("defect_item")
    @Expose
    private String defectItem;
    @SerializedName("defect_qty")
    @Expose
    private String defectQty;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("sent_by")
    @Expose
    private String sentBy;
    @SerializedName("sent_date")
    @Expose
    private String sentDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("replacement_type")
    @Expose
    private String replacementType;
    @SerializedName("approved_date")
    @Expose
    private String approvedDate;
    @SerializedName("approved_by")
    @Expose
    private String approvedBy;
    @SerializedName("replacement_approval_by")
    @Expose
    private String replacementApprovalBy;
    @SerializedName("repalcement_approval_date")
    @Expose
    private String repalcementApprovalDate;
    @SerializedName("service_status")
    @Expose
    private String serviceStatus;
    @SerializedName("sent_status")
    @Expose
    private String sentStatus;
    @SerializedName("invoice_status")
    @Expose
    private String invoiceStatus;
    @SerializedName("invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("inventory_id")
    @Expose
    private String inventoryId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("complaintstatus")
    @Expose
    private String complaintstatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(Integer enquiryId) {
        this.enquiryId = enquiryId;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getDefectItem() {
        return defectItem;
    }

    public void setDefectItem(String defectItem) {
        this.defectItem = defectItem;
    }

    public String getDefectQty() {
        return defectQty;
    }

    public void setDefectQty(String defectQty) {
        this.defectQty = defectQty;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSentBy() {
        return sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getReplacementType() {
        return replacementType;
    }

    public void setReplacementType(String replacementType) {
        this.replacementType = replacementType;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getReplacementApprovalBy() {
        return replacementApprovalBy;
    }

    public void setReplacementApprovalBy(String replacementApprovalBy) {
        this.replacementApprovalBy = replacementApprovalBy;
    }

    public String getRepalcementApprovalDate() {
        return repalcementApprovalDate;
    }

    public void setRepalcementApprovalDate(String repalcementApprovalDate) {
        this.repalcementApprovalDate = repalcementApprovalDate;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getSentStatus() {
        return sentStatus;
    }

    public void setSentStatus(String sentStatus) {
        this.sentStatus = sentStatus;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getComplaintstatus() {
        return complaintstatus;
    }

    public void setComplaintstatus(String complaintstatus) {
        this.complaintstatus = complaintstatus;
    }

}