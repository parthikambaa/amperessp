package com.ampere.bike.ampere.views.fragment.parthi.Indent.SpareDispatch.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpareDispatchPojos {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("chassis_no")
    @Expose
    private String chassisNo;
    @SerializedName("dispatch_type")
    @Expose
    private String dispatchType;
    @SerializedName("docket")
    @Expose
    private String docket;
    @SerializedName("vide")
    @Expose
    private String vide;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("driver_no")
    @Expose
    private String driverNo;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("model_varient")
    @Expose
    private String modelVarient;
    @SerializedName("spare_name")
    @Expose
    private String spareName;
    @SerializedName("serial_no")
    @Expose
    private String serialNo;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("indent_invoice_date")
    @Expose
    private String indentInvoiceDate;
    @SerializedName("manufacture_date")
    @Expose
    private String manufactureDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getDispatchType() {
        return dispatchType;
    }

    public void setDispatchType(String dispatchType) {
        this.dispatchType = dispatchType;
    }

    public String getDocket() {
        return docket;
    }

    public void setDocket(String docket) {
        this.docket = docket;
    }

    public String getVide() {
        return vide;
    }

    public void setVide(String vide) {
        this.vide = vide;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverNo() {
        return driverNo;
    }

    public void setDriverNo(String driverNo) {
        this.driverNo = driverNo;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getModelVarient() {
        return modelVarient;
    }

    public void setModelVarient(String modelVarient) {
        this.modelVarient = modelVarient;
    }

    public String getSpareName() {
        return spareName;
    }

    public void setSpareName(String spareName) {
        this.spareName = spareName;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getIndentInvoiceDate() {
        return indentInvoiceDate;
    }

    public void setIndentInvoiceDate(String indentInvoiceDate) {
        this.indentInvoiceDate = indentInvoiceDate;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
