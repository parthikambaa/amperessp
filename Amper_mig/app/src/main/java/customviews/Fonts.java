package customviews;


public interface Fonts {
    String REGULAR = "fonts/Lato-Regular.ttf";
    String MEDIUM = "fonts/Lato-Medium.ttf";
    String BOLD = "fonts/Lato-Bold.ttf";
    String LIGHT = "fonts/Lato-Light.ttf";
}
