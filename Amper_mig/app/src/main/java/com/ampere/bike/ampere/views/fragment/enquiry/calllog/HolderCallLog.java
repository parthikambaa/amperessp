package com.ampere.bike.ampere.views.fragment.enquiry.calllog;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ampere.vehicles.R;


/**
 * Created by AND I5 on 04-10-2017.
 */
public class HolderCallLog extends RecyclerView.ViewHolder {
    public TextView mNumber, mDate, mDuration, mCallName;
    public ImageView mDir;
    public LinearLayout mLayout;

    public HolderCallLog(View view) {
        super(view);
        mNumber = (TextView) view.findViewById(R.id.item_call_log_number);
        mDate = (TextView) view.findViewById(R.id.item_call_log_date);
        mDuration = (TextView) view.findViewById(R.id.item_call_log_duration);
        mDir = (ImageView) view.findViewById(R.id.item_call_log_dir);
        mLayout = (LinearLayout) view.findViewById(R.id.item_call_log_layout);
        mCallName = view.findViewById(R.id.item_call_log_name);
    }
}
