package com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare

import java.io.Serializable

data class Sparedespatched(
        val id: Int,
        val spare_name: String,
        val serial_no: String,
        val qty: String,
        val indent_invoice_date: String,
        val manufacture_date: String,
        val status: String,
        val acceptance: String,
        val user_id: Int,
        val delivered_by: Int,
        val created_at: String,
        val updated_at: String, val isCheck: Boolean
) : Serializable