package com.ampere.bike.ampere.model.task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 */
public class ViewTaskModel {
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("success")
    @Expose
    public Boolean success;
    @SerializedName("message")
    @Expose
    public String message;
}