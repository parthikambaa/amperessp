package com.ampere.bike.ampere.model.calendar;

public class ModelDateSelection {
    private final String id, updatedAt, customerName, mobileNo, city, email, enquiryDate, enquiryTime, leadsource, vehicleModel, enquiryFor, chassisNo, message, assignedTo,
            assignedPerson, assignedDate, task, taskStatus, createdBy;
    private boolean isChange;

    public String getId() {
        return id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getCity() {
        return city;
    }

    public String getEmail() {
        return email;
    }

    public String getEnquiryDate() {
        return enquiryDate;
    }

    public String getEnquiryTime() {
        return enquiryTime;
    }

    public String getLeadsource() {
        return leadsource;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public String getEnquiryFor() {
        return enquiryFor;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public String getMessage() {
        return message;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public String getAssignedPerson() {
        return assignedPerson;
    }

    public String getAssignedDate() {
        return assignedDate;
    }

    public String getTask() {
        return task;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public String getCreatedBy() {
        return createdBy;
    }
    public boolean isChecked() {
        return isChange;
    }

    public ModelDateSelection(String id, String customerName, String mobileNo, String city, String email, String enquiryDate, String enquiryTime, String leadsource,
                              String enquiryFor, String vehicleModel, String chassisNo, String message, String assignedTo, String assignedPerson, String assignedDate,
                              String task, String taskStatus, String createdBy, String updatedAt, boolean isChange) {

        this.id = id;
        this.customerName = customerName;
        this.mobileNo = mobileNo;
        this.city = city;
        this.email = email;
        this.enquiryDate = enquiryDate;
        this.enquiryTime = enquiryTime;
        this.leadsource = leadsource;
        this.enquiryFor = enquiryFor;
        this.vehicleModel = vehicleModel;
        this.chassisNo = chassisNo;
        this.message = message;
        this.assignedTo = assignedTo;
        this.assignedPerson = assignedPerson;
        this.assignedDate = assignedDate;

        this.task = task;
        this.taskStatus = taskStatus;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.isChange = isChange;

    }
}
