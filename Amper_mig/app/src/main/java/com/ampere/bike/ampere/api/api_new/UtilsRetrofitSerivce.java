package com.ampere.bike.ampere.api.api_new;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UtilsRetrofitSerivce {

    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method);

    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method, @Header("Authorization") String token, @Body Map<String, Object> map);
}

