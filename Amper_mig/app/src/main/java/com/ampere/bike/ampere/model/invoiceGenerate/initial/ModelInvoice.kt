package com.ampere.bike.ampere.model.invoiceGenerate.initial

data class ModelInvoice(
        val success: Boolean,
        val data: Data,
        val message: String
)