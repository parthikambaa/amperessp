package com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm

import com.google.gson.annotations.SerializedName

class StatusModel {

    @SerializedName("data")
    lateinit var data: List<Datum>
    @SerializedName("message")
    lateinit var message: String
    @SerializedName("success")
    var success: Boolean = false

}
