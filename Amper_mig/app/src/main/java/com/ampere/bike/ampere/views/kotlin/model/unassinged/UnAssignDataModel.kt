package com.ampere.bike.ampere.views.kotlin.model.unassinged

data class UnAssignDataModel(
        val enquiry: List<UnAssignEnquiryModel>
)