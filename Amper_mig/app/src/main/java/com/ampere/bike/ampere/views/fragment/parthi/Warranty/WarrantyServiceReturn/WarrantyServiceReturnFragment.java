package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyServiceReturn;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyServiceReturn.Adapter.WarrantyServiceReturnAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyServiceReturn.Pojos.ServiceReturnPojo;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;


/**
 * A simple {@link Fragment} subclass.
 */
public class WarrantyServiceReturnFragment extends Fragment implements ResponsebackToClass,ConfigClickEvent {
RecyclerView warrantyServiceReturnList;
Common common;
LinearLayout linearLayout;
CheckBox checkBox;
Button button;
TextView textView;
ArrayList<ServiceReturnPojo> serviceReturnPojoArrayList  =new ArrayList<>();
ArrayList<ServiceReturnPojo> servicServerArrayList  =new ArrayList<>();

WarrantyServiceReturnAdapter warrantyServiceReturnAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_warranty_service_return, container, false);
        common = new Common(getActivity(),this);

        warrantyServiceReturnList = view.findViewById(R.id.warranty_service_return_list);
        checkBox = view.findViewById(R.id.checKBox);
        button =view.findViewById(R.id.acpt);
        linearLayout =view.findViewById(R.id.accept);
        textView =view.findViewById(R.id.emptyList);
        warrantyServiceReturnList.setLayoutManager(new LinearLayoutManager(getActivity()));
        callApi();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                servicServerArrayList.clear();
                if (serviceReturnPojoArrayList.size() > 0) {
                    if (isChecked) {
                        int i;
                        for ( i = 0; serviceReturnPojoArrayList.size() > i; i++) {
                            serviceReturnPojoArrayList.get(i).setCheck(true);
                        }
                        warrantyServiceReturnAdapter.setList(serviceReturnPojoArrayList);
                        warrantyServiceReturnAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.VISIBLE);
                    } else {

                        for (int i = 0; serviceReturnPojoArrayList.size() > i; i++) {
                            serviceReturnPojoArrayList.get(i).setCheck(false);
                        }

                        warrantyServiceReturnAdapter.setList(serviceReturnPojoArrayList);
                        warrantyServiceReturnAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.GONE);
                    }
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToserver();
            }
        });
        /*view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
                    i.putExtra("SELECTEDVALUE", 0);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);


                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return view;
    }

    private void sendToserver() {
        common.showLoad(true);
        HashMap<String,Object> objectHashMap =new HashMap<>();
        HashMap<String,Object> object =new HashMap<>();
        servicServerArrayList.clear();
        for(int i=0;i<serviceReturnPojoArrayList.size();i++){
            if(serviceReturnPojoArrayList.get(i).isCheck()){
                servicServerArrayList.add(serviceReturnPojoArrayList.get(i));
            }
        }
        object.put("serviceitems",servicServerArrayList);

        objectHashMap.put("serviceitems",object);
        if(common.isNetworkConnected()){
            common.callApiReques("acceptservicereturn","POST",objectHashMap,0);
        }else{
            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();        }
    }

    public void callApi(){
        common.showLoad(true);
        if(common.isNetworkConnected()){
            HashMap<String,String> map = new HashMap<>();
            map.put("userid", LocalSession.getUserInfo(getActivity(),KEY_ID));
            common.callApiRequest("servicereturn","POST",map,0);
        }else{
            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();        }
    }

    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message , boolean success) throws JSONException, IOException {
        common.hideLoad();
       if(objResponse!=null){
           if(method.equals("servicereturn")){
                       serviceReturnPojoArrayList= new Gson().fromJson(objResponse.toString(),new TypeToken<ArrayList<ServiceReturnPojo>>(){}.getType());
                if(serviceReturnPojoArrayList.size()>0){
                    warrantyServiceReturnAdapter =new WarrantyServiceReturnAdapter(getActivity(),serviceReturnPojoArrayList,this);
                    warrantyServiceReturnList.setAdapter(warrantyServiceReturnAdapter);
                }else{
                    textView.setVisibility(View.VISIBLE);
                        textView.setText(message);
                }

           }else{

           }
       }else{
            if(method.equals("acceptservicereturn")){
                startActivity(new Intent(getActivity(), WarrantyHome.class));
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
       }

    }

    @Override
    public void connectposition(int position, int listSize) {
        if(listSize==0){
            linearLayout.setVisibility(View.GONE);
        }else{
            linearLayout.setVisibility(View.VISIBLE);
            button.setText("Accept(" + listSize + ")");
        }
    }
}
