package com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle

data class InventoryModel(
        val success: Boolean,
        val data: Data,
        val message: String
)