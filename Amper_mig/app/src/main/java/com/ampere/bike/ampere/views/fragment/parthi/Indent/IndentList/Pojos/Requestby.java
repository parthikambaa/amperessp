package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.Pojos;

public class Requestby{
	private int depatmentId;
	private String address;
	private String profilePhoto;
	private String address1;
	private String mobile;
	private String active;
	private String createdAt;
	private int statecode;
	private int userType;
	private String updatedAt;
	private String gstNo;
	private String name;
	private int id;
	private String state;
	private String email;
	private String username;
	private Object dealerId;

	public void setDepatmentId(int depatmentId){
		this.depatmentId = depatmentId;
	}

	public int getDepatmentId(){
		return depatmentId;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setProfilePhoto(String profilePhoto){
		this.profilePhoto = profilePhoto;
	}

	public String getProfilePhoto(){
		return profilePhoto;
	}

	public void setAddress1(String address1){
		this.address1 = address1;
	}

	public String getAddress1(){
		return address1;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setActive(String active){
		this.active = active;
	}

	public String getActive(){
		return active;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setStatecode(int statecode){
		this.statecode = statecode;
	}

	public int getStatecode(){
		return statecode;
	}

	public void setUserType(int userType){
		this.userType = userType;
	}

	public int getUserType(){
		return userType;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setGstNo(String gstNo){
		this.gstNo = gstNo;
	}

	public String getGstNo(){
		return gstNo;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public void setDealerId(Object dealerId){
		this.dealerId = dealerId;
	}

	public Object getDealerId(){
		return dealerId;
	}

	@Override
 	public String toString(){
		return 
			"Requestby{" + 
			"depatment_id = '" + depatmentId + '\'' + 
			",address = '" + address + '\'' + 
			",profile_photo = '" + profilePhoto + '\'' + 
			",address1 = '" + address1 + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",active = '" + active + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",statecode = '" + statecode + '\'' + 
			",user_type = '" + userType + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",gst_no = '" + gstNo + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",state = '" + state + '\'' + 
			",email = '" + email + '\'' + 
			",username = '" + username + '\'' + 
			",dealer_id = '" + dealerId + '\'' + 
			"}";
		}
}
