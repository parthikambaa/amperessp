package com.ampere.bike.ampere.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.EnquiryDetails;
import com.ampere.vehicles.R;

import java.util.List;


public class Register extends Fragment {

    BottomSheetBehavior bottomSheetBehavior;
    List<EnquiryDetails> enquiryDetailsMutableList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).enableNavigationView(getString(R.string.nav_register), 0);
        //((CustomNavigationDuo) getActivity()).disableToolbar();
Bundle bundle=new Bundle();

        //ItemForTaskUpdate(getActivity(), bottomSheetBehavior, enquiryDetailsMutableList)
        return inflater.inflate(R.layout.frag_register, container, false);
    }
}
