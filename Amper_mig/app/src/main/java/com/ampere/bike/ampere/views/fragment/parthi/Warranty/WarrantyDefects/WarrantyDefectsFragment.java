package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyDefects;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyDefects.Adapter.WarrantyDefectsAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyDefects.Pojos.DefectsPojo;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;


public class WarrantyDefectsFragment extends Fragment implements ResponsebackToClass,ConfigClickEvent {
   RecyclerView warrantyDefctsList;
   LinearLayout linearLayout;
   WarrantyDefectsAdapter warrantyDefectsAdapter;
   static ArrayList<DefectsPojo> defectsPojoArrayList   = new ArrayList<>();
   static ArrayList<DefectsPojo> defectsServerArrayList   = new ArrayList<>();
   Button button;
   Common common;
   TextView textView;
   CheckBox checkBox;
    Spinner spinner;
    AlertDialog dialog;
    ArrayList<String> sendthrough = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;

    EditText docket,vide,driverName,drMobileNo;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("Activiy","Defects");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.fragment_warranty_defects, container, false);
            warrantyDefctsList=view.findViewById(R.id.warranty_defects_list);
            checkBox = view.findViewById(R.id.MARKall);
            button =view.findViewById(R.id.acpt);
            textView =view.findViewById(R.id.emptyList);
            linearLayout =view.findViewById(R.id.accept);
            common =new Common(getActivity(),this);
        sendthrough.clear();
        sendthrough.add("Send Through");
        sendthrough.add("Courier");
        sendthrough.add("Direct");
        warrantyDefctsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (defectsPojoArrayList.size() > 0) {
                    if (isChecked) {
                        int i;
                        for ( i = 0; defectsPojoArrayList.size() > i; i++) {
                            defectsPojoArrayList.get(i).setCheck(true);
                        }
                        warrantyDefectsAdapter.setList(defectsPojoArrayList);
                        warrantyDefectsAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.VISIBLE);
                    } else {

                        for (int i = 0; defectsPojoArrayList.size() > i; i++) {
                            defectsPojoArrayList.get(i).setCheck(false);
                        }

                        warrantyDefectsAdapter.setList(defectsPojoArrayList);
                        warrantyDefectsAdapter.notifyDataSetChanged();
                   linearLayout.setVisibility(View.GONE);
                    }
                }
            }
        });
/*
* defectreturn
* */
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popmenu();
            }
        });
        callApi();
       /* view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
                    i.putExtra("SELECTEDVALUE", 0);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);


                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return  view;
    }


    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message, boolean sucess) throws JSONException, IOException {
        common.hideLoad();
        if(objResponse!=null){
            if(method.equals("defectreturn")) {
                defectsPojoArrayList = new Gson().fromJson(objResponse.toString(), new TypeToken<ArrayList<DefectsPojo>>() {}.getType());
                if(defectsPojoArrayList.size()>0){
                    textView.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.GONE);
                    warrantyDefectsAdapter = new WarrantyDefectsAdapter(getActivity(), defectsPojoArrayList, this);
                    warrantyDefctsList.setAdapter(warrantyDefectsAdapter);
                    warrantyDefectsAdapter.notifyDataSetChanged();
                }else{
                    textView.setVisibility(View.VISIBLE);
                    linearLayout.setVisibility(View.GONE);
                    textView.setText(message);
                }

            }
        }else {
            if(method.equals("senddefectreturn")){
                 if(dialog.isShowing()){
                    dialog.dismiss();
                }else{
                    dialog.dismiss();

                }
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                 startActivity(new Intent(getActivity(), WarrantyHome.class ));
            }
        }
    }

    @Override
    public void connectposition(int position,int listSize) {
        if(listSize==0){
            linearLayout.setVisibility(View.GONE);
        }else{
            linearLayout.setVisibility(View.VISIBLE);
            button.setText("Accept(" + listSize + ")");
        }

    }

    public void callApi(){
        common.showLoad(true);
        if(common.isNetworkConnected()){
            HashMap<String,String> map = new HashMap<>();
            map.put("userid", LocalSession.getUserInfo(getActivity(),KEY_ID));
            common.callApiRequest("defectreturn","POST",map,0);
        }else{
            common.hideLoad();
            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();        }
    }

   public void sendToDefects() throws JSONException {
       common.showLoad(true);
            HashMap<String,Object> map1  =new HashMap<>();
            HashMap<String,Object> map2  =new HashMap<>();
       for(int h=0;h<defectsPojoArrayList.size();h++){
           if(defectsPojoArrayList.get(h).isCheck()){
               defectsServerArrayList.add(defectsPojoArrayList.get(h));
           }
       }
            map2.put("defects",defectsServerArrayList);
            map1.put("defects",map2);
            map1.put("userid",LocalSession.getUserInfo(getActivity(),KEY_ID));
       map1.put("sent_type",spinner.getSelectedItem().toString());
       if(spinner.getSelectedItem().equals("Direct")){
           Log.v("spinnerSelectedItem",""+spinner.getSelectedItem());

           if(common.isNetworkConnected()){
               common.callApiReques("senddefectreturn","POST",map1,0);
           }else{
               common.hideLoad();
               Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();           }
       }else{
           if(docket.getText() == null ||  docket.getText().toString().matches("")) {
               docket.setError("Required");
           }else{
               if (vide.getText().toString().matches("") || vide.getText() == null) {
                   vide.setError("Required");

               } else {
                   if (drMobileNo.getText().toString().matches("") ||
                           drMobileNo.getText() == null || drMobileNo.getText().length() !=10) {
                       drMobileNo.setError("Required");
                   } else {
                       if (driverName.getText().toString().matches("") || driverName.getText() == null) {
                           driverName.setError("Required");
                       } else {
                           map1.put("docket", docket.getText().toString());
                           map1.put("vide", vide.getText().toString());
                           map1.put("Driver_phone_no", drMobileNo.getText().toString());
                           map1.put("driver_name", driverName.getText().toString());
                           if(common.isNetworkConnected()){
                               common.callApiReques("senddefectreturn","POST",map1,0);
                           }else{
                               Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();                           }
                       }
                   }
               }
           }
       }


    }
    public  void  popmenu(){
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_wanty_defects_retn, null);
        spinner = view.findViewById(R.id.sendthrough);
        final LinearLayout linearLayout = view.findViewById(R.id.courier);
        docket =view.findViewById(R.id.docket);
        vide =view.findViewById(R.id.vide);
        driverName =view.findViewById(R.id.dr_name);
        drMobileNo =view.findViewById(R.id.mob_no);
        final Button sendToPlant = view.findViewById(R.id.snd_plnt);
        final Button close = view.findViewById(R.id.close);
        AlertDialog.Builder alertDialogBuilder =new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        arrayAdapter = new ArrayAdapter<String>(getActivity(),R.layout.warranty_complaint_spin,sendthrough);
        dialog = alertDialogBuilder.create();
        spinner.setAdapter(arrayAdapter);
        dialog.show();

        sendToPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    sendToDefects();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.isShowing()){
                    dialog.dismiss();

                }else{
                    dialog.dismiss();

                }
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String selectstring =spinner.getSelectedItem().toString();
                if(selectstring.equals("Courier")){
                    linearLayout.setVisibility(View.VISIBLE);

                }else{
                    linearLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}
