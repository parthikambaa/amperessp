package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.SerializedName;


public class TaskDetailsModel {

    @SerializedName("data")
    public Data data;

    @SerializedName("success")
    public boolean success;

    @SerializedName("message")
    public String message;



    @Override
    public String toString() {
        return
                "TaskDetailsModel{" +
                        "data = '" + data + '\'' +
                        ",success = '" + success + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }
}