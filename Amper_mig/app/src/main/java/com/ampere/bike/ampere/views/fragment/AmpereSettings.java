package com.ampere.bike.ampere.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.vehicles.R;

import java.util.ArrayList;
import java.util.HashMap;


public class AmpereSettings extends Fragment {
    //@BindView(R.id.textin)
    EditText textIn;
    //@BindView(R.id.add)
    Button buttonAdd;
    // @BindView(R.id.container)
    LinearLayout container;
    BottomSheetBehavior bottomSheetBehavior;

    //@BindView(R.id.textout)
    TextView textOut;
    //@BindView(R.id.remove)
    Button buttonRemove;
    ArrayList<HashMap<String, String>> hashMaps = new ArrayList<>();
    private boolean isClick = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(getString(R.string.nav_settings));
        return inflater.inflate(R.layout.frag_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //ButterKnife.bind(this, view);
        textIn = (EditText) view.findViewById(R.id.textin);
        buttonAdd = (Button) view.findViewById(R.id.add);

        container = (LinearLayout) view.findViewById(R.id.container);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String input = textIn.getText().toString().trim();
                if (!input.isEmpty()) {
                    textIn.setText("");
                    HashMap<String, String> stringStringHashMap = new HashMap<>();
                    stringStringHashMap.put("input", input);
                    hashMaps.add(stringStringHashMap);

                    Log.d("hashMaps", "" + hashMaps.size());
                    LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.item_add_view, null);
                    //ButterKnife.bind(this, addView);

                    final TextView textOut = (TextView) addView.findViewById(R.id.textout);
                    final TextView add = (TextView) addView.findViewById(R.id.add);
                    textOut.setText(input);
                    Button buttonRemove = (Button) addView.findViewById(R.id.remove);
                    buttonRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            HashMap<String, String> stringStringHashMap = new HashMap<>();
                            stringStringHashMap.put("input", textOut.getText().toString());
                            hashMaps.remove(stringStringHashMap);

                            ((LinearLayout) addView.getParent()).removeView(addView);

                            //Log.d("hashMaps 22  ", "" + hashMaps.size());

                        }
                    });

                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int input = Integer.parseInt(textOut.getText().toString());
                            int addd = Integer.parseInt(add.getText().toString());

                            //Log.d("addd", "" + addd + "   " + input);
                            if (isClick) {
                                isClick = false;
                                addd = addd - 2;

                                //Log.d("adddsdsds ", "---------  " + addd + "*" + input + "= " + (input * addd));
                                add.setText("" + addd);
                                Toast.makeText(getActivity(), "" + (input * addd), Toast.LENGTH_SHORT).show();
                            } else {
                                addd = addd + 3;
                                //Log.d("adddsdsds ", "+++++++  " + addd + "*" + input + "= " + (input * addd));
                                Toast.makeText(getActivity(), "" + (input * addd), Toast.LENGTH_SHORT).show();
                                isClick = true;
                                add.setText("" + addd);
                            }

                        }
                    });

                    container.addView(addView);


                } else {
                    Toast.makeText(getActivity(), "Enter Values", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}

