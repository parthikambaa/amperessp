package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.SerializedName;


public class TasksItem{

	@SerializedName("task")
	public String task;

	@SerializedName("updated_at")
	public String updatedAt;

	@SerializedName("created_at")
	public String createdAt;

	@SerializedName("id")
	public int id;

	@SerializedName("task_type")
	public String taskType;

	@Override
 	public String toString(){
		return 
			"TasksItem{" + 
			"task = '" + task + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",task_type = '" + taskType + '\'' + 
			"}";
		}
}