package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.SerializedName;

public class AssignedPerson {


    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("username")
    public String username;

    @SerializedName("email")
    public String email;

    @SerializedName("mobile")
    public String mobile;

    @SerializedName("profile_photo")
    public String profile_photo;


    @SerializedName("address")
    public String address;

    @SerializedName("user_type")
    public String user_type;


}
