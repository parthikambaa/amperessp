
package com.ampere.bike.ampere.model.warranty;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Warrantyinvoice {

    @SerializedName("chassis_no")
    private String mChassisNo;
    @SerializedName("dispatch_type")
    private String mDispatchType;
    @SerializedName("docket")
    private String mDocket;
    @SerializedName("driver_name")
    private String mDriverName;
    @SerializedName("driver_no")
    private String mDriverNo;
    @SerializedName("id")
    private Long mId;
    @SerializedName("indent_invoice_date")
    private String mIndentInvoiceDate;
    @SerializedName("manufacture_date")
    private String mManufactureDate;
    @SerializedName("model_varient")
    private String mModelVarient;
    @SerializedName("qty")
    private String mQty;
    @SerializedName("serial_no")
    private String mSerialNo;
    @SerializedName("spare_name")
    private String mSpareName;
    @SerializedName("vehicle_model")
    private String mVehicleModel;
    @SerializedName("vide")
    private String mVide;

    public String getChassisNo() {
        return mChassisNo;
    }

    public void setChassisNo(String chassisNo) {
        mChassisNo = chassisNo;
    }

    public String getDispatchType() {
        return mDispatchType;
    }

    public void setDispatchType(String dispatchType) {
        mDispatchType = dispatchType;
    }

    public String getDocket() {
        return mDocket;
    }

    public void setDocket(String docket) {
        mDocket = docket;
    }

    public String getDriverName() {
        return mDriverName;
    }

    public void setDriverName(String driverName) {
        mDriverName = driverName;
    }

    public String getDriverNo() {
        return mDriverNo;
    }

    public void setDriverNo(String driverNo) {
        mDriverNo = driverNo;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIndentInvoiceDate() {
        return mIndentInvoiceDate;
    }

    public void setIndentInvoiceDate(String indentInvoiceDate) {
        mIndentInvoiceDate = indentInvoiceDate;
    }

    public String getManufactureDate() {
        return mManufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        mManufactureDate = manufactureDate;
    }

    public String getModelVarient() {
        return mModelVarient;
    }

    public void setModelVarient(String modelVarient) {
        mModelVarient = modelVarient;
    }

    public String getQty() {
        return mQty;
    }

    public void setQty(String qty) {
        mQty = qty;
    }

    public String getSerialNo() {
        return mSerialNo;
    }

    public void setSerialNo(String serialNo) {
        mSerialNo = serialNo;
    }

    public String getSpareName() {
        return mSpareName;
    }

    public void setSpareName(String spareName) {
        mSpareName = spareName;
    }

    public String getVehicleModel() {
        return mVehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        mVehicleModel = vehicleModel;
    }

    public String getVide() {
        return mVide;
    }

    public void setVide(String vide) {
        mVide = vide;
    }

}
