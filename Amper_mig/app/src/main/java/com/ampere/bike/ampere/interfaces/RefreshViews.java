package com.ampere.bike.ampere.interfaces;

import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.TaskDetailsModel;

public interface RefreshViews {

    void refresh(TaskDetailsModel taskDetailsModel);

}
