package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.ampere.bike.ampere.model.enquiry.edit.ModelServiceComponets;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Data {

    @SerializedName("enquirydetails")
    public List<EnquiryDetails> enquirydetails;

    @SerializedName("enquiry")
    public TaskEnquiryModel enquiry;

    @SerializedName("tasks")
    public List<TasksItem> tasks;
    @SerializedName("enquirystatuslist")
    public List<EnquiryStatusList> enquirystatuslist;

    @SerializedName("enquirystatus")
    public List<EnquiryStatus> enquiryStatusList;

    @SerializedName("spares")
    public List<Spares> sparesList;

    @SerializedName("sparehistory")
    public List<SpareHistory> spareHistory;

    @SerializedName("warrantycomponents")
    public List<ModelServiceComponets> modelServiceComponets;


    public class EnquiryStatus {
        @SerializedName("id")
        public String id;
        @SerializedName("enquiry_id")
        public String enquiry_id;
        @SerializedName("status")
        public String status;
        @SerializedName("completed_at")
        public String completed_at;
        @SerializedName("name")
        public String completed_by;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
    }

    public class Spares {
        @SerializedName("id")
        public String id;

        @SerializedName("enquiry_id")
        public String enquiry_id;

        @SerializedName("spares_id")
        public String spares_id;

        @SerializedName("spare_name")
        public String spare_name;


        @SerializedName("qty")
        public String qty;
    }

    public class SpareHistory {
        @SerializedName("id")
        public String id;
        @SerializedName("enquiry_id")
        public String enquiry_id;
        @SerializedName("chassis_no")
        public String chassis_no;
        @SerializedName("component")
        public String component;
        @SerializedName("old_serial_no")
        public String old_serial_no;
        @SerializedName("new_serial_no")
        public String new_serial_no;
        @SerializedName("warranty")
        public String warranty;
        @SerializedName("replaced_by")
        public String replaced_by;
        @SerializedName("changed_date")
        public String changed_date;

        @SerializedName("sent_for_replacement")
        public String sent_for_replacement;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
    }
}