package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm.CRMFilterSales
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.item_crm.view.*
import java.util.*

class ItemSales(val context: FragmentActivity) : RecyclerView.Adapter<ItemSales.HolderSales>() {

    private var isLoadingAdded = false
    private var retryPageLoad = false
    // View Types
    private val ITEM = 0
    private val LOADING = 1
    private val HERO = 2

    var saleResult = ArrayList<CRMFilterSales>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderSales {
        return HolderSales(LayoutInflater.from(context).inflate(R.layout.item_crm, parent, false))

    }

    override fun getItemCount(): Int {
        return saleResult.size
    }

    override fun onBindViewHolder(holder: HolderSales, position: Int) {
        holder.itemDate.setText("" + (position + 1))
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            HERO
        } else {
            if (position == saleResult.size - 1 && isLoadingAdded) LOADING else ITEM
        }
    }

    fun addAll(sales: ArrayList<CRMFilterSales>?) {
        for (sale in sales!!) {
            saleResult.add(sale)
        }
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = saleResult.size - 1
        val result = getItem(position)

        if (result != null) {
            saleResult.removeAt(position)
            notifyItemRemoved(position)
        }
        //notifyDataSetChanged()
    }

    fun getItem(position: Int): CRMFilterSales {
        return saleResult.get(position)
    }

    fun add(r: CRMFilterSales) {
        saleResult.add(r)
        notifyItemInserted(saleResult.size - 1)
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        add(CRMFilterSales("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", false))
    }


    class HolderSales(view: View) : RecyclerView.ViewHolder(view) {
        val itemName = view.item_crm_name
        val itemDate = view.item_crm_date
        val itemTask = view.item_crm_task
        val itemAction = view.item_crm_action
        val itemStatus = view.item_crm_status
        val itemLayout = view.item_crm
        val itemEmail = view.item_crm_action_email
        val itemCall = view.item_crm_action_call
        val snack_view = view.snack_view
        val crm_item_lay_model = view.crm_item_lay_model
        val itemModel = view.crm_item_model
    }
}