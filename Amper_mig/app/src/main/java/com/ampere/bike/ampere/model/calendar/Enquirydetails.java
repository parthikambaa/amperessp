package com.ampere.bike.ampere.model.calendar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

//@Generated("com.robohorse.robopojogenerator")
public class Enquirydetails{

	@SerializedName("enquirydetails")
	public List<EnquirydetailsItem> enquirydetails;

	@SerializedName("message")
	public String message;

	@SerializedName("status")
	public boolean status;
/*
	@Override
 	public String toString(){
		return 
			"Enquirydetails{" + 
			"enquirydetails = '" + enquirydetails + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}*/
}