package com.ampere.bike.ampere.model.indent;

import com.google.gson.annotations.SerializedName;


public class IndentModelSuccessResponse{

	@SerializedName("data")
	public int data;

	@SerializedName("success")
	public boolean success;

	@SerializedName("message")
	public String message;

	@Override
 	public String toString(){
		return 
			"IndentModelSuccessResponse{" + 
			"data = '" + data + '\'' + 
			",success = '" + success + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}