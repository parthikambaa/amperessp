package modifyviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;


public class InputMyTextView_Lato_Bold extends TextInputEditText {

    public InputMyTextView_Lato_Bold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public InputMyTextView_Lato_Bold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public InputMyTextView_Lato_Bold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Bold.ttf");
            setTypeface(tf);
        }
    }

}