package com.ampere.bike.ampere.views.fragment.dashboard;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.model.warranty.InvoiceWarranty;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.vehicles.R;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ampere.bike.ampere.shared.LocalSession.BEARER;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;

public class Dashboard extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(getActivity().getString(R.string.dash_board));
        return inflater.inflate(R.layout.frag_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        Call<InvoiceWarranty> invoiceWarrantyCall = MyUtils.getRetroService().onInvoiceWarranty(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), LocalSession.getUserInfo(getActivity(), KEY_ID));

        invoiceWarrantyCall.enqueue(new Callback<InvoiceWarranty>() {
            @Override
            public void onResponse(Call<InvoiceWarranty> call, Response<InvoiceWarranty> response) {
                InvoiceWarranty invoiceWarranty = response.body();
                //Log.d("voicesdsd", "" + invoiceWarranty.getData().getWarrantyinvoice().size());
                //Log.d("voiceuds", "" + invoiceWarranty.getData().getWarrantyinvoice().get(0).getVehicleModel());
            }

            @Override
            public void onFailure(Call<InvoiceWarranty> call, Throwable t) {

            }
        });
    }
}
