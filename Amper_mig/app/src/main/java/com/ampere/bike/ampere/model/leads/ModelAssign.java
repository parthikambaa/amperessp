package com.ampere.bike.ampere.model.leads;

import com.ampere.bike.ampere.model.enquiry.DataItem;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ModelAssign {

    @SerializedName("success")
    public boolean isStatus;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data data;

    public static class Data {
        @SerializedName("dealers")
        public ArrayList<Dealers> dealers;

        @SerializedName("corporate")
        public ArrayList<Corporate> corporate;

        @SerializedName("vehicle_model")
        public String vehicle_model;

        @SerializedName("chassis_no")
        public String chassis_no;

        @SerializedName("tasks")
        public ArrayList<Tasks> tasks;

        @SerializedName("spares")
        public ArrayList<DataItem> dataItems;


        @SerializedName("serviceissues")
        public ArrayList<ServiceIssue> serviceIssues;


        @SerializedName("servicetypes")
        public ArrayList<ServiceType> serviceTypes;

        public static class Dealers {

            @SerializedName("id")
            public String id;

            @SerializedName("name")
            public String name;

            @SerializedName("username")
            public String username;

            @SerializedName("email")
            public String email;

            @SerializedName("mobile")
            public String mobile;

            @SerializedName("profile_photo")
            public String profile_photo;

            @SerializedName("address")
            public String address;

            @SerializedName("user_type")
            public String user_type;
        }

        public static class Corporate {

            @SerializedName("id")
            public String id;

            @SerializedName("name")
            public String name;

            @SerializedName("username")
            public String username;

            @SerializedName("email")
            public String email;

            @SerializedName("mobile")
            public String mobile;

            @SerializedName("profile_photo")
            public String profile_photo;

            @SerializedName("address")
            public String address;

            @SerializedName("user_type")
            public String user_type;

        }

        public class Tasks {

            @SerializedName("id")
            public String id;

            @SerializedName("task_type")
            public String task_type;

            @SerializedName("task")
            public String task;

        }
    }

    public class ServiceIssue {

        @SerializedName("id")
        public String id;

        @SerializedName("service_issue")
        public String service_issue;

    }

    public class ServiceType {
        @SerializedName("id")
        public String id;

        @SerializedName("type")
        public String type;

        @SerializedName("status")
        public String status;

        @SerializedName("statusname")
        public String statusname;

    }

}
