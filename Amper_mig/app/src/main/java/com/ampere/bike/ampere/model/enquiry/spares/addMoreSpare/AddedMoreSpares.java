package com.ampere.bike.ampere.model.enquiry.spares.addMoreSpare;

public class AddedMoreSpares {
    private String spareName, spareQty;

    public AddedMoreSpares(String sparesName, String spareQty) {
        this.spareName = sparesName;
        this.spareQty = spareQty;
    }
}
