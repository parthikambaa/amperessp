package com.ampere.bike.ampere.views.kotlin.fragments.inventory

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.DD_MM
import com.ampere.bike.ampere.utils.DateUtils.YYYY_MM_DD
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.Sparedelivered
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle.Vehicledelivered
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.frag_inventory.view.*
import kotlinx.android.synthetic.main.item_stocks_spare.view.*
import java.io.Serializable
import java.util.*

public class PurchaseStocks : Fragment() {
    var item: ItemStocksInDeliverdSpare? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        //(activity as CustomNavigationDuo).disbleNavigationView("Pending")
        return inflater.inflate(R.layout.frag_inventory, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.invent_list_view.visibility = View.GONE
        view.invent_no_data.visibility = View.GONE

        setListView(view)

    }


    class ItemStockAcceptedVehicle(var activity: FragmentActivity, val stockDeails: ArrayList<Vehicledelivered>) : RecyclerView.Adapter<ItemStockAcceptedVehicle.StocksInVehicleAccepHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StocksInVehicleAccepHolder {

            return StocksInVehicleAccepHolder(View.inflate(activity, R.layout.item_stocks_spare, null))
        }

        override fun getItemCount(): Int {
            return stockDeails.size
        }

        override fun onBindViewHolder(holder: StocksInVehicleAccepHolder, position: Int) {
            holder.stock_name.text = stockDeails[position].vehicle_model
            holder.stock_check.visibility = View.GONE
            holder.stock_invoice_date.text = DateUtils.convertDates(stockDeails[position].indent_invoice_date, YYYY_MM_DD, DD_MM)
            holder.stock_qty.text = stockDeails[position].qty
            holder.stock_serial_number.text = stockDeails[position].spares_serial_no
            holder.inventory_lay.setOnClickListener {
                var fragment = ViewViehicleDetailed()
                var bundle = Bundle()
                bundle.putString("id", "" + stockDeails[position].id)
                bundle.putString("id_values", "Vehicle_delivery")
                fragment.arguments = bundle
                MyUtils.passFragmentBackStack(activity, fragment)
            }
        }

        class StocksInVehicleAccepHolder(view: View) : RecyclerView.ViewHolder(view) {
            val stock_name = view.stock_name
            val stock_check = view.stock_check
            val stock_serial_number = view.stock_serial_number
            val stock_invoice_date = view.stock_invoice_date
            val stock_qty = view.stock_qty
            val inventory_lay = view.inventory_lay
        }
    }


    class ItemStocksInDeliverdSpare(var activity: FragmentActivity, val stockDeails: ArrayList<Sparedelivered>) : RecyclerView.Adapter<ItemStocksInDeliverdSpare.StocksInSpareDeliverHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StocksInSpareDeliverHolder {
            return StocksInSpareDeliverHolder(View.inflate(activity, R.layout.item_stocks_spare, null))
        }

        override fun getItemCount(): Int {
            return stockDeails.size
        }

        override fun onBindViewHolder(holder: StocksInSpareDeliverHolder, position: Int) {
            holder.stock_name.text = stockDeails[position].spare_name
            holder.stock_check.visibility = View.GONE
            holder.stock_invoice_date.text = DateUtils.convertDates(stockDeails[position].indent_invoice_date, YYYY_MM_DD, DD_MM)
            holder.stock_qty.text = stockDeails[position].qty
            holder.stock_serial_number.text = stockDeails[position].serial_no



            holder.inventory_lay.setOnClickListener {
                var fragment = ViewSpareDetailed()
                var bundle = Bundle()
                bundle.putString("id", "" + stockDeails[position].id)
                bundle.putString("id_values", "Spare_delivery")
                bundle.putSerializable("item_values", stockDeails[position] as Serializable)
                fragment.arguments = bundle
                MyUtils.passFragmentBackStack(activity, fragment)

            }
        }

        class StocksInSpareDeliverHolder(view: View) : RecyclerView.ViewHolder(view) {
            val stock_name = view.stock_name
            val stock_check = view.stock_check
            val stock_serial_number = view.stock_serial_number
            val stock_invoice_date = view.stock_invoice_date
            val stock_qty = view.stock_qty
            val inventory_lay = view.inventory_lay
        }
    }


    private fun setListView(view: View) {

//        Log.d("DELIVER_VEHICLEsdsds", "" + DELIVER_VEHICLE.size + "  VIEW_ITEM " + MyUtils.VIEW_ITEM + " DELIVER_SPARE  " + DELIVER_SPARE.size)
        //Accepted Stocks
        view.invent_list_view.layoutManager = GridLayoutManager(activity, 1)
        if (MyUtils.VIEW_ITEM.equals(getString(R.string.vehicle))) {
            if (MyUtils.DELIVER_VEHICLE.size == 0) {
                view.invent_no_data.setText("No Pending Vehicle Stock Stocks Found")
                view.invent_no_data.visibility = View.VISIBLE
                view.invent_list_view.visibility = View.GONE
            } else {
                view.invent_no_data.visibility = View.GONE
                view.invent_list_view.visibility = View.VISIBLE
                var item = ItemStockAcceptedVehicle(activity!!, MyUtils.DELIVER_VEHICLE)
                view.invent_list_view.adapter = item
            }
        } else {
            if (MyUtils.DELIVER_SPARE.size == 0) {
                view.invent_no_data.setText("No Pending Spare Stocks Found")
                view.invent_no_data.visibility = View.VISIBLE
                view.invent_list_view.visibility = View.GONE
            } else {
                view.invent_no_data.visibility = View.GONE
                view.invent_list_view.visibility = View.VISIBLE
                item = ItemStocksInDeliverdSpare(activity!!, MyUtils.DELIVER_SPARE)
                view.invent_list_view.adapter = item
            }
        }

    }

}