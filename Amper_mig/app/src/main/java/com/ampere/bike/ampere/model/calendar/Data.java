package com.ampere.bike.ampere.model.calendar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

//@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("message")
	public String message;

	@SerializedName("enquirycount")
	public List<EnquirycountItem> enquirycount;

	@SerializedName("status")
	public boolean status;

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"message = '" + message + '\'' + 
			",enquirycount = '" + enquirycount + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}