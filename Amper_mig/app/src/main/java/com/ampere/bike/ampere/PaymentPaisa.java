package com.ampere.bike.ampere;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.ampere.vehicles.R;

import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Formatter;

/*
import fonepaisa.com.fonepaisapg_sdk.FPConstants;
import fonepaisa.com.fonepaisapg_sdk.fonePaisaPG;*/

public class PaymentPaisa extends AppCompatActivity {
    Button PG_VIEW_BTN;
    int FONEPAISAPG_RET_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_paisa);
        PG_VIEW_BTN = (Button) findViewById(R.id.test_pg);
        PG_VIEW_BTN.setOnClickListener(new Test_PG());
    }


    public class Test_PG implements Button.OnClickListener {
        @Override
        public void onClick(View v) {
            //TODO
            String API_KEY = "08Z1782051U62BY9OUGW4XM67GF2004";
           /* Intent intent = new Intent(PaymentPaisa.this, fonePaisaPG.class);
            JSONObject json_to_be_sent = new JSONObject();
            try {
                json_to_be_sent.put("id", "FPTEST");    // Mandatory .. FPTEST is just for testing it has to be changed before going to production
                json_to_be_sent.put("merchant_id", "FPTEST");   // Mandatory .. FPTEST is just for testing it has to be changed before going to production
                json_to_be_sent.put("merchant_display", "fonepaisa");  // Mandatory ..  change it to whatever you want to get it displayed
                json_to_be_sent.put("invoice", System.currentTimeMillis() + "FP"); //mandatory  .. this is the unique reference against which you can enquire and it can be system generated or manually entered
                json_to_be_sent.put("mobile_no", "7204853405");    ///pass the mobile number if you have else send it blank and the customer will be prompted for the mobile no so that confirmation msg can be sent
                json_to_be_sent.put("email", "");          // pass email if an invoice details has to be mailed
                json_to_be_sent.put("invoice_amt", "1.00");    //pass the amount with two decimal rounded off
                json_to_be_sent.put("note", "");         // pass any notes if you need
                json_to_be_sent.put("payment_types", "");    // not mandatory . this is to restrict the payment types
                json_to_be_sent.put("addnl_info", "");          // pass any addnl data which u need to get baack
                //input for signing  API_KET#id#merchant_id#invoice#amount
                String signed_ip = API_KEY + "#" + json_to_be_sent.getString("id") + "#" + json_to_be_sent.getString("merchant_id") + "#" + json_to_be_sent.getString("invoice") + "#" + json_to_be_sent.getString("invoice_amt") + "#";

                *//* *********************************************************************************************
                 *               TODO                                                                           *
                 *     Just for testing we have signed in the client side .                                     *
                 *    Please do the signing on your server side . and pass the signed message in the json       *
                 *                                                                                              *
                 ************************************************************************************************//*

                String signed_msg = getSignedMsg(signed_ip);
                json_to_be_sent.put("sign", signed_msg);
                json_to_be_sent.put("Environment", FPConstants.Test_Environment);  //mandatory   //Change it based on the environment you are using
                intent.putExtra("data", json_to_be_sent.toString());
                startActivityForResult(intent, FONEPAISAPG_RET_CODE);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }
    }

    /*************************************************************************************
     *                                   TODO                                             *
     * This is the code you need to modify to handle your success and failure scenarios . *
     **************************************************************************************/
    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent returned_intent) {
        if (requestCode == FONEPAISAPG_RET_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                System.out.println("returned message" + returned_intent.getStringExtra("resp_msg"));
                System.out.println("returned code" + returned_intent.getStringExtra("resp_code"));
                System.out.println("sent json" + returned_intent.getStringExtra("data_sent"));
                System.out.println("returned json" + returned_intent.getStringExtra("data_recieved"));
                Toast toast = Toast.makeText(getApplicationContext(), returned_intent.getStringExtra("resp_msg").toString(), Toast.LENGTH_SHORT);
                toast.show();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast toast = Toast.makeText(getApplicationContext(), "customer cancelled the payment", Toast.LENGTH_SHORT);
               /* System.out.println("returned message on cancelled "+returned_intent.getStringExtra("resp_msg"));
                System.out.println("returned code on cancelled "+returned_intent.getStringExtra("resp_code"));
                Toast toast = Toast.makeText(getApplicationContext(),returned_intent.getStringExtra("resp_msg").toString(), Toast.LENGTH_SHORT);
                toast.show();*/
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), "customer cancelled the payment", Toast.LENGTH_SHORT);
            }
        }
    }

    //TODO
    private String getSignedMsg(String input) {
        try {
            /**************************************************************************************************************
             *                                    TODO                                                                     *
             *IMPORTANT : Signing of the message should be done in the backend to ensure that the keys are not compromised *
             *               and  can be modified on a regular  basis.                                                     *
             ***************************************************************************************************************/


            String privKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDNzBfgbTSNp62zSYmScf9pwyr77QCEjGRvyuvHaRQ9AsPqYvxIwstar4WESLIKN9tUvvzE4xJGRuy7J0sGnehRom93fSWtdck6V+MCATfxUOfrRJCNqPEEcNi1809dcz1qsmUfZegG34p/ARURJ3bHAaVvS3YdX8tAwcnHFGX2QLl9/eX02qAZWbxpBFojsAlzpkF1E6NkaIaIiuymvPN1EmQQmeibeSUJlx6juUgtF2K9eS6hbke0khA2Upy+0nA6PLGDnpVuQnFpXG0UxiojLMdlh/Rv0oCadd2exYdGB2CT3q/NGHtDYSKnLU1xBImvS92nPMHVmO7gauoxKSQfAgMBAAECggEBAKM1BoJ/WLw2jHSxDx9KtOolU4NzU4PK6yQVY6NDXD9+X+0UD0uM4ETNCi/8juW3ooO06zUhd66wNLG/2aontMR487lpUGYeETXp2SgP21PPe/2C5LjTkECbVeIGUZyk9cIWNEgQQ1CgG2/ZZeGy0GnGjnKS/9sPy1tR1DnDnZEKG2jANGeE6H7ouYChFVwP85UEOgiHv2fmidRdfk5KrNw4prVelpznTOthKx/Zi+7q9oGkEie/PmE4hdCO1ucXUtOVxU3QMOBL0QWIhyBlWhTeY6nt7ayX4MnIweRsu4CQlP0LsrihmSfsEcgpIRteEQzhG9TuC1CVHvzI6XH2iQECgYEA6teV0RSCSUO9AB64A9p5wgB5V9+SkaBF3lCPfmJbOboQs5Jl0U5F3Ph7IfW1OzZDtqPThRfy+yV6+XWu3fczG3GP2OFCOrPlesmGXPTvWAWbfowEkiQCkddrRFmypI6QR2ZBItrHXhzPfR9Br8RybBzrLuUv7yho0BGCJ/ZnZ18CgYEA4FadXsVJnJxK65akI8ihGJpu1KgVmrKo6zYfplw3bdzoFPbLRAmuc09J9aQcuAQaCBEfguHeVNJb2CTAv90dx7E3gXHCDbW4SUcyybbTBBG5ZUIz4Ltv4LnLITqFKxiTpiOc+vi8+XFfOvDtqyXfBVVegIbV+xUiaOYPS4ULO0ECgYA0e1VZ0lGDegXk3viUs+B+AIkdoDMrJDw5AJvwzJ5Celh9KPxkGC/4v/cUkcqcnvXm/RmqJr4AblHbKfeYV0Qun+RbvYuFfuqL1DmY0IwkiaxETZo/5phEa3XnYnxP1iRcMHfiCC6B08Jy3edaFnbTvmq4ojNiKQ+zYBZMQ/671QKBgEX8/6+3YRXI9N626pJ3XzrrwzP5FHRk1Ko9AnbGQky2JHmV3Shm1NQIooxOHN+T+AMYRHpyuQhBcIHoRXIWK9pHAYgS03WvgcTqv3+K2B5m4S4kD0dHcsnrbOH6/dzKGBY2+hyaSWqQ4iLjU2KXuBJT5d23Mz7YAxoy3Aa1hSGBAoGAHJgJe7f6PT5BgeDZbC7yw1RcLyH7uFwMaTTZCHcnrsvz1FHbWLI4N5lCd08RBzyygEtIofQXGsxpE6wwyDGlnPHBZXnQsUWIm+eYQ4bl0O5mAunPVU9VWl6P8ynCoBqehbjAyy34b59XJ92gxcdK73ybVbHyemocPU4Fj8sp48o=";
            // Get private key from String
            PrivateKey pk = loadPrivateKey(privKey);

            Signature sign = Signature.getInstance("SHA512withRSA");
            sign.initSign(pk);
            System.out.println("TEST3");
            sign.update(input.getBytes());
            byte[] hashBytes = sign.sign();
            return bytesToHexString(hashBytes);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);

        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        formatter.close();
        return sb.toString();
    }

    private PrivateKey loadPrivateKey(String key64) throws GeneralSecurityException {
        byte[] privkeybytes = Base64.decode(key64, Base64.NO_WRAP);
        KeyFactory fact;
        try {
            fact = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(privkeybytes);
            PrivateKey privateKey1 = fact.generatePrivate(privateSpec);
            return privateKey1;
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
