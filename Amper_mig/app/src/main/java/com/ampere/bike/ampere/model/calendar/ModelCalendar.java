package com.ampere.bike.ampere.model.calendar;

import com.google.gson.annotations.SerializedName;


public class ModelCalendar {

    @SerializedName("enquirydetails")
    public Enquirydetails enquirydetails;

    @SerializedName("data")
    public Data data;

    @SerializedName("success")
    public boolean success;

    @SerializedName("message")
    public String message;

    @Override
    public String toString() {
        return
                "ModelCalendar{" +
                        "enquirydetails = '" + enquirydetails + '\'' +
                        ",data = '" + data + '\'' +
                        ",success = '" + success + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }
}