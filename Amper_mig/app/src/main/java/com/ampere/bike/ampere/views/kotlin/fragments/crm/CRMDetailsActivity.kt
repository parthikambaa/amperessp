package com.ampere.bike.ampere.views.kotlin.fragments.crm

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.TextInputLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.widget.NestedScrollView
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import com.ampere.bike.ampere.api.RetrofitCall
import com.ampere.bike.ampere.interfaces.ListSetter
import com.ampere.bike.ampere.interfaces.RefreshViews
import com.ampere.bike.ampere.interfaces.RemarksAndIDUpdate
import com.ampere.bike.ampere.model.enquiry.edit.ModelServiceComponets
import com.ampere.bike.ampere.model.enquiry.enquiryRemarksUpdate.CallAPIEnquiryTaskIDAndRemarks
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.*
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MessageUtils.showErrorCodeToast
import com.ampere.bike.ampere.utils.MessageUtils.showFailureToast
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.utils.MyUtils.checkVersion
import com.ampere.bike.ampere.utils.MyUtils.convertStreamToString
import com.ampere.bike.ampere.utils.NetworkTester
import com.ampere.bike.ampere.views.fragment.Login.pattern
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry.LOCATION_PERMISSION_CODE
import com.ampere.bike.ampere.views.kotlin.fragments.ViewEventDetailed
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.AddNextTask.Companion.refresh
import com.ampere.bike.ampere.views.kotlin.fragments.invoice.InvoiceActivity
import com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm.StatusModel
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.Data
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.EnquiryDetails
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.EnquiryStatusList
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.TaskDetailsModel
import com.ampere.bike.ampere.views.kotlin.item.ItemForTaskUpdate
import com.ampere.bike.ampere.views.kotlin.model.invoice.AddInvoice
import com.ampere.vehicles.R
import com.google.gson.Gson
import customviews.CustomButton
import customviews.CustomTextEditView
import customviews.CustomTextView
import kotlinx.android.synthetic.main.dialog_invoice.*
import kotlinx.android.synthetic.main.frag_view_even_details.*
import kotlinx.android.synthetic.main.item_dialog.view.*
import kotlinx.android.synthetic.main.item_enquiry_status_list.view.*
import kotlinx.android.synthetic.main.item_spare_list.view.*
import kotlinx.android.synthetic.main.view_task_expandable.*
import modifyviews.MyTextView_Lato_Bold
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.entity.mime.MultipartEntity
import org.apache.http.entity.mime.content.FileBody
import org.apache.http.entity.mime.content.StringBody
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.protocol.BasicHttpContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class CRMDetailsActivity : AppCompatActivity(), ListSetter<TaskDetailsModel>, RemarksAndIDUpdate {

    companion object {
        var childId: String = ""
        var enquiryId: String = ""
        var user_name: String = ""
        var task_name: String = ""
        var addNextTask: AddNextTask? = null
        var dialog: Dialog? = null
        var mEnqquiry_for: CustomTextView? = null
        var mVehicleModel: CustomTextView? = null

        var mEmailId: CustomTextView? = null
        var mMobileNo: CustomTextView? = null

        var mValues: CustomTextView? = null
        var mName: CustomTextView? = null

        var mStatus: CustomTextView? = null
        var mSnacView: CoordinatorLayout? = null
        var models: TaskDetailsModel? = null
    }

    var enquiryDetailsList: MutableList<EnquiryDetails> = ArrayList<EnquiryDetails>()
    var mEnquiryStatusView: LinearLayout? = null
    var mEnquiryStatusList: RecyclerView? = null
    var mNoDataEnquiryStatusList: CustomTextView? = null

    var mSpareListView: LinearLayout? = null
    var mSpareList: RecyclerView? = null
    var mNoDataSpareList: CustomTextView? = null

    var isHidden: Boolean = false
    var year: Int = 0
    var month: Int = 0
    var day: Int = 0
    var statusValues = ArrayList<String>()
    var mTaskDetails: CustomTextView? = null
    var TAKE_PHOTO_CODE = 0
    var outputFileUri: Uri? = null
    val retrofit = MyUtils.getInstance()
    var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    var bottomDheetDialog: BottomSheetDialog? = null
    internal var bottomSheetBehavior: BottomSheetBehavior<*>? = null

    var mRLServiceComponent: RelativeLayout? = null
    var mLLServiceType: LinearLayout? = null
    var mSpSeviceComponent: Spinner? = null
    var mEdtServiceComponent: CustomTextEditView? = null
    var mSpSeviceType: Spinner? = null
    var mEdtServiceType: CustomTextEditView? = null

    var serviceComponents: List<ModelServiceComponets>? = ArrayList()
    var strServiceComponentId: String? = null
    var mIPLRemarks: TextInputLayout? = null
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        var newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        setContentView(R.layout.frag_view_even_details)

        mEnqquiry_for = findViewById(R.id.ev_details_enqquiry_for)
        mVehicleModel = findViewById(R.id.ev_details_vehicle_model)
        mEmailId = findViewById(R.id.ev_details_mail)
        mMobileNo = findViewById(R.id.ev_details_mobile_no)
        mValues = findViewById(R.id.ev_invoice)
        mName = findViewById(R.id.ev_details_name)
        mStatus = findViewById(R.id.ev_task_details)
        mEnquiryStatusView = findViewById(R.id.enquiry_status_layout)
        mEnquiryStatusList = findViewById(R.id.enquiry_status_list)
        mEnquiryStatusList!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        mNoDataEnquiryStatusList = findViewById(R.id.no_enquiry_status)
        mSpareListView = findViewById(R.id.spare_list_layout)
        mSpareList = findViewById(R.id.spare_list)
        mSpareList!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        mNoDataSpareList = findViewById(R.id.no_spare_status)

        mRLServiceComponent = findViewById(R.id.rl_service_component)
        mLLServiceType = findViewById(R.id.ll_service_type)
        mSpSeviceComponent = findViewById(R.id.sp_service_component)
        mEdtServiceComponent = findViewById(R.id.apnt_edt_service_component)
        mSpSeviceType = findViewById(R.id.sp_service_type)
        mEdtServiceType = findViewById(R.id.apnt_edt_service_type)
        mIPLRemarks = findViewById(R.id.bs_impl_remark)
        mIPLRemarks?.visibility = View.GONE
        var serviceType = arrayOf("Warranty", "Paid")

        mSpSeviceType!!.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, R.layout.item_dialog, R.id.text1, serviceType) as SpinnerAdapter?


        view_hidden_items.visibility = View.GONE
        if (checkVersion() == 1) {
            add_new_task.setBackgroundResource(R.color.orange)
        }

        ic_back_crm.setOnClickListener {
            onBackPressed()
        }

        mTaskDetails = ev_task_details
        mSnacView = snack_view
        add_new_task.visibility = View.VISIBLE

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        var bundle = intent
        enquiryId = bundle.getStringExtra("id")
        user_name = bundle.getStringExtra("user_name")
        task_name = bundle.getStringExtra("task_name")

        Log.d("user_nameuser_name", "" + user_name + "  enquiryId " + enquiryId + " task_name " + task_name)

        tool_title.text = task_name + " Details"

        if (task_name.equals(getString(R.string.service))) {
            mLLServiceType!!.visibility = View.VISIBLE
        } else {
            mLLServiceType!!.visibility = View.GONE
        }

        if (task_name.equals(getString(R.string.sales))) {
            ev_details_spares.visibility = View.INVISIBLE
        } else if (task_name.equals(getString(R.string.service))) {
            ev_details_spares.visibility = View.INVISIBLE
        } else if (task_name.equals(getString(R.string.spares))) {
            ev_details_spares.visibility = View.VISIBLE
        }

        val llBottomSheet: LinearLayout = bottom_sheet!!
        llBottomSheet.visibility = View.VISIBLE
        //var viewss: View = llBottomSheet.parent as View
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)
        bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        bottomSheetBehavior?.setPeekHeight(340)
        bottomSheetBehavior?.setHideable(true)

        ev_lis_view.layoutManager = LinearLayoutManager(this)
        var miniTask = HashMap<String, String>()
        miniTask.put("enquiry_id", "" + enquiryId)
        miniTask.put("user_name", "" + user_name)
        var taskModel: Call<TaskDetailsModel> = retrofit.onLoadTaskDetails(BEARER + LocalSession.getUserInfo(this, KEY_TOKEN), miniTask)


        //set Task details from enquiry_id
        onTaskLoading(taskModel)

        mSwipeRefreshLayout = findViewById(R.id.swiperefresh)
        mSwipeRefreshLayout?.setRefreshing(false)


        val animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.lay_slide_up)
        val animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.lay_slide_down)


        mName?.setOnClickListener {
            if (isHidden) {
                isHidden = false
                view_hidden_items.startAnimation(animFadeIn)
                view_hidden_items.visibility = View.GONE

                mName?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_user, 0, R.drawable.ic_down, 0)

            } else {
                view_hidden_items.startAnimation(animFadeOut)
                view_hidden_items.visibility = View.VISIBLE
                mName?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_user, 0, R.drawable.ic_up, 0)

                isHidden = true
            }
        }

        ev_lis_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && add_new_task.visibility === View.VISIBLE) {
                    add_new_task.hide()
                } else if (dy < 0 && add_new_task.visibility !== View.VISIBLE) {
                    add_new_task.show()
                }
            }
        })
        mSpSeviceType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapterVie: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val strServiceType = mSpSeviceType?.selectedItem as String
                Log.d("strServiceType", "" + strServiceType)

                if (strServiceType != null) {
                    mEdtServiceType?.setText("" + strServiceType)
                    // onGetServiceComponents(strServiceType)
                    if (strServiceType.equals("Warranty")) {
                        mRLServiceComponent!!.visibility = View.VISIBLE
                    } else {
                        mRLServiceComponent!!.visibility = View.GONE
                    }
                } else {
                    mEdtServiceType!!.setText(" ")
                }
            }

        }
        mSpSeviceComponent?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapterVie: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

                p0?.getChildAt(p2)
                val serviceComponents: LinearLayout = p0!!.selectedView as LinearLayout

                val strServiceComponentName = serviceComponents.text1.text.toString()
                Log.d("strServiceType", " " + strServiceComponentName)
                if (strServiceComponentName != null) {
                    mEdtServiceComponent?.setText("" + strServiceComponentName)
                    strServiceComponentId = serviceComponents.id_s.text.toString()
                    //onGetServiceComponents(strServiceType)
                } else {
                    mEdtServiceComponent!!.setText(" ")

                }
            }

        }

        refresh(object : RefreshViews {
            override fun refresh(models: TaskDetailsModel) {
                //Log.d("taskDetailsModel_models", "" + models)
                mName?.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.customerName)
                ev_details_city.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.city)
                ev_details_vehicle_model.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.vehicleModel)
                ev_details_chassis_no.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.chassisNo)
                mEmailId?.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.email)
                mValues?.text = "  " + getString(R.string.inr) + " " + MyUtils.nullPointerValidation(models.data.enquiry.invoice_value)
                mMobileNo?.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.mobileNo)
                mEnqquiry_for?.text = MyUtils.nullPointerValidation(models.data.enquiry.enquiryFor)
                ev_details_spares.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.sparesName)
                ev_lead_source.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.leadsource)
                ev_details_task.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.task)
                ev_details_message.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.message)
                ev_task_details.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.taskStatus)

                CustomTextView.marquee(ev_details_task)
                CustomTextView.marquee(mEmailId)

                try {
                    if (models.data.enquiry.enquiryDate != null) {
                        ev_details_enquiry_date.text = "  " + DateUtils.convertDates(models.data.enquiry.enquiryDate, DateUtils.CURRENT_FORMAT, DateUtils.DD_MM) + "  " + models.data.enquiry.enquiryTime
                    }
                    if (models.data.enquiry.assignedDate != null) {
                        ev_details_enquiry_assigned_date.text = "  " + DateUtils.convertDates(models.data.enquiry.assignedDate, DateUtils.CURRENT_FORMAT + " HH:mm:ss", DateUtils.DD_MM + " hh:mm a")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

        mSwipeRefreshLayout?.setOnRefreshListener {
            onTaskLoading(taskModel)
        }

        bs_task?.setOnClickListener {
            bs_task_spinner?.performClick()
        }

        bs_task_spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                var taskInput = bs_task_spinner?.selectedItem as String
                if (!taskInput.equals("")) {
                    bs_task?.setText(taskInput)
                } else {
                    bs_task?.setText("")
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        bottomSheetBehavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        //  Log.d("bottom_sheet", "hidd")
                        add_new_task.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        //  Log.d("bottom_sheet", "exx")
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        // Log.d("bottom_sheet", "coll")
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        // Log.d("bottom_sheet", "dragg")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                        //  Log.d("bottom_sheet", "sett")
                        add_new_task.visibility = View.GONE
                    }
                }

            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })
        close.setOnClickListener {
            //if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
            //}
        }


        bs_add_event_new?.setOnClickListener {

            var remarksStr = bs_remarks?.text.toString()
            var statusStr = bs_status_spinner.selectedItem.toString()
            var dateStr = bs_date?.text.toString()
            var timeStr = bs_time?.text.toString()
            var taskStr = bs_task?.text.toString()
            var chassisNumberStr = bs_edt_chassis_number.text.toString()
            var oldSerialNo = bs_edt_old_serial_number.text.toString()
            var newSerialNo = bs_edt_new_serial_number.text.toString()


            //if (!MyUtils.isEmpty(this, remarksStr, "Enter Your Remarks")) {
            val enquiuryUpeadeRemarks: ArrayList<CallAPIEnquiryTaskIDAndRemarks> = ArrayList<CallAPIEnquiryTaskIDAndRemarks>()
            //var taskCount: Int = ev_lis_view.adapter.itemCount
            var taskCount: Int = enquiryDetailsList.size
            Log.d("taskCountsdsd", "" + taskCount)
            for (i in 0..taskCount - 1) {
                System.out.print("indendksdjs " + i)
                var remarks: String = enquiryDetailsList.get(i).remarks.toString()
                var task_id: String = enquiryDetailsList.get(i).id.toString()
                Log.d("remarkssdsd", "" + i + "  " + remarks + "  " + task_id)
                var callAPIEnquiryTaskIDAndRemarks = CallAPIEnquiryTaskIDAndRemarks(task_id, remarks)
                enquiuryUpeadeRemarks.add(callAPIEnquiryTaskIDAndRemarks)

            }

            Log.d("enquiuryUpeadeRemark", "" + enquiuryUpeadeRemarks.size)
            var enquirytaskid = HashMap<String, Any>()
            enquirytaskid.put("enquirytaskid", enquiuryUpeadeRemarks)
            var map = HashMap<String, Any>()
            map.put("enquirytaskids", enquirytaskid)
            map.put("enquiry_id", enquiryId)
            map.put("enquiry_status", statusStr)
            map.put("username", LocalSession.getUserInfo(this@CRMDetailsActivity, LocalSession.KEY_ID))
            //map.put("enquirytaskid", childId)

            var isLoadAPI: Boolean = false
            if (statusStr.equals(getString(R.string.closure))) {
                if (!taskStr.equals("No Task List Found")) {
                    Log.d("outputFileUri", "" + outputFileUri)
                    if (!MyUtils.isEmpty(this@CRMDetailsActivity, taskStr, "Select Next Task")) {
                        if (!MyUtils.isEmpty(this@CRMDetailsActivity, remarksStr, "Remarks can't be empty")) {
                            map.put("nexttask", taskStr)
                            map.put("lostremark", remarksStr)
                            isLoadAPI = true
                        }
                    }
                    /*addNextTask = AddNextTask(this@CRMDetailsActivity, dateStr, timeStr, taskStr, remarksStr, statusStr, "" + outputFileUri, chassisNumberStr, oldSerialNo, newSerialNo)
                    addNextTask?.execute()*/
                } else {
                    MessageUtils.showToastMessage(this@CRMDetailsActivity, "Task Is Empty. So Contact your Admin.")
                }
            } else {
                if (!MyUtils.isEmpty(this@CRMDetailsActivity, dateStr, "Select Next Date")) {
                    if (!MyUtils.isEmpty(this@CRMDetailsActivity, timeStr, "Select Next Time")) {
                        if (!MyUtils.isEmpty(this@CRMDetailsActivity, taskStr, "Select Next Task")) {
                            if (!taskStr.equals("No Task List Found")) {
                                Log.d("outputFileUri", "" + outputFileUri)

                                map.put("nexttask", taskStr)
                                map.put("next_activity_date", dateStr + " " + timeStr)
                                Log.d("mapsdsd", "" + map)
                                isLoadAPI = true
                                /*var date = next_activity_date
                                onAddNextTask(date, date, enquiry_status, remarks, userName, enquiry_id, enquirytaskid, nexttask, nexttask)*/
                                /* addNextTask = AddNextTask(this@CRMDetailsActivity, dateStr, timeStr, taskStr, remarksStr, statusStr, "" + outputFileUri, chassisNumberStr, oldSerialNo, newSerialNo)
                                    addNextTask?.execute()
*/
                            } else {
                                MessageUtils.showToastMessage(this@CRMDetailsActivity, "Task Is Empty. So Contact your Admin.")
                            }
                        }
                    }
                }
            }

            Log.d("mapsdsd_after", "" + map)
            if (isLoadAPI) {
                var taskDetailsModel: Call<TaskDetailsModel> = MyUtils.getInstance().onUpdateEnquiry(BEARER + LocalSession.getUserInfo(this, KEY_TOKEN), map)

                var dialog = MessageUtils.showDialog(this@CRMDetailsActivity)

                taskDetailsModel.enqueue(object : Callback<TaskDetailsModel> {
                    override fun onFailure(call: Call<TaskDetailsModel>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.setFailureKotlin(this@CRMDetailsActivity, snack_view, MessageUtils.showFailureToast(t.message))
                    }

                    override fun onResponse(call: Call<TaskDetailsModel>, response: Response<TaskDetailsModel>) {
                        MessageUtils.dismissDialog(dialog)
                        if (response.isSuccessful) {
                            models = response.body()
                            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
                            if (task_name.equals(getString(R.string.sales))) {
                                if (statusStr.equals(getString(R.string.closure))) {
                                    if (taskStr.toLowerCase().equals("won".toLowerCase())) {

                                        //MyUtils.passFragmentBackStack(this@CRMDetailsActivity, InvoiceFragment())
                                        var intent = Intent(this@CRMDetailsActivity, InvoiceActivity::class.java)
                                        intent.putExtra("id", "" + enquiryId)
                                        intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_USER_NAME))
                                        intent.putExtra("task_name", "" + statusStr)
                                        startActivity(intent)
                                        overridePendingTransition(0, 0)
                                        finish()

                                    }
                                }
                            } else {
                                viewRefresh(models)
                            }

                        } else {
                            MessageUtils.setFailureKotlin(this@CRMDetailsActivity, snack_view, MessageUtils.showErrorCodeToast(response?.code()))
                        }
                    }
                })
            } else {
                MessageUtils.setFailureKotlin(this@CRMDetailsActivity, snack_view, "Something went wrong")
            }
        }


        bs_status_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                try {
                    var taskInput: String = ""
                    val localStr: String = bs_status_spinner?.selectedItem as String

                    //val taskInput: String = "58"
                    // Log.d("taskInput", "" + localStr)
                    if (models != null) {
                        for (enquiry_list in models!!.data.enquirystatuslist) {
                            var status = enquiry_list.status
                            //Log.d("statussdsd", "" + status)
                            if (localStr.equals(status)) {
                                taskInput = enquiry_list.statusHint
                            }
                        }

                    }

                    //Log.d("taskInput_after", "" + localStr + " taskInput " + taskInput)

                    if (taskInput != null && !taskInput.equals("")) {
                        bs_status?.setText(taskInput)
                        if (localStr.equals(getString(R.string.closure))) {
                            mIPLRemarks?.visibility = View.VISIBLE
                            var enquiry = mEnqquiry_for?.text.toString()
                            //Log.d("enquirysds", "" + enquiry)
                            if (enquiry.equals("Service")) {
                                bs_open_camera_lay.visibility = View.VISIBLE
                            }
                            layout_action_date.visibility = View.GONE
                        } else {
                            mIPLRemarks?.visibility = View.GONE
                            bs_open_camera_lay.visibility = View.GONE
                            layout_action_date.visibility = View.VISIBLE
                        }
                    } else {
                        bs_open_camera_lay.visibility = View.GONE
                        bs_status?.setText("")
                        layout_action_date.visibility = View.VISIBLE
                    }
                    // Log.d("when_comes", "sdsdsd no " + taskInput + " hid " + bottomSheetBehavior?.isHideable + "  state " + bottomSheetBehavior?.state)
                    //if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                    val statusValues = ArrayList<String>()
                    //dialog = MessageUtils.showDialog(this)
                    val map = HashMap<String, String>()
                    map.put("enquiry_id", enquiryId)
                    //map.put("status", CRMDetailsActivity.mTaskDetails?.text.toString().trim())
                    //map.put("status", "New Enquiry")
                    map.put("status", taskInput)
                    var retrofit = MyUtils.getInstance()
                    var dialog = MessageUtils.showDialog(this@CRMDetailsActivity)

                    val statusModel: Call<StatusModel> = retrofit.onGetStatus(BEARER + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN), map)
                    MessageUtils.dismissDialog(dialog)
                    dialog?.setOnKeyListener { dialogInterface, _, keyEvent ->
                        dialogInterface.dismiss()
                        statusModel.cancel()
                        false
                    }

                    statusModel.enqueue(object : Callback<StatusModel> {
                        override fun onResponse(call: Call<StatusModel>?, response: Response<StatusModel>?) {
                            try {
                                MessageUtils.dismissDialog(dialog)
                                if (response?.isSuccessful!!) {
                                    var models: StatusModel = response.body()!!
                                    if (models != null) {
                                        if (models.success) {
                                            //  var newTask = ArrayList<NewTaskStatusUpdate>()
                                            for (statuass in models.data) {
                                                statusValues.add(statuass.task)

                                                /*  var addTaskId = NewTaskStatusUpdate(statuass.task, statuass.id, statuass.status, statuass.taskType)
                                                  newTask.add(addTaskId)*/
                                            }
                                            bs_task_spinner.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, android.R.layout.simple_list_item_1, android.R.id.text1, statusValues)

                                        } else {

                                            statusValues.add("" + models.message)
                                            bs_task_spinner.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, android.R.layout.simple_list_item_1, android.R.id.text1, statusValues)
                                            if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                                                MessageUtils.showToastMessage(this@CRMDetailsActivity, models.message)
                                            }
                                        }
                                    } else {
                                        if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                                            MessageUtils.showToastMessage(this@CRMDetailsActivity, showFailureToast(getString(R.string.api_response_not_found)))
                                        }
                                    }
                                } else {
                                    if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                                        MessageUtils.showToastMessage(this@CRMDetailsActivity, showErrorCodeToast(response.code()))
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                        override fun onFailure(call: Call<StatusModel>?, t: Throwable?) {
                            MessageUtils.dismissDialog(dialog)
                            if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                                MessageUtils.showToastMessage(this@CRMDetailsActivity, t?.message)
                            }
                        }
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        bs_add_event_cancel?.setOnClickListener {
            /*if (bottomDheetDialog != null) {
                bottomDheetDialog?.dismiss()*/
            // add_new_task.visibility = View.VISIBLE
            bs_date?.setText("")
            bs_time?.setText("")
            //bs_task?.setText("")
            bs_remarks?.setText("")
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)

            //}
        }

        bs_date?.setOnClickListener {
            var datesStr = showDateDailog()
            Log.d("datesStr", "" + datesStr)
            //bs_date?.setText(datesStr)
        }

        bs_time?.setOnClickListener {
            val calendar = Calendar.getInstance()

            val d = TimePickerDialog(this, getTimeFromPicker(this, bs_time!!), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false)
            d.window.attributes.windowAnimations = R.style.grow
            d.show()

        }

        snack_view.setOnClickListener {
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        }

        take_image.setOnClickListener {

            if (isReadLocationAllowed()) {

                //MessageUtils.showToastMessage(this@CRMDetailsActivity, "Open Camera")
                var dir =/* "" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +*/ "/sdcard/Ampere_Service/"
                var newdir = File(dir)
                newdir.mkdirs()
                var file = dir + DateUtils.getCurrentDate(DateUtils.DATE_DDMMYYYYHHMMAP) + ".jpg"
                var newfile = File(file)
                try {
                    newfile.createNewFile()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                outputFileUri = Uri.fromFile(newfile)
                Log.d("outputFileUriClicke", "" + outputFileUri)

                var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                startActivityForResult(cameraIntent, TAKE_PHOTO_CODE)
            } else {
                requestLocationPermission()
            }
        }

        add_new_task.setOnClickListener {
            //snack_view.setBackgroundColor(resources.getColor(R.color.white_un_select))
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)
        }
    }

    /**
     * Get Information of Components
     *
     */
    private fun onGetServiceComponents(strServiceType: String) {
        var map: HashMap<String, String> = HashMap()
        map.put("serviceType", strServiceType)
        var serviceComponents: Call<ModelServiceComponets> = MyUtils.getRetroService().onGetServiceComponents(BEARER + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN), map)

        var dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
        dialog.setOnKeyListener { dialogInterface, _, keyEvent ->
            serviceComponents.cancel()
            MessageUtils.dismissDialog(dialog)
            false
        }

        serviceComponents.enqueue(object : Callback<ModelServiceComponets> {
            override fun onFailure(call: Call<ModelServiceComponets>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                var msg = MessageUtils.showFailureToast(t!!.message)
                MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, msg)

            }

            override fun onResponse(call: Call<ModelServiceComponets>, response: Response<ModelServiceComponets>) {
                MessageUtils.dismissDialog(dialog)
                if (response?.isSuccessful) {


                    var models = response.body()
                } else {
                    var msg = MessageUtils.showErrorCodeToast(response?.code())
                    MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, msg)

                }
            }
        })
    }

    /* private fun onLoadAsynTask(dateStr: String, timeStr: String, taskStr: String, remarksStr: String) {
         var dialog: Dialog? = null
         var result: String = ""

       */
    class AddNextTask(var crmActivity: CRMDetailsActivity, var dateStr: String, var timeStr: String, var taskStr: String, var remarksStr: String, var enquiry_status: String
                      , var outputFileUri: String, var chassisNumberStr: String, var oldSerialNo: String, var newSerialNo: String) : AsyncTask<String, Void, String>() {

        //var dialog: Dialog? = null
        var result: String = ""


        companion object {
            var refreshViews: RefreshViews? = null

            fun refresh(refreshViews: RefreshViews) {
                this.refreshViews = refreshViews
            }
        }

        override fun onPreExecute() {
            super.onPreExecute()
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
                dialog = MessageUtils.showDialog(crmActivity)
            }

            dialog?.setOnKeyListener { dialogInterface, _, keyEvent ->
                dialogInterface.toString()
                addNextTask?.cancel(true)
                false
            }
        }


        override fun doInBackground(vararg p0: String?): String {
            var url = RetrofitCall.BASE_URL + "updatetask"
            Log.d("vdsfdfd", "" + url)
            var httpclient = DefaultHttpClient()
            var httppost = HttpPost("" + url)
            var multipartEntity = MultipartEntity(HttpMultipartMode.STRICT)
            var localContext = BasicHttpContext()
            try {
                // Log.d("imageFilePath", "" + imageFilePath)

                multipartEntity.addPart("user_name", StringBody(LocalSession.getUserInfo(crmActivity, LocalSession.KEY_USER_NAME)))
                multipartEntity.addPart("next_activity_date", StringBody(dateStr))
                multipartEntity.addPart("next_activity_time", StringBody(timeStr))
                multipartEntity.addPart("enquiry_status", StringBody(enquiry_status))
                multipartEntity.addPart("remarks", StringBody(remarksStr))
                multipartEntity.addPart("enquiry_id", StringBody(enquiryId))
                multipartEntity.addPart("nexttask", StringBody(taskStr))
                multipartEntity.addPart("enquirytaskid", StringBody(childId))

                multipartEntity.addPart("chassis_number", StringBody(chassisNumberStr))
                multipartEntity.addPart("old_serial_number", StringBody(oldSerialNo))
                multipartEntity.addPart("new_serial_number", StringBody(newSerialNo))

                Log.d("enquiry_status", "" + enquiry_status)
                if (enquiry_status.equals("Closure")) {
                    var enquiry = mEnqquiry_for?.text.toString()
                    if (enquiry.equals("Service")) {
                        outputFileUri = outputFileUri.replace("file:///", "/")
                        Log.d("outputFileUri_replace", "" + outputFileUri)
                        multipartEntity.addPart("upload", FileBody(File(outputFileUri)))
                        multipartEntity.addPart("imagename", StringBody("http://13.232.130.235:8000/public/uploads/" + File(outputFileUri).getName()))
                    }
                }
                var totalSize = multipartEntity.getContentLength()
                Log.d("totalSize", "" + totalSize)
                httppost.setEntity(multipartEntity)
                var response = httpclient.execute(httppost, localContext)
                var r_entity = response.getEntity()
                var inputStream = r_entity.getContent()
                result = convertStreamToString(inputStream)
                Log.d("resultsdsd", "" + result)
                return result


            } catch (e: UnsupportedEncodingException) {
                return "fileNotFound"
            } catch (e: FileNotFoundException) {
                // MessageUtils.showToastMessage(crmActivity, e.message)
                e.printStackTrace()
            }

            return result
        }

        @SuppressLint("SetTextI18n")
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            MessageUtils.dismissDialog(dialog)

            try {
                File(outputFileUri).delete()
                var gson = Gson()
                var models = gson.fromJson(result, TaskDetailsModel::class.java)

                if (models.success) {
                    if (models.data != null) {
                        crmActivity.setList(models)
                        if (refreshViews != null) {
                            refreshViews?.refresh(models)
                        }

                        Log.d("task_details ", "enquiry_status " + enquiry_status + "  " + taskStr + "  task_name  " + task_name)

                        if (task_name.equals(crmActivity.getString(R.string.sales))) {
                            if (enquiry_status.equals(crmActivity.getString(R.string.closure))) {
                                if (taskStr.equals("Win")) {

                                    var dialogInvoice = Dialog(crmActivity)
                                    dialogInvoice.setContentView(R.layout.dialog_invoice)
                                    dialogInvoice.show()
                                    dialogInvoice.setCancelable(false)

                                    var vehicle_model: CustomTextEditView = dialogInvoice.findViewById(R.id.vehicle_model)
                                    var model_varent: CustomTextEditView = dialogInvoice.model_varent
                                    var chassis_number: CustomTextEditView = dialogInvoice.chassis_number
                                    var invoice_amt: CustomTextEditView = dialogInvoice.invoice_amt
                                    var customer_name: CustomTextEditView = dialogInvoice.customer_name
                                    var customer_address: CustomTextEditView = dialogInvoice.customer_address
                                    var customer_proof: CustomTextEditView = dialogInvoice.customer_proof
                                    var customer_address_proof: CustomTextEditView = dialogInvoice.customer_address_proof
                                    var customer_email_id: CustomTextEditView = dialogInvoice.customer_email_id
                                    var customer_mobile_no: CustomTextEditView = dialogInvoice.customer_mobile_no
                                    var invoice_cancel: CustomButton = dialogInvoice.invoice_cancel
                                    var invoice_submit: CustomButton = dialogInvoice.invoice_submit
                                    var mSnackView: NestedScrollView = dialogInvoice.mSnackView
                                    var model_color: CustomTextEditView = dialogInvoice.vehicle_model_color


                                    var strVehicle: String = mVehicleModel!!.text.toString().trim()
                                    val strEmail: String = mEmailId?.text.toString()
                                    val strName: String = mName?.text.toString()
                                    val mobileNo: String = mMobileNo?.text.toString().trim()
                                    val modelColor: String = model_color.text.toString().trim()
                                    val strValues: String = mValues?.text.toString().trim().replace("₹ ", "")

                                    Log.d("strVehicle", "strVehicle  " + strVehicle + " strEmail " + strEmail +
                                            "  strName  " + strName + " mobileNo " + mobileNo + " strValues " + strValues)
                                    vehicle_model.setText(strVehicle)
                                    customer_email_id.setText(strEmail)
                                    customer_name.setText(strName)
                                    customer_mobile_no.setText(mobileNo)
                                    invoice_amt.setText(strValues)

                                    invoice_submit.setOnClickListener {
                                        var vehileModel: String = vehicle_model.text.toString().trim()
                                        var modelVarent: String = model_varent.text.toString().trim()
                                        var chassisNumber: String = chassis_number.text.toString().trim()
                                        var invoiceAmt: String = invoice_amt.text.toString().trim()
                                        var custmerName: String = customer_name.text.toString().trim()
                                        var customerMobileNumber: String = customer_mobile_no.text.toString().trim()
                                        var customerEmail: String = customer_email_id.text.toString().trim()
                                        var customerAdd: String = customer_address.text.toString().trim()
                                        var customerProof: String = customer_proof.text.toString().trim()
                                        var customerAddProof: String = customer_address_proof.text.toString().trim()

                                        if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, vehileModel, "Vehicle Model can't be empty")) {
                                            if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, modelVarent, "Model Vareint can't be empty")) {
                                                if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, chassisNumber, "Chassis Number can't be empty")) {
                                                    if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, modelColor, "Model Color can't be empty")) {

                                                        if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, invoiceAmt, "Invoice Amount can't be empty")) {

                                                            if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, custmerName, "Customer Name can't be empty")) {
                                                                if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerMobileNumber, "Customer Mmobile Number can't be empty")) {
                                                                    if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerEmail, "Customer Email Id can't be empty")) {
                                                                        if (Pattern.matches(pattern, customerEmail)) {
                                                                            if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerAdd, "Customer Address can't be empty")) {

                                                                                if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerProof, "Customer Proof can't be empty")) {
                                                                                    if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerAddProof, "Customer Address Proof can't be empty")) {
                                                                                        var map = HashMap<String, String>()
                                                                                        map.put("vehicle_model", vehileModel)
                                                                                        map.put("model_varient", modelVarent)
                                                                                        map.put("chassis_no", chassisNumber)
                                                                                        map.put("invoice_amount", invoiceAmt)
                                                                                        map.put("customer_name", custmerName)
                                                                                        map.put("mobile", customerMobileNumber)
                                                                                        map.put("email_id", customerEmail)
                                                                                        map.put("customer_address", customerAdd)
                                                                                        map.put("id_proof", customerProof)
                                                                                        map.put("address_proof", customerAddProof)
                                                                                        map.put("enquiry_id", enquiryId)
                                                                                        map.put("modelColor", modelColor)
                                                                                        map.put("username", LocalSession.getUserInfo(crmActivity, LocalSession.KEY_ID))

                                                                                        onAddInVoice(map, dialogInvoice)

                                                                                    }
                                                                                }
                                                                            }
                                                                        } else {
                                                                            MessageUtils.showSnackBar(crmActivity, mSnackView, "Enter valid email id")
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    invoice_cancel.setOnClickListener {

                                        dialogInvoice.dismiss()
                                    }


                                }
                            }
                        }
                    }
                }


            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private fun onAddInVoice(map: HashMap<String, String>, dialogInvoice: Dialog) {
            var addInvoice: Call<AddInvoice> = MyUtils.getRetroService().onAddInvoice(BEARER + LocalSession.getUserInfo(crmActivity, KEY_TOKEN), map)
            addInvoice.enqueue(object : Callback<AddInvoice> {
                override fun onResponse(call: Call<AddInvoice>?, response: Response<AddInvoice>?) {
                    if (response != null) {
                        dialogInvoice.dismiss()
                        if (response.isSuccessful) {
                            var addInvoic: AddInvoice = response.body() as AddInvoice
                            MessageUtils.showToastMessage(crmActivity, addInvoic!!.message)
                            var bundle = Bundle()
                            var fragment = ViewEventDetailed()
                            fragment.arguments = bundle
                            val intent = Intent(crmActivity, CRMActivity::class.java)
                            /*intent.putExtra("id", "" + enquiryId)
                            intent.putExtra("user_name", "" + items[position].customerName)
                            intent.putExtra("task_name", "" + context.getString(R.string.sales))*/
                            ContextCompat.startActivity(crmActivity, intent, bundle)
                            crmActivity.overridePendingTransition(0, 0)
                            crmActivity.finish()
                        } else {
                            var msg: String = MessageUtils.showErrorCodeToast(response.code())
                            MessageUtils.showToastMessage(crmActivity, msg)
                        }
                    }
                }

                override fun onFailure(call: Call<AddInvoice>?, t: Throwable?) {
                    dialogInvoice.dismiss()
                    var msg: String = MessageUtils.showFailureToast(t?.message)
                    MessageUtils.showToastMessage(crmActivity, msg)
                }
            })
        }

    }
    //}


    private fun onAddNextTask(part: MultipartBody.Part, next_activity_date: RequestBody, next_activity_time: RequestBody, enquiry_status: RequestBody, remarks: RequestBody, user_name: RequestBody, enquiry_id: RequestBody, enquirytaskid: RequestBody, nexttask: RequestBody, imageName: RequestBody) {

        var model: Call<TaskDetailsModel> = retrofit.onAddNextTask(BEARER + LocalSession.getUserInfo(this, KEY_TOKEN), part, next_activity_date, next_activity_time, enquiry_status, remarks, user_name, enquiry_id, enquirytaskid, nexttask, imageName)
        dialog = MessageUtils.showDialog(this)
        dialog?.setOnKeyListener { dialogInterface, _, keyEvent ->
            dialogInterface.toString()
            model.cancel()
            false
        }

        model.enqueue(object : Callback<TaskDetailsModel> {
            override fun onResponse(call: Call<TaskDetailsModel>, response: Response<TaskDetailsModel>) {
                MessageUtils.dismissDialog(dialog)
                if (response.isSuccessful) {
                    var valuesOfModel = response.body()
                    if (valuesOfModel != null) {
                        if (valuesOfModel.success) {
                            if (valuesOfModel.data.enquirydetails.size == 0) {
                                task_no_data.visibility = View.VISIBLE
                                ev_lis_view.visibility = View.GONE
                            } else {
                                task_no_data.visibility = View.GONE
                                ev_lis_view.visibility = View.VISIBLE


                                ev_lis_view.adapter = ItemForTaskUpdate(this@CRMDetailsActivity, bottomSheetBehavior as BottomSheetBehavior<*>, valuesOfModel.data.enquirydetails as MutableList<EnquiryDetails>, "")
                                MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, valuesOfModel.message)
                            }
                        } else {
                            MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, valuesOfModel.message)
                        }
                    } else {
                        MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, getString(R.string.api_response_not_found))
                    }

                } else {
                    MessageUtils.setErrorKotlin(this@CRMDetailsActivity, snack_view, response.code())
                }
            }

            override fun onFailure(call: Call<TaskDetailsModel>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                MessageUtils.setFailureKotlin(this@CRMDetailsActivity, snack_view, t.message)
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onTaskLoading(taskModel: Call<TaskDetailsModel>) {
        if (NetworkTester.isNetworkAvailable(this@CRMDetailsActivity)) {
            dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
            dialog?.setOnKeyListener { dialogInterface, _, keyEvent ->
                dialogInterface.dismiss()
                taskModel.cancel()
                finish()
                false
            }

            taskModel.clone().enqueue(object : Callback<TaskDetailsModel> {
                @RequiresApi(Build.VERSION_CODES.M)
                override fun onResponse(call: Call<TaskDetailsModel>, response: Response<TaskDetailsModel>) {
                    MessageUtils.dismissDialog(dialog)
                    if (response.isSuccessful) {
                        models = response.body()
                        viewRefresh(models)
                    } else {
                        var msg: String = showErrorCodeToast(response.code())
                        txt_no_data_task.text = msg
                        all_view_view_event.visibility = View.GONE
                        add_new_task.visibility = View.GONE
                        appbar_view.visibility = View.GONE
                        txt_no_data_task.visibility = View.VISIBLE
                        MessageUtils.setErrorKotlin(this@CRMDetailsActivity, mSnacView, response.code())

                    }
                }

                override fun onFailure(call: Call<TaskDetailsModel>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    var msg: String = showFailureToast(t.message)
                    txt_no_data_task.text = msg
                    all_view_view_event.visibility = View.GONE
                    appbar_view.visibility = View.GONE
                    add_new_task.visibility = View.GONE
                    txt_no_data_task.visibility = View.VISIBLE
                    //MessageUtils.setFailureKotlin(this@CRMDetailsActivity, mSnacView, t.message)
                }
            })
        } else {
            MessageUtils.snackActionSetting(this@CRMDetailsActivity, mSnacView, getString(R.string.check_internet))
        }
    }

    private fun viewRefresh(models: TaskDetailsModel?) {
        if (models != null) {

            if (models != null) {
                if (models!!.success) {
                    if (models!!.data != null) {
                        if (models!!.data.enquiry != null) {
                            try {
                                all_view_view_event.visibility = View.VISIBLE
                                appbar_view.visibility = View.VISIBLE
                                txt_no_data_task.visibility = View.GONE

                                ItemForTaskUpdate.init(this)

                                if (MyUtils.nullPointerValidation(models.data.enquiry.message).equals("-")) {
                                    ev_details_message.visibility = View.GONE
                                } else {
                                    ev_details_message.visibility = View.VISIBLE
                                }

                                if (MyUtils.nullPointerValidation(models.data.enquiry.sparesName).equals("-")) {
                                    ev_details_spares.visibility = View.GONE
                                } else {
                                    ev_details_spares.visibility = View.VISIBLE
                                }

                                if (MyUtils.nullPointerValidation(models.data.enquiry.task).equals("-")) {
                                    ev_details_task.visibility = View.GONE
                                } else {
                                    ev_details_task.visibility = View.VISIBLE
                                }

                                mName?.text = "  " + models!!.data.enquiry.customerName
                                CustomTextView.marquee(mName)
                                ev_details_city.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.city)
                                ev_details_vehicle_model.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.vehicleModel)
                                ev_details_chassis_no.text = if (models!!.data.enquiry.assignedDetails != null) "  " + models!!.data.enquiry.assignedDetails.name else ""
                                mEmailId?.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.email)
                                mValues?.text = "  " + getString(R.string.inr) + " " + MyUtils.nullPointerValidation(models!!.data.enquiry.invoice_value)
                                CustomTextView.marquee(ev_details_task)
                                CustomTextView.marquee(mEmailId)
                                CustomTextView.marquee(ev_details_message)

                                mMobileNo?.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.mobileNo)
                                ev_details_spares.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.sparesName)
                                ev_lead_source.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.leadsource)
                                ev_details_task.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.task)
                                ev_details_message.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.message)
                                ev_task_details.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.taskStatus)

                                mEnqquiry_for?.text = models!!.data.enquiry.enquiryFor
                                if (models.data.enquiry.enquiryDate != null) {


                                    try {
                                        ev_details_enquiry_date.text = "  " + DateUtils.convertDates(models!!.data.enquiry.enquiryDate, DateUtils.CURRENT_FORMAT, DateUtils.DD_MM) + "  " + DateUtils.convertDates(models!!.data.enquiry.enquiryTime, "HH:mm:ss", "hh:mm a")
                                    } catch (e: RuntimeException) {
                                        e.printStackTrace()
                                    }
                                }
                                if (models.data.enquiry.assignedDate != null) {

                                    ev_details_enquiry_assigned_date.text = "  " + DateUtils.convertDates(models!!.data.enquiry.assignedDate, DateUtils.CURRENT_FORMAT + " HH:mm:ss", DateUtils.DD_MM + " hh:mm a")
                                }

                                val taskItem: List<EnquiryDetails> = models.data.enquirydetails as List<EnquiryDetails>
                                if (taskItem.size == 0) {
                                    task_no_data.visibility = View.VISIBLE
                                    ev_lis_view.visibility = View.GONE
                                } else {
                                    task_no_data.visibility = View.GONE
                                    ev_lis_view.visibility = View.VISIBLE
                                    ev_lis_view.adapter = ItemForTaskUpdate(this@CRMDetailsActivity, bottomSheetBehavior as BottomSheetBehavior<*>, taskItem as MutableList<EnquiryDetails>, MyUtils.nullPointerValidation(models.data.enquiry.taskStatus))
                                }

                                val taskArray = ArrayList<String>()
                                for (task in models!!.data.tasks) {
                                    taskArray.add(task.task)
                                }

                                bs_task_spinner?.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, android.R.layout.simple_list_item_1, android.R.id.text1, taskArray)
                                //bs_task_spinner?.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, taskArray)
                                statusValues.clear()
                                //bs_status_spinner.adapter = ItemBottomSheetStatusUpdate(this@CRMDetailsActivity, models!!.data.enquirystatuslist)
                                for (enquiry_list in models.data.enquirystatuslist) {
                                    statusValues.add(enquiry_list.status)
                                }

                                //This adaper is Next Update Status from Bottomsheet
                                bs_status_spinner.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, android.R.layout.simple_list_item_1, android.R.id.text1, statusValues)
                                mSwipeRefreshLayout?.setRefreshing(false)

                                if (models.data.enquiry.taskStatus.equals("SS4")) {
                                    add_new_task.visibility = View.GONE


                                    /*var task = bs_task_spinner.selectedItem.toString()
                                    var status = bs_status_spinner.selectedItem.toString()

                                    bs_status.setText(status)
                                    bs_task.setText(task)
                                    bs_task.visibility = View.VISIBLE
                                    bs_status.visibility = View.VISIBLE

                                    bs_task_spinner.visibility = View.GONE
                                    bs_status_spinner.visibility = View.GONE
                                    layout_action_date.visibility = View.GONE
                                    bs_btn_lay.visibility = View.GONE


                                    bs_status.setTextColor(getColor(R.color.black))
                                    bs_task.setTextColor(getColor(R.color.black))*/


                                } else {
                                    add_new_task.visibility = View.VISIBLE
                                    layout_action_date.visibility = View.VISIBLE
                                }


                                Log.d("modelServiceComponets", "" + models?.data?.modelServiceComponets?.size)
                                var componetCount: Int = 0
                                if (models?.data?.modelServiceComponets != null)
                                    componetCount = models?.data?.modelServiceComponets?.size!!
                                else
                                    componetCount = 0

                                if (componetCount != 0) {
                                    serviceComponents = models?.data?.modelServiceComponets
                                }
                                /*var adapter = ArrayAdapter<ModelServiceComponets>(this@CRMDetailsActivity, R.layout.item_dialog, R.id.text1, serviceComponents)
                                adapter.setDropDownViewResource(R.layout.item_dialog)
                                mSpSeviceComponent?.adapter = adapter*/

                                mSpSeviceComponent?.adapter = MyClassAdapter(this@CRMDetailsActivity, serviceComponents)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                Log.d("exceptionMesg", "" + ex.message)
                            }
                        } else {
                            MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, "Enquiry Details not found")
                        }


                        Log.d("what_comes_here", "" + models.data.enquiryStatusList.size + "  sparesList  " + models.data.sparesList.size + "  spareHistory " + models.data.spareHistory.size)
                        if (models.data.sparesList.size != 0) {
                            mSpareListView!!.visibility = View.VISIBLE
                            mSpareList!!.adapter = ItemSpares(this@CRMDetailsActivity, models.data.sparesList)
                        } else {
                            mSpareListView!!.visibility = View.GONE
                        }
                        if (models.data.enquiryStatusList.size != 0) {
                            mEnquiryStatusView!!.visibility = View.VISIBLE
                            Log.d("enquiry_details_before", "" + models.data.enquiryStatusList.size)

                            /*modelTasks.data.enquiryStatusList.addAll(modelTasks.data.enquiryStatusList)
                            Log.d("enquiry_details_mi", "" + modelTasks.data.enquiryStatusList.size)
                            modelTasks.data.enquiryStatusList.addAll(modelTasks.data.enquiryStatusList)
                            Log.d("enquiry_details_after", "" + modelTasks.data.enquiryStatusList.size)*/



                            mEnquiryStatusList!!.adapter = ItemEnquiry(this@CRMDetailsActivity, models.data.enquiryStatusList)
                            /*mSpareListView!!.visibility = View.VISIBLE
                            mSpareList!!.adapter = ItemEnquiry(this@CRMDetailsActivity, modelTasks.data.enquiryStatusList)*/
                        } else {
                            mEnquiryStatusView!!.visibility = View.GONE
                        }

                    } else {
                        MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, getString(R.string.api_response_not_found))
                    }
                } else {
                    MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, models!!.message)
                }
            }
        } else {
            MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, getString(R.string.api_response_not_found))
        }
    }


    private fun showDateDailog(): String {
        var date = ""
        val datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { datePicker, selectedYear, selectedMonth, selectedDate ->
            year = selectedYear
            month = selectedMonth
            day = selectedDate
            val dateFormatter = SimpleDateFormat(DateUtils.DD_MM_YYYY, Locale.US)
            val newDate = Calendar.getInstance()
            newDate.set(year, month, day)

            date = dateFormatter.format(newDate.time)
            bs_date?.setText(date)

            bs_time?.setText("12:00 PM")
            Log.d("daasaste", "" + date)


        }, year, month, day)


        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.window.attributes.windowAnimations = R.style.grow
        datePickerDialog.show()
        Log.d("daasaste", "return  " + date)
        return date
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == TAKE_PHOTO_CODE) {

                if (resultCode == Activity.RESULT_OK) {

                    if (outputFileUri != null) {
                        var files = File(outputFileUri.toString())
                        Log.d("outputFileUri", "" + File("" + outputFileUri).name)
                        bs_image_name.text = files.name
                        //file:/storage/emulated/0/Pictures/Ampere_Service/280620180548PM.jpg

                        // Get content resolver.
                        var contentResolver: ContentResolver = getContentResolver()
                        // Use the content resolver to open camera taken image input stream through image uri.
                        var inputStream: InputStream = contentResolver.openInputStream(outputFileUri)
                        // Decode the image input stream to a bitmap use BitmapFactory.
                        var pictureBitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
                        // Set the camera taken image bitmap in the image view component to display.
                        take_image.setImageBitmap(pictureBitmap)
                    }

                }
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    private fun getTimeFromPicker(Enquiry: AppCompatActivity, mTime: CustomTextEditView): TimePickerDialog.OnTimeSetListener {
        return TimePickerDialog.OnTimeSetListener { view, hourOfDay_, minute ->
            var hourOfDay = hourOfDay_

            Log.d("TimefromTimePicker", "$view $hourOfDay  $minute")

            var AM_PM = " AM"
            var mm_precede = ""
            val hourOfDayPlusTwo = hourOfDay + 2
            //if (hourOfDay >= 9 && 22 > hourOfDay) {
            if (hourOfDay >= 12) {
                AM_PM = " PM"
                if (hourOfDay >= 13 && hourOfDay < 24) {
                    hourOfDay -= 12
                } else {
                    hourOfDay = 12
                }
            } else if (hourOfDay == 0) {
                hourOfDay = 12
            }
            if (minute < 10) {
                mm_precede = "0"
            }
            mTime.setText("$hourOfDay:$mm_precede$minute$AM_PM")

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun setList(valuesOfModel: TaskDetailsModel?) {

        if (valuesOfModel != null) {
            if (valuesOfModel.success) {
                if (valuesOfModel.data.enquirydetails.size == 0) {
                    task_no_data.visibility = View.VISIBLE
                    ev_lis_view.visibility = View.GONE
                } else {
                    task_no_data.visibility = View.GONE
                    ev_lis_view.visibility = View.VISIBLE

                    ItemForTaskUpdate.init(this)


                    ev_lis_view.adapter = ItemForTaskUpdate(this@CRMDetailsActivity, bottomSheetBehavior as BottomSheetBehavior<*>, valuesOfModel.data.enquirydetails as MutableList<EnquiryDetails>, "")
                    bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
                    bs_date?.setText("")
                    bs_time?.setText("")
                    //bs_task?.setText("")
                    bs_remarks?.setText("")
                    MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, valuesOfModel.message)
                }

            } else {
                MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, valuesOfModel.message)
            }
        } else {
            MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, getString(R.string.api_response_not_found))
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this@CRMDetailsActivity, Manifest.permission.CAMERA)) {
            Toast.makeText(this@CRMDetailsActivity, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show()
        }
        requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), LOCATION_PERMISSION_CODE)
    }

    private fun isReadLocationAllowed(): Boolean {
        val result = ContextCompat.checkSelfPermission(this@CRMDetailsActivity, Manifest.permission.CAMERA)
        val result1 = ContextCompat.checkSelfPermission(this@CRMDetailsActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        //val CALL_PHONE = ContextCompat.checkSelfPermission(this@CRMDetailsActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
        return if (result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED) {
            return true
        } else false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        //Checking the request code of our request
        if (requestCode == LOCATION_PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                //Displaying another toast if permission is not granted
            }
        }
    }

    class ItemBottomSheetStatusUpdate(val crmDetailsActivity: CRMDetailsActivity, val enquiryStatusList: MutableList<EnquiryStatusList>) : BaseAdapter() {
        val mInflater: LayoutInflater = LayoutInflater.from(crmDetailsActivity)

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {

            val view: View?
            var textViewItemName: TextView? = null
            if (convertView == null) {
                view = mInflater.inflate(R.layout.item_dialog, parent, false)
                textViewItemName = view!!.findViewById(R.id.text1) as TextView

            } else {
                view = convertView

            }
            textViewItemName!!.setText(enquiryStatusList.get(position).status)
            textViewItemName.setOnClickListener {
                Log.d("aslklskds", "" + enquiryStatusList.get(position).statusHint)

            }

            return view
        }


        override fun getItem(p0: Int): Any {
            return enquiryStatusList.size
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return enquiryStatusList.size
        }
    }

    class ItemEnquiry(val crmDetailsActivity: CRMDetailsActivity, val enquiryStatusList: MutableList<Data.EnquiryStatus>) : RecyclerView.Adapter<HoldeEnquiry>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoldeEnquiry {
            Log.d("enquiry_details_two", "" + enquiryStatusList.size)
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_enquiry_status_list, parent, false)
            return HoldeEnquiry(v)
        }

        override fun getItemCount(): Int {
            return enquiryStatusList.size
        }

        override fun onBindViewHolder(holder: HoldeEnquiry, position: Int) {
            try {
                Log.d("compleetedBy", "" + enquiryStatusList[position] + "\n" + enquiryStatusList[position].completed_by + "  at" + enquiryStatusList[position].completed_at + "no")
                holder.itemSiNo.text = "" + (position + 1)
                holder.itemCompletedBy.text = MyUtils.nullPointerValidation(enquiryStatusList[position].completed_by)
                holder.itemStatus.text = MyUtils.nullPointerValidation(enquiryStatusList[position].status)
                var dateStr: String = MyUtils.nullPointerValidation(enquiryStatusList[position].completed_at)
                if (dateStr != "-") {
                    dateStr = DateUtils.convertDates(dateStr, "yyyy-MM-dd hh:mm:ss", "dd/MM hh:mm:ss")
                }
                holder.itemCompletedDate.text = dateStr

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    class HoldeEnquiry(view: View) : RecyclerView.ViewHolder(view) {
        val itemCompletedBy = view.item_enquiry_eompleted_by
        val itemStatus = view.item_enquiry_name
        val itemSiNo = view.item_enquiry_si_no
        val itemCompletedDate = view.item_enquiry_completed_date

    }


    class ItemSpares(val crmDetailsActivity: CRMDetailsActivity, val enquiryStatusList: MutableList<Data.Spares>) : RecyclerView.Adapter<HolderSpares>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderSpares {
            Log.d("enquiry_details_two", "" + enquiryStatusList.size)
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_spare_list, parent, false)
            return HolderSpares(v)
        }

        override fun getItemCount(): Int {
            return enquiryStatusList.size
        }

        override fun onBindViewHolder(holder: HolderSpares, position: Int) {
            holder.itemSiNo.text = "" + (position + 1)
            holder.itemSpareName.text = MyUtils.nullPointerValidation(enquiryStatusList[position].spare_name)
            holder.itemCount.text = MyUtils.nullPointerValidation(enquiryStatusList[position].qty)


        }

    }

    class HolderSpares(view: View) : RecyclerView.ViewHolder(view) {
        val itemSpareName = view.item_spare_name
        val itemCount = view.item_spare_count
        val itemSiNo = view.item_spare_si_no


    }


    class MyClassAdapter(val context: FragmentActivity, val items: List<ModelServiceComponets>?) : BaseAdapter() {
        override fun getView(p0: Int, v: View?, p2: ViewGroup?): View {
            val dayView: MyTextView_Lato_Bold
            val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            var v = vi.inflate(R.layout.item_dialog, null)
            dayView = v!!.findViewById(R.id.text1)
            var id: MyTextView_Lato_Bold = v!!.findViewById(R.id.id_s)
            dayView.text = items!![p0].componentname
            id.text = "" + items!![p0].componentid
            return v
        }

        override fun getItem(p0: Int): Any {
            return p0
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return items!!.size
        }

    }

    override fun updateIDAndRemarkDetails(enquiryDetailsList: MutableList<EnquiryDetails>) {
        Log.d("enquiryDetailsList", "" + enquiryDetailsList.size)
        this.enquiryDetailsList = enquiryDetailsList
        Log.d("enquiryDetailsList_af", "" + enquiryDetailsList.size)
    }
}

