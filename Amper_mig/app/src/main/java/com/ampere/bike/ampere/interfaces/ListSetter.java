package com.ampere.bike.ampere.interfaces;

public interface ListSetter<G> {

    void setList(G object);
}
