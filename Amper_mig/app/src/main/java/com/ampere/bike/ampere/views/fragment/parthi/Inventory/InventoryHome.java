package com.ampere.bike.ampere.views.fragment.parthi.Inventory;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.SpareStock;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock.VehicleStock;
import com.ampere.vehicles.R;

public class InventoryHome extends Fragment {
    private InventoryTapAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * inside of fragment  using tap layout and  viewpager  you can use this
         * lines
         * setRetainInstance(true);
         */
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_inventory_home,container,false);

        viewPager =  view.findViewById(R.id.viewPager);
        tabLayout =view.  findViewById(R.id.tabLayout);
        setupViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.v("Selected Tab",""+tab.getText());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.v("unSelected Tab",""+tab.getText());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.v("RESelected Tab",""+tab.getText());
            }
        });
        /*view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
                    i.putExtra("SELECTEDVALUE", 0);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);
                    Objects.requireNonNull(getActivity()).finish();

                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return view;
    }
/// Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        /**
         * get tab adapter working with activity
         */
//        adapter = new InventoryTapAdapter(Objects.requireNonNull(getActivity()).getSupportFragmentManager());
        /**
         * get Inside of Fragment  again using fragment
         */
        adapter = new InventoryTapAdapter(getChildFragmentManager());
        adapter.addFragment(new VehicleStock(),"Vehicle Stock");
        adapter.addFragment(new SpareStock(),"Spare Stock");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
