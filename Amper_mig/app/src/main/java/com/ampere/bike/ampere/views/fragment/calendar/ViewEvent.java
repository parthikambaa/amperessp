package com.ampere.bike.ampere.views.fragment.calendar;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.RetrofitService;
import com.ampere.bike.ampere.model.calendar.EnquirydetailsItem;
import com.ampere.bike.ampere.model.calendar.ModelDateSelection;
import com.ampere.bike.ampere.utils.DateUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.kotlin.item.ItemsViewEvent;
import com.ampere.vehicles.R;

import java.text.ParseException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customviews.CustomTextView;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_CONTACTS;
import static com.ampere.bike.ampere.views.fragment.calendar.CustomeCalendarView.modelCalendar;
import static com.ampere.bike.ampere.views.fragment.enquiry.Enquiry.LOCATION_PERMISSION_CODE;


public class ViewEvent extends Fragment {

    private Unbinder unbinder;
    @BindView(R.id.list_proposal)
    RecyclerView mRecyclerView;
    @BindView(R.id.no_text)
    CustomTextView mNoData;

    @BindView(R.id.crm_header_name)
    CustomTextView headerName;
    @BindView(R.id.crm_header_date)
    CustomTextView headerDate;
    @BindView(R.id.snack_View)
    LinearLayout snack_View;
    @BindView(R.id.crm_header)
    LinearLayout mCrmHeader;

    private String mEventDate = "", eventName = "";

    public static RetrofitService retrofitService = MyUtils.getRetroService();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Bundle bundle = getArguments();
        mEventDate = bundle.getString("date");
        eventName = bundle.getString("eventName");
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(mEventDate);

        return inflater.inflate(R.layout.frag_list_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        retrofitService = MyUtils.getRetroService();
        mRecyclerView.setHasFixedSize(true);
        /*
        headerDate.setVisibility(View.GONE);
        headerName.setVisibility(View.VISIBLE);*/


        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        if (modelCalendar != null && modelCalendar.enquirydetails != null && modelCalendar.enquirydetails.enquirydetails != null) {
            ArrayList<ModelDateSelection> modelDateSelections = new ArrayList<>();
            for (EnquirydetailsItem enquirydetails : modelCalendar.enquirydetails.enquirydetails) {
                Log.d("New_datess", "" + enquirydetails.assignedDate + "  " + mEventDate);
                try {
                    if (DateUtils.convertDates(enquirydetails.assignedDate, DateUtils.DATE_YYYY_MM_DD_HH_MM_SS, DateUtils.YYYY_MM_DD).equals(mEventDate)) {
                        //9176098983
                        ModelDateSelection modelDateSelection = new ModelDateSelection("" + enquirydetails.id, enquirydetails.customerName, enquirydetails.mobileNo, enquirydetails.city,
                                enquirydetails.email, enquirydetails.enquiryDate, enquirydetails.enquiryTime, enquirydetails.leadsource, enquirydetails.enquiryFor, enquirydetails.vehicleModel,
                                enquirydetails.chassisNo, enquirydetails.message, enquirydetails.assignedTo, enquirydetails.assignedPerson, enquirydetails.assignedDate, enquirydetails.task, enquirydetails.taskStatus,
                                enquirydetails.createdBy, enquirydetails.updatedAt, false);
                        modelDateSelections.add(modelDateSelection);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            if (modelDateSelections.size() >= 1) {
                mNoData.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mRecyclerView.setAdapter(new ItemsViewEvent(getActivity(), modelDateSelections));

            } else {
                mNoData.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_CALL_LOG)) {
            // Toast.makeText(getActivity(), "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        }
        requestPermissions(new String[]{Manifest.permission.READ_CALL_LOG, READ_CONTACTS, CALL_PHONE}, LOCATION_PERMISSION_CODE);
    }

    private boolean isReadLocationAllowed() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALL_LOG);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), READ_CONTACTS);
        if (result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request

        if (requestCode == LOCATION_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //requestLocationPermission();
            } else {
                requestLocationPermission();
                //Displaying another toast if permission is not granted

            }
        }
    }


/*
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted && cameraAccepted)
                        Snackbar.make(snack_View, "Permission Granted, Now you can access contacts and phone calls.", Snackbar.LENGTH_LONG).show();
                    else {

                        Snackbar.make(snack_View, "Permission Denied, You cannot access location and phone calls.", Snackbar.LENGTH_LONG).show();

                        MessageUtils.showSnackBarAction(getActivity(), snack_View, "");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(READ_CALL_LOG)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{READ_CALL_LOG, READ_CONTACTS},
                                                            LOCATION_PERMISSION_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }*/

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", okListener)
                //.setNegativeButton("Cancel", null)
                .create()
                .show();
    }

}
