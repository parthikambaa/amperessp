package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentsItemPojos {
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("indent_id")
	@Expose
	private String indentId;
	@SerializedName("indent_no")
	@Expose
	private String indentNo;
	@SerializedName("request_to")
	@Expose
	private Integer requestTo;
	@SerializedName("request_by")
	@Expose
	private Integer requestBy;
	@SerializedName("request_date")
	@Expose
	private String requestDate;
	@SerializedName("total_qty")
	@Expose
	private String totalQty;
	@SerializedName("delivered_qty")
	@Expose
	private String deliveredQty;
	@SerializedName("pending_qty")
	@Expose
	private String pendingQty;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("remarks")
	@Expose
	private String remarks;
	@SerializedName("approved_by")
	@Expose
	private String approvedBy;
	@SerializedName("approved_date")
	@Expose
	private String approvedDate;
	@SerializedName("closed_by")
	@Expose
	private String closedBy;
	@SerializedName("closed_date")
	@Expose
	private Object closedDate;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("updated_at")
	@Expose
	private String updatedAt;
	@SerializedName("requestby")
	@Expose
	private Requestby requestby;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIndentId() {
		return indentId;
	}

	public void setIndentId(String indentId) {
		this.indentId = indentId;
	}

	public String getIndentNo() {
		return indentNo;
	}

	public void setIndentNo(String indentNo) {
		this.indentNo = indentNo;
	}

	public Integer getRequestTo() {
		return requestTo;
	}

	public void setRequestTo(Integer requestTo) {
		this.requestTo = requestTo;
	}

	public Integer getRequestBy() {
		return requestBy;
	}

	public void setRequestBy(Integer requestBy) {
		this.requestBy = requestBy;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(String totalQty) {
		this.totalQty = totalQty;
	}

	public String getDeliveredQty() {
		return deliveredQty;
	}

	public void setDeliveredQty(String deliveredQty) {
		this.deliveredQty = deliveredQty;
	}

	public String getPendingQty() {
		return pendingQty;
	}

	public void setPendingQty(String pendingQty) {
		this.pendingQty = pendingQty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}

	public Object getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(Object closedDate) {
		this.closedDate = closedDate;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Requestby getRequestby() {
		return requestby;
	}

	public void setRequestby(Requestby requestby) {
		this.requestby = requestby;
	}
}
