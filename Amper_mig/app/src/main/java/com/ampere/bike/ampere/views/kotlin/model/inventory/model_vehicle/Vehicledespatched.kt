package com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle

data class Vehicledespatched(
        val id: Int,
        val vehicle_model: String,
        val model_varient: String,
        val color: String,
        val chassis_no: String,
        //val spares_serial_no: String,
        val qty: String,
        val indent_invoice_date: String,
        val manufacture_date: String,
        val status: String,
        val acceptance: String,
        val user_id: Int,
        val delivered_by: Int,
        val created_at: String,
        val updated_at: String,
        var isCheck: Boolean
)