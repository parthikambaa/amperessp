package com.ampere.bike.ampere.views.fragment.enquiry.calllog;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ampere.bike.ampere.utils.CallLogDate;
import com.ampere.vehicles.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.ampere.bike.ampere.utils.MyUtils.isSelect;

/**
 * Created by AND I5 on 04-10-2017.
 */
public class ItemCallLog extends RecyclerView.Adapter<HolderCallLog> {
    private final ArrayList<HashMap<String, String>> mItemValues;
    private final FragmentActivity mContext;

    public ItemCallLog(FragmentActivity mainActivity, ArrayList<HashMap<String, String>> callLogLis) {
        this.mContext = mainActivity;
        this.mItemValues = callLogLis;
    }

    @Override
    public HolderCallLog onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HolderCallLog(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_call_log, parent, false));
    }

    @Override
    public int getItemCount() {
        return mItemValues.size();
    }

    @Override
    public void onBindViewHolder(final HolderCallLog holder, final int position) {
        String contactName = mItemValues.get(position).get("contactName");
        // Log.d("contactName", "" + contactName);
        holder.mCallName.setText(contactName);
        /*  if (contactName == null) {*/
        holder.mNumber.setText(mItemValues.get(position).get("phNumber"));
        /*} else {
            holder.mNumber.setText(contactName);
        }*/
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (position == 0) {
            lp.setMargins(16, 16, 16, 8);
        } else if (position == mItemValues.size() - 1) {
            lp.setMargins(16, 8, 16, 16);
        } else {
            lp.setMargins(16, 8, 16, 8);
        }
        holder.mDate.setText(mItemValues.get(position).get("callDayTime"));


        holder.mLayout.setLayoutParams(lp);
        /*SimpleDateFormat spf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = spf.parse(mItemValues.get(position).get("callDayTime"));
            spf = new SimpleDateFormat("dd-MM-yyyy");
            String date = spf.format(newDate);

            spf = new SimpleDateFormat("hh:mm a");
            String time = spf.format(newDate);

            time = time.replace("p.m.", "PM");

            holder.mDate.setText(mItemValues.get(position).get("callDayTime"));

        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        int milliseconds = Integer.parseInt(mItemValues.get(position).get("callDuration"));


        ///long seconds = milliseconds / 1000;
        String secondsStr = "", minutesStr = "", hoursStr = "";
        long seconds = milliseconds % 60;
        long minutes = milliseconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        secondsStr = String.valueOf(milliseconds % 60);
        minutesStr = String.valueOf(minutes % 60);
        hoursStr = String.valueOf(hours % 24);

        if (hours < 10) {
            hoursStr = "0" + hours;
        }
        if (minutes < 10) {
            minutesStr = "0" + minutes;
        }
        if (seconds < 10) {
            secondsStr = "0" + seconds;
        }
        String time = hoursStr + ":" + minutesStr + ":" + secondsStr;

        if (milliseconds >= 0) {
            holder.mDuration.setText(time);
        } else {
            holder.mDuration.setText("");
        }

        String dir = mItemValues.get(position).get("dir");

        if ("OUTGOING".equals(dir)) {
            holder.mDir.setImageResource(R.drawable.ic_call_made);
        } else if ("INCOMING".equals(dir)) {
            holder.mDir.setImageResource(R.drawable.ic_call_received);
        } else if ("MISSED".equals(dir)) {
            holder.mDir.setImageResource(R.drawable.ic_call_missed);
        }
        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "" + mItemValues.get(position).get("phNumber"), Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent();
                intent.putExtra("number", "" + mItemValues.get(position).get("phNumber"));
                intent.putExtra("date", "" + holder.mDate.getText().toString());
                intent.putExtra("duration", "" + holder.mDuration.getText().toString());*/

                String mobileNumber = mItemValues.get(position).get("phNumber");
                CallLogDate.NUMBER = mobileNumber.replace("+91", "");

                SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
                Date newDate = null;

                try {
                    newDate = spf.parse(holder.mDate.getText().toString());
                    spf = new SimpleDateFormat("dd-MM-yyyy");
                    CallLogDate.DATE = spf.format(newDate);
                    spf = new SimpleDateFormat("hh:mm a");
                    String tmePM = spf.format(newDate);
                    tmePM = tmePM.replace("p.m.", "PM");
                    CallLogDate.TIME = tmePM;

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                isSelect = true;
                CallLogDate.DURATION = holder.mDuration.getText().toString();
                CallLogDate.CONTACT_NAME = holder.mCallName.getText().toString();
                mContext.finish();
                /*mContext.setResult(RESULT_OK, intent);
                 */
            }
        });
    }

}
