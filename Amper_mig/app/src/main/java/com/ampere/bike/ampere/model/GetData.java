
package com.ampere.bike.ampere.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GetData {

    @SerializedName("success")
    private boolean mSuccess;
    @SerializedName("message")
    private String message;


}
