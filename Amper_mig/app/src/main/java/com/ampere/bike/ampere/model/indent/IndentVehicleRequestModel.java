package com.ampere.bike.ampere.model.indent;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class IndentVehicleRequestModel {

    @SerializedName("data")
    public List<DataItem> data;

    @SerializedName("users")
    public List<UserIntentModel> users;

    @SerializedName("success")
    public boolean success;

    @SerializedName("message")
    public String message;

    @Override
    public String toString() {
        return
                "IndentVehicleRequestModel{" +
                        "data = '" + data + '\'' +
                        ",success = '" + success + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }
}