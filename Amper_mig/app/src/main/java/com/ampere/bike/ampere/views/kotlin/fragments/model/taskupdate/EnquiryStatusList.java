
package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EnquiryStatusList {

    @SerializedName("created_at")
    private Object mCreatedAt;
    @SerializedName("id")
    private Long mId;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("updated_at")
    private Object mUpdatedAt;

    @SerializedName("statusname")
    private String statusname;

    public Object getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Object createdAt) {
        mCreatedAt = createdAt;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        mUpdatedAt = updatedAt;
    }
    public void setStatusHint(String statusname) {
        statusname = statusname;
    }

    public String getStatusHint() {
        return statusname;
    }
}
