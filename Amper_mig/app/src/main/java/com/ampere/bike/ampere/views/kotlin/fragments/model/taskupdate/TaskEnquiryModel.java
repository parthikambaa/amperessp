package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.SerializedName;


public class TaskEnquiryModel {

    @SerializedName("created_time")
    public String createdTime;

    @SerializedName("task_status")
    public String taskStatus;

    @SerializedName("assigned_date")
    public String assignedDate;

    @SerializedName("city")
    public String city;

    @SerializedName("mobile_no")
    public String mobileNo;

    @SerializedName("vehicle_model")
    public String vehicleModel;

    @SerializedName("created_at")
    public String createdAt;

    @SerializedName("message")
    public String message;

    @SerializedName("created_by")
    public String createdBy;

    @SerializedName("enquiry_time")
    public String enquiryTime;

    @SerializedName("task")
    public String task;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("spares_id")
    public String sparesId;

    @SerializedName("chassis_no")
    public String chassisNo;

    @SerializedName("enquiry_date")
    public String enquiryDate;

    @SerializedName("enquiry_for")
    public String enquiryFor;

    @SerializedName("leadsource")
    public String leadsource;

    @SerializedName("id")
    public int id;

    @SerializedName("customer_name")
    public String customerName;

    @SerializedName("spares_name")
    public String sparesName;

    @SerializedName("email")
    public String email;

    @SerializedName("assigned_to")
    public String assignedTo;

    @SerializedName("assigned_person")
    public String assignedPerson;
    @SerializedName("invoice_value")
    public String invoice_value;


    @SerializedName("assignedperson")
    public AssignedPerson assignedDetails;

    @Override
    public String toString() {
        return
                "TaskEnquiryModel{" +
                        "created_time = '" + createdTime + '\'' +
                        ",task_status = '" + taskStatus + '\'' +
                        ",assigned_date = '" + assignedDate + '\'' +
                        ",city = '" + city + '\'' +
                        ",mobile_no = '" + mobileNo + '\'' +
                        ",vehicle_model = '" + vehicleModel + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",message = '" + message + '\'' +
                        ",created_by = '" + createdBy + '\'' +
                        ",enquiry_time = '" + enquiryTime + '\'' +
                        ",task = '" + task + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",spares_id = '" + sparesId + '\'' +
                        ",chassis_no = '" + chassisNo + '\'' +
                        ",enquiry_date = '" + enquiryDate + '\'' +
                        ",enquiry_for = '" + enquiryFor + '\'' +
                        ",leadsource = '" + leadsource + '\'' +
                        ",id = '" + id + '\'' +
                        ",customer_name = '" + customerName + '\'' +
                        ",spares_name = '" + sparesName + '\'' +
                        ",email = '" + email + '\'' +
                        ",assigned_to = '" + assignedTo + '\'' +
                        ",assigned_person = '" + assignedPerson + '\'' +
                        "}";
    }
}