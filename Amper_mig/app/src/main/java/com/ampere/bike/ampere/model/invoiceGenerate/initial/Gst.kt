package com.ampere.bike.ampere.model.invoiceGenerate.initial

data class Gst(
        val id: Int,
        val sgst: String,
        val cgst: String,
        val igst: String,
        val subsidy_percentage: String,
        val terms_conditions: String,
        val created_at: Any,
        val updated_at: String
)