package com.ampere.bike.ampere.views.fragment.parthi.Warranty;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.WarrantyComplaintsFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyDefects.WarrantyDefectsFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.WarrantyInvoiceFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyServiceReturn.WarrantyServiceReturnFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WrrantyTabAdapter.WarrantyTabAdapter;
import com.ampere.vehicles.R;


public class WarrantyHome extends AppCompatActivity {
    private WarrantyTabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warranty_home);
        viewPager =  findViewById(R.id.viewPager);
        tabLayout =  findViewById(R.id.tabLayout);
        adapter = new WarrantyTabAdapter(getSupportFragmentManager());
        adapter.addFragment(new WarrantyInvoiceFragment(),"Invoice");
        adapter.addFragment(new WarrantyComplaintsFragment(),"Complaints");
        adapter.addFragment(new WarrantyServiceReturnFragment(),"ServiceReturn");
        adapter.addFragment(new WarrantyDefectsFragment(),"Defects");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.v("Selected Tab",""+tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.v("unSelected Tab",""+tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.v("RESelected Tab",""+tab);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.viewPager);
        System.out.println("  when click back press function"+fragment.toString());
//       startActivity(new Intent(this, CustomNavigationDuo.class).setAction(String.valueOf(Intent.FLAG_ACTIVITY_CLEAR_TASK)).setAction(String.valueOf(Intent.FLAG_ACTIVITY_CLEAR_TOP)).putExtra("SELECTEDVALUE", 0));
        finish();

    }
}
