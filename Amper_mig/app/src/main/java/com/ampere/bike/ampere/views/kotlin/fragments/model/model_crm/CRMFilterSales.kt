package com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm

class CRMFilterSales(val id: String?, val customerName: String?, val city: String?, val mobileNo: String?, val email: String?, val leadsource: String?,
                     val assignedDate: String?, val assignedTo: String?, val enquiryDate: String?, val enquiryFor: String?, val enquiryTime: String?,
                     val assignedPerson: String?, val message: String?, val taskStatus: String?, val task: String?, val vehicleModel: String?, val chassisNo: String, val isChecked: Boolean)


/*        this.createdBy = createdBy;
        this.updatedAt = updatedAt;


public String getCreatedBy() {
        return createdBy;
    }
 public String getUpdatedAt() {
        return updatedAt;
    }        */
