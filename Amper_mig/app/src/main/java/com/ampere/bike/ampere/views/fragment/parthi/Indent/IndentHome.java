package com.ampere.bike.ampere.views.fragment.parthi.Indent;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentListFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentTapAdapter.IndentTapAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.SpareDispatch.SpareDispatch;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.VehileDispatch.VehileDispatch;
import com.ampere.vehicles.R;


public class IndentHome extends AppCompatActivity {
    private IndentTapAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indent_home);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        adapter = new IndentTapAdapter(getSupportFragmentManager());
        adapter.addFragment(new IndentListFragment(), "IndentList");
        adapter.addFragment(new VehileDispatch(), "Vehicle  Dispatched");
        adapter.addFragment(new SpareDispatch(), "Spare Dispatched");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.v("Selected Tab", "" + tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.v("unSelected Tab", "" + tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.v("RESelected Tab", "" + tab);
            }
        });

        FloatingActionButton floatingActionButton = findViewById(R.id.add_indent);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    //  getSupportFragmentManager().beginTransaction().replace(R.id.call_frgment, new Indent()).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
