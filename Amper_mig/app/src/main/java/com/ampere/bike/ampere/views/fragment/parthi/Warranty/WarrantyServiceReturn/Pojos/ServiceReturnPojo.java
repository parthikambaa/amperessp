package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyServiceReturn.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceReturnPojo {

        @SerializedName("chassis_no")
        @Expose
        private String chassisNo;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("send_through")
        @Expose
        private String sendThrough;
        @SerializedName("docket")
        @Expose
        private String docket;
        @SerializedName("vide")
        @Expose
        private String vide;
        @SerializedName("driver_no")
        @Expose
        private String driverNo;
        @SerializedName("driver_name")
        @Expose
        private String driverName;
        @SerializedName("sent_date")
        @Expose
        private String sentDate;
        @SerializedName("defect_service_type")
        @Expose
        private String defectServiceType;
        @SerializedName("defect_serial_no")
        @Expose
        private String defectSerialNo;
        @SerializedName("defect_item")
        @Expose
        private String defectItem;
        @SerializedName("defect_qty")
        @Expose
        private String defectQty;

        public String getChassisNo() {
            return chassisNo;
        }

        public void setChassisNo(String chassisNo) {
            this.chassisNo = chassisNo;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getSendThrough() {
            return sendThrough;
        }

        public void setSendThrough(String sendThrough) {
            this.sendThrough = sendThrough;
        }

        public String getDocket() {
            return docket;
        }

        public void setDocket(String docket) {
            this.docket = docket;
        }

        public String getVide() {
            return vide;
        }

        public void setVide(String vide) {
            this.vide = vide;
        }

        public String getDriverNo() {
            return driverNo;
        }

        public void setDriverNo(String driverNo) {
            this.driverNo = driverNo;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getSentDate() {
            return sentDate;
        }

        public void setSentDate(String sentDate) {
            this.sentDate = sentDate;
        }

        public String getDefectServiceType() {
            return defectServiceType;
        }

        public void setDefectServiceType(String defectServiceType) {
            this.defectServiceType = defectServiceType;
        }

        public String getDefectSerialNo() {
            return defectSerialNo;
        }

        public void setDefectSerialNo(String defectSerialNo) {
            this.defectSerialNo = defectSerialNo;
        }

        public String getDefectItem() {
            return defectItem;
        }

        public void setDefectItem(String defectItem) {
            this.defectItem = defectItem;
        }

        public String getDefectQty() {
            return defectQty;
        }

        public void setDefectQty(String defectQty) {
            this.defectQty = defectQty;
        }

        boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
