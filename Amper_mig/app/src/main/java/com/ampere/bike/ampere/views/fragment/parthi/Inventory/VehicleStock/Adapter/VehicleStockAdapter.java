package com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock.Adapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock.Pojos.VehicleStockPojo;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class VehicleStockAdapter extends RecyclerView.Adapter<VehicleStockAdapter.ViewHolder> {
    ArrayList<VehicleStockPojo> vehicleStockPojosArrayList;
    Context context;

    public VehicleStockAdapter(ArrayList<VehicleStockPojo> vehicleStockPojosArrayList, Context context) {
        this.vehicleStockPojosArrayList = vehicleStockPojosArrayList;
        this.context = context;
    }

    public  void setList(  ArrayList<VehicleStockPojo> vehicleStockPojosArrayList){
        this.vehicleStockPojosArrayList = vehicleStockPojosArrayList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_inventory_vehicle_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.srlNo.setText(String.valueOf(position+1));
        holder.vehicleID.setText(String.valueOf(vehicleStockPojosArrayList.get(position).getId()));
        holder.vehicleVariant.setText(vehicleStockPojosArrayList.get(position).getModelVarient());
        holder.vehicleSerialNumber.setText(vehicleStockPojosArrayList.get(position).getBatterySerialNo());
        holder.vehicleInvoiceDate.setText(vehicleStockPojosArrayList.get(position).getIndentInvoiceDate());
        holder.vehicleStatus.setText(vehicleStockPojosArrayList.get(position).getStatus());

    }

    @Override
    public int getItemCount() {
        return vehicleStockPojosArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView srlNo, vehicleID, vehicleSerialNumber, vehicleInvoiceDate, vehicleStatus, vehicleVariant;
        public ViewHolder(View itemView) {
            super(itemView);
            srlNo =itemView.findViewById(R.id.sERNUM);
            vehicleID =itemView.findViewById(R.id.vehicle_id);
            vehicleSerialNumber =itemView.findViewById(R.id.vehicle_serl_num);
            vehicleInvoiceDate = itemView.findViewById(R.id.vehicle_invoice_date);
            vehicleStatus =itemView.findViewById(R.id.vehicle_status);
            vehicleVariant =itemView.findViewById(R.id.vehicle_model);
        }
    }
}

