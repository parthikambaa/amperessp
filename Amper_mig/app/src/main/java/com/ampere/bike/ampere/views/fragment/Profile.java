package com.ampere.bike.ampere.views.fragment;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.RetrofitService;
import com.ampere.bike.ampere.model.profile.ModelProfile;
import com.ampere.bike.ampere.model.profile.ModelUpdateProfile;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.vehicles.R;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import customviews.CustomTextEditView;
import customviews.CustomTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ampere.bike.ampere.shared.LocalSession.BEARER;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_NAME;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;


public class Profile extends Fragment {
    Unbinder unbinder;

    @BindView(R.id.mSnakView)
    ScrollView mSnakView;

    @BindView(R.id.profile_main)
    ConstraintLayout mProfileMainLayout;

    @BindViews({R.id.profile_layout, R.id.status_layout})
    List<LinearLayout> mProfileLayout;

    @BindViews({R.id.pro_name, R.id.pro_address, R.id.pro_mobile, R.id.pro_email})
    List<CustomTextEditView> mProfileInfo;

    @BindViews({R.id.pro_proposal, R.id.pro_negotiation, R.id.pro_closure})
    List<CustomTextView> mProposalStatus;

    @BindView(R.id.pro_edit)
    FloatingActionButton mEditFAB;

    @BindView(R.id.model_text_name)
    CustomTextView model_text_name;

    private boolean isEditProfile = false;
    private Dialog dialog;
    private String crtName = "", crtEmail = "", crtMobile = "", crtAddress = "";
    private RetrofitService retrofitService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(getString(R.string.nav_profile));
        return inflater.inflate(R.layout.frag_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        retrofitService = MyUtils.getInstance();

        dialog = MessageUtils.showDialog(getActivity());
        retrofitService.onLoadProfile(BEARER+LocalSession.getUserInfo(getActivity(),KEY_TOKEN),LocalSession.getUserInfo(getActivity(), KEY_NAME)).enqueue(new Callback<ModelProfile>() {
            @Override
            public void onResponse(Call<ModelProfile> call, Response<ModelProfile> response) {
                MessageUtils.dismissDialog(dialog);
                if (response.isSuccessful()) {
                    ModelProfile modelProfile = response.body();
                } else {
                    MessageUtils.setErrorMessage(getActivity(), mSnakView, response.code());
                }
            }

            @Override
            public void onFailure(Call<ModelProfile> call, Throwable t) {
                MessageUtils.dismissDialog(dialog);
                MessageUtils.setFailureMessage(getActivity(), mSnakView, t.getMessage());
            }
        });

        mProfileLayout.get(0).setVisibility(View.VISIBLE);
        mProfileLayout.get(1).setVisibility(View.VISIBLE);

        mProfileInfo.get(0).setText("Subbu ");
        mProfileInfo.get(1).setText("Erode ");
        mProfileInfo.get(2).setText("9788645210 ");
        mProfileInfo.get(3).setText("subbu@gmail.com ");

        mProposalStatus.get(0).setText("546");
        mProposalStatus.get(1).setText("506");
        mProposalStatus.get(2).setText("40");

        model_text_name.setTypeface(MyUtils.getTypeface(getActivity(), "permanent_marker.ttf"));
        model_text_name.setText("JOHN WADE".toUpperCase());
        model_text_name.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        model_text_name.setSingleLine(true);
        model_text_name.setSelected(true);

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @OnClick(R.id.pro_edit)
    public void onEditClick() {

        // Enable Edit mode
        if (!isEditProfile) {
            isEditProfile = true;
            mEditFAB.setImageResource(R.drawable.ic_done);

            disableTouchMode(true);
            mProfileInfo.get(0).setFocusableInTouchMode(true);
            mProfileInfo.get(1).setFocusableInTouchMode(true);
            mProfileInfo.get(2).setFocusableInTouchMode(true);
            mProfileInfo.get(3).setFocusableInTouchMode(true);

            mProfileInfo.get(1).setSelection(mProfileInfo.get(0).getText().toString().length());
            mProfileInfo.get(1).requestFocus();

            crtName = mProfileInfo.get(0).getText().toString().trim();
            crtAddress = mProfileInfo.get(1).getText().toString().trim();
            crtMobile = mProfileInfo.get(2).getText().toString().trim();
            crtEmail = mProfileInfo.get(3).getText().toString().trim();

        }
        // disable edit mode
        else {
            String crName = "", crEmail = "", crMobile = "", crAddress = "";

            crName = mProfileInfo.get(0).getText().toString().trim();
            crAddress = mProfileInfo.get(1).getText().toString().trim();
            crMobile = mProfileInfo.get(2).getText().toString().trim();
            crEmail = mProfileInfo.get(3).getText().toString().trim();

            if (!crName.isEmpty() && !crAddress.isEmpty() && !crMobile.isEmpty() && !crEmail.isEmpty()) {
                if (crtName.equals(crName) && crtAddress.equals(crAddress) && crtMobile.equals(crMobile) && crtEmail.equals(crEmail)) {
                   // MessageUtils.showSnackBar(getActivity(), mSnakView, "No Changes");
                    mEditFAB.setImageResource(R.drawable.ic_edit);
                    disableTouchMode(false);
                } else {
                    mEditFAB.setImageResource(R.drawable.ic_edit);
                    disableTouchMode(false);

                    //MessageUtils.showSnackBar(getActivity(), mSnakView, "Changes is there. waiting API");

                    dialog = MessageUtils.showDialog(getActivity());

                    retrofitService.onUpdateProfile(BEARER+LocalSession.getUserInfo(getActivity(),KEY_TOKEN),crName, crAddress, crMobile, crEmail).enqueue(new Callback<ModelUpdateProfile>() {
                        @Override
                        public void onResponse(Call<ModelUpdateProfile> call, Response<ModelUpdateProfile> response) {
                            MessageUtils.dismissDialog(dialog);
                            if (response.isSuccessful()) {
                                ModelUpdateProfile modelUpdateProfile = response.body();
                            } else {
                                MessageUtils.setErrorMessage(getActivity(), mSnakView, response.code());
                            }
                        }

                        @Override
                        public void onFailure(Call<ModelUpdateProfile> call, Throwable t) {
                            MessageUtils.dismissDialog(dialog);
                            MessageUtils.setFailureMessage(getActivity(), mSnakView, t.getMessage());

                        }
                    });
                }
            } else {
                MessageUtils.showSnackBar(getActivity(), mSnakView, "All fields are filling up to Change");
            }
        }

    }

    private void disableTouchMode(boolean isFocus) {
        isEditProfile = isFocus;

        mProfileInfo.get(0).setFocusableInTouchMode(isFocus);
        mProfileInfo.get(1).setFocusableInTouchMode(isFocus);
        mProfileInfo.get(2).setFocusableInTouchMode(isFocus);
        mProfileInfo.get(3).setFocusableInTouchMode(isFocus);

        mProfileInfo.get(0).setFocusable(isFocus);
        mProfileInfo.get(1).setFocusable(isFocus);
        mProfileInfo.get(2).setFocusable(isFocus);
        mProfileInfo.get(3).setFocusable(isFocus);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
