package com.ampere.bike.ampere.interfaces;

import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.EnquiryDetails;

import java.util.List;

public interface RemarksAndIDUpdate {
    void updateIDAndRemarkDetails(List<EnquiryDetails> enquiryDetailsList);
}
