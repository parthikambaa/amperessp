package com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle

data class Data(
        val vehicledespatched: ArrayList<Vehicledespatched>,
        val vehicledelivered: ArrayList<Vehicledelivered>
)