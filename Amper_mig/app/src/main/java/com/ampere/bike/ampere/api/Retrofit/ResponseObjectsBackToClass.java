package com.ampere.bike.ampere.api.Retrofit;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public interface ResponseObjectsBackToClass {
    void responseObjects(JSONObject objResponse, String method, int position, String message, boolean success) throws JSONException, IOException;

}
