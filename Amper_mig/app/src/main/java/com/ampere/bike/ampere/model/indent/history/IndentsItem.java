package com.ampere.bike.ampere.model.indent.history;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class IndentsItem implements Serializable {

    @SerializedName("request_by")
    public String requestBy;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("total_qty")
    public String totalQty;

    @SerializedName("request_date")
    public String requestDate;

    @SerializedName("created_at")
    public String createdAt;

    @SerializedName("id")
    public int id;

    @SerializedName("status")
    public String status;


    @SerializedName("requestby")
    public RequestBy requestby;


    public class RequestBy {

        @SerializedName("id")
        public int id;

        @SerializedName("name")
        public String name;

        @SerializedName("username")
        public String username;

        @SerializedName("email")
        public String email;

        @SerializedName("mobile")
        public String mobile;

        @SerializedName("profile_photo")
        public String profile_photo;

        @SerializedName("address")
        public String address;


    }
}