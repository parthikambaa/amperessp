package com.ampere.bike.ampere.views.kotlin.fragments.indent

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.model.indent.history.IndentModelViewDetails
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.item.ViewIndentDetailsItem
import com.ampere.vehicles.R
import customviews.CustomTextView
import kotlinx.android.synthetic.main.frag_indent_details.view.*
import kotlinx.android.synthetic.main.item_crm_header.view.*
import kotlinx.android.synthetic.main.item_indent_details.view.*
import kotlinx.android.synthetic.main.item_indent_info.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IndentDetailsClick : Fragment() {

    var dialog: Dialog? = null
    var mSnackView: LinearLayout? = null
    var mNoData: CustomTextView? = null
    var mLisOfIndent: RecyclerView? = null
    var response1: Response<IndentModelViewDetails>? = null
    var mCRMHeader: LinearLayout? = null
    var mIndentHeader1: LinearLayout? = null
    var mIndentHeader: LinearLayout? = null
    var retrofit = MyUtils.getInstance()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        var bundle: Bundle? = arguments
        var custom_name: String = bundle?.get("custom_name").toString()
        if (LocalSession.getUserInfo(context, LocalSession.KEY_USER_TYPE).equals(LocalSession.T_DEALER)) {
            (activity as CustomNavigationDuo).disbleNavigationView("Indent Details")
        } else {
            (activity as CustomNavigationDuo).disbleNavigationView(custom_name)
        }
        return inflater.inflate(R.layout.frag_indent_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSnackView = view.snack_view
        //mHeader = view.crm_header
        mNoData = view.no_data
        mLisOfIndent = view.list_of_indent
        mCRMHeader = view.crm_header
        mIndentHeader1 = view.in_snack_view
        mIndentHeader = view.indent_header
        mCRMHeader?.visibility = View.GONE
        mIndentHeader?.visibility = View.GONE
        mLisOfIndent?.layoutManager = GridLayoutManager(activity, 1)
        //Log.d("indentDetails", "size_of_serialized " + indentDetails.size + "  " + id)
        loadData(response1)

    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)


        var bundle = arguments
        //var indentDetails = bundle?.getSerializable("indentDetails") as List<IndentdetailsItem>
        var id = bundle?.get("id")
        dialog = MessageUtils.showDialog(activity)
        var map = HashMap<String, String>()
        map.put("id", "" + id)
        var modelIndentDetails: Call<IndentModelViewDetails> = retrofit.onIndentViewDetails(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),map);
        dialog?.setOnKeyListener { dialogInterface, i, keyEvent ->
            dialogInterface.dismiss()
            modelIndentDetails.cancel()
            false
        }

        modelIndentDetails.enqueue(object : Callback<IndentModelViewDetails> {
            override fun onResponse(call: Call<IndentModelViewDetails>, response: Response<IndentModelViewDetails>) {
                MessageUtils.dismissDialog(dialog);
                response1 = response
                loadData(response1)
            }

            override fun onFailure(call: Call<IndentModelViewDetails>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);
                mNoData?.text = MessageUtils.showFailureToast(t?.message)
                mNoData?.visibility = View.VISIBLE
                mIndentHeader1?.visibility = View.GONE
                MessageUtils.setFailureMessage(activity, mSnackView, t?.message)
            }

        })
    }

    private fun loadData(response: Response<IndentModelViewDetails>?) {
        if (response != null) {
            if (response.isSuccessful) {
                var modelIndentViewDetails = response.body()
                if (modelIndentViewDetails != null) {
                    if (modelIndentViewDetails.status) {
                        Log.d("sdsasdad", "" + modelIndentViewDetails.indentdetails.size)

                        if (modelIndentViewDetails.indentdetails.size == 0) {
                            mNoData?.visibility = View.VISIBLE
                            mLisOfIndent?.visibility = View.GONE
                            mIndentHeader1?.visibility = View.GONE

                        } else {
                            mNoData?.visibility = View.GONE
                            mLisOfIndent?.visibility = View.VISIBLE
                            mIndentHeader1?.visibility = View.VISIBLE
                            mLisOfIndent?.adapter = ViewIndentDetailsItem(activity, modelIndentViewDetails.indentdetails)
                        }
                    } else {
                        mIndentHeader1?.visibility = View.GONE
                        mNoData?.visibility = View.VISIBLE
                        MessageUtils.showSnackBar(activity, mSnackView, modelIndentViewDetails.message)
                        mNoData?.text = modelIndentViewDetails.message
                    }
                } else {
                    mIndentHeader1?.visibility = View.GONE
                    mNoData?.visibility = View.VISIBLE
                    mNoData?.text = activity?.getString(R.string.api_response_not_found)
                    MessageUtils.showSnackBar(activity, mSnackView, activity!!.getString(R.string.api_response_not_found))
                }
            } else {
                mIndentHeader1?.visibility = View.GONE
                mNoData?.visibility = View.VISIBLE
                mNoData?.text = MessageUtils.showErrorCodeToast(response.code())
                MessageUtils.setErrorMessage(activity, mSnackView, response.code())
            }
        }
    }
}