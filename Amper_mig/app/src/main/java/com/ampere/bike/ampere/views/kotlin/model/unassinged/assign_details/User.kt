package com.ampere.bike.ampere.views.kotlin.model.unassinged.assign_details

data class User(
        val id: String,
        val name: String,
        val username: String,
        val email: String,
        val mobile: String,
        val profile_photo: String,
        val address: String,
        val user_type: String,
        val created_at: String,
        val updated_at: String
)