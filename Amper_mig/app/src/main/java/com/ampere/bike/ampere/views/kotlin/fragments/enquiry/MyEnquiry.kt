package com.ampere.bike.ampere.views.kotlin.fragments.enquiry

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.model.Data
import com.ampere.bike.ampere.views.kotlin.model.MyEnquiryModel
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.frag_my_enquiry.*
import kotlinx.android.synthetic.main.frag_my_enquiry.view.*
import kotlinx.android.synthetic.main.item_my_enquiry.view.*
import retrofit2.Call
import retrofit2.Response

class MyEnquiry : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView("My Enquiry")
        return inflater.inflate(R.layout.frag_my_enquiry, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadMyEnquiry(view)

    }

    private fun loadMyEnquiry(view: View) {

        var map: HashMap<String, String> = HashMap()
        map.put("username", "" + LocalSession.getUserInfo(activity, LocalSession.KEY_ID))
        var models = MyUtils.getRetroService().onMyEnquiryDetails(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
        var dialog = MessageUtils.showDialog(activity)

        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            models.cancel()
            MessageUtils.dismissDialog(dialog);
            false
        }
        models.clone().enqueue(object : retrofit2.Callback<MyEnquiryModel> {

            override fun onResponse(call: Call<MyEnquiryModel>?, response: Response<MyEnquiryModel>?) {
                MessageUtils.dismissDialog(dialog);

                if (response != null) {
                    if (response.isSuccessful) {
                        val myEnquiryModel = response.body()
                        if (myEnquiryModel != null) {
                            if (myEnquiryModel.success) {
                                view.my_en_list_view.layoutManager = LinearLayoutManager(activity)
                                if (myEnquiryModel.data.size == 0) {
                                    view.my_en_no_data.visibility = View.VISIBLE
                                    view.my_en_list_view.visibility = View.GONE
                                    view.my_en_no_data.text = myEnquiryModel.message
                                } else {
                                    view.my_en_list_view.visibility = View.VISIBLE
                                    view.my_en_no_data.visibility = View.GONE
                                    view.my_en_list_view.adapter = ItemMyEnquiryDetails(activity!!, myEnquiryModel.data)
                                }
                            } else {
                                var msg = MessageUtils.showErrorCodeToast(response.code())
                                //MessageUtils.showSnackBar(activity, invent_snack_view, msg)
                                view.my_en_no_data.text = msg
                                view.my_en_no_data.visibility = View.VISIBLE
                            }
                        }
                    } else {
                        var msg = MessageUtils.showErrorCodeToast(response.code())
                        MessageUtils.showSnackBar(activity, my_en_snack_view, msg)
                        my_en_no_data.text = msg
                        my_en_no_data.visibility = View.VISIBLE
                    }
                }
            }

            override fun onFailure(call: Call<MyEnquiryModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);
                var msg = MessageUtils.showFailureToast(t!!.message)
                MessageUtils.showSnackBar(activity, my_en_snack_view, msg)
                my_en_no_data.text = msg
                my_en_no_data.visibility = View.VISIBLE
            }

        })
    }

    class ItemMyEnquiryDetails(val context: FragmentActivity, val item: List<Data>) : RecyclerView.Adapter<ItemViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {

            return ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_enquiry, parent, false))
        }

        override fun getItemCount(): Int {
            return item.size
        }

        override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
            holder.itemSiNo.text = "" + (position + 1)
            holder.itemCustomerName.text = item[position].customer_name
            holder.itemCustomerMobileNo.text = item[position].mobile_no
            holder.itemVehicleModel.text = item[position].vehicle_model

            holder.itemStatus.text = item[position].task_status
        }

    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var itemSiNo = view.my_en_si_no
        var itemCustomerName = view.my_en_customer_name
        var itemCustomerMobileNo = view.my_en_mobile_no
        var itemVehicleModel = view.my_en_Vehicle_model
        var itemStatus = view.my_en_status
    }
}