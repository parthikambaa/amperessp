package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;


import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.Adapter.WarrantyComplaintsAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.Pojos.Warrantycomplaint;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;


public class WarrantyComplaintsFragment extends Fragment implements ResponsebackToClass,ConfigClickEvent {
    RecyclerView warrantyComplaintsList;
    WarrantyComplaintsAdapter warrantyComplaintsAdapter;
    Spinner spinner;
    String complntId;
    ArrayList<Warrantycomplaint> warrantycomplaintsArrayList ;
    AlertDialog dialog;
    ArrayList<String> sendthrough = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    Common common;
        EditText docket,vide,driverName,drMobileNo;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("Activiy","Complaint");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_warranty_complaints, container, false);
        common = new Common(getActivity(),this);
        sendthrough.clear();
        sendthrough.add("Send Through");
        sendthrough.add("Courier");
        sendthrough.add("Direct");
        warrantyComplaintsList =view.findViewById(R.id.warranty_complaint_list);
        common.showLoad(true);
        HashMap<String ,String> map =new HashMap<>();
//        map.put("userid",sharedPreferences.getString("id",""));sharedPreferences.getvalue(ID);
        if(LocalSession.getUserInfo(getActivity(),KEY_ID)!=null){
           map.put("userid",LocalSession.getUserInfo(getActivity(),KEY_ID));
        }else{
            Log.v("ngsliv4er hg4   Id",LocalSession.getUserInfo(getActivity(),KEY_ID));
        }
        if(common.isNetworkConnected()){
            common.callApiRequest("warrantycomplaints","POST",map,0);
        }else{
            common.hideLoad();
            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
        }
        warrantyComplaintsList.setLayoutManager(new LinearLayoutManager(getActivity()));

        /* view.setFocusableInTouchMode(true);
       view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
//                    // i.putExtra("Comingback", cameback);
                    i.putExtra("SELECTEDVALUE", 0);
                    startActivity(i);
                    return true;
                } else {
                    return false;
                }
            }
        });*/
         return view;
    }


    public  void  popmenu(int configposition){
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_wanty_cmplt_send, null);
        spinner = view.findViewById(R.id.sendthrough);
        final LinearLayout linearLayout = view.findViewById(R.id.courier);
            docket =view.findViewById(R.id.docket);
            vide =view.findViewById(R.id.vide);
            driverName =view.findViewById(R.id.dr_name);
            drMobileNo =view.findViewById(R.id.mob_no);
        final Button sendToPlant = view.findViewById(R.id.snd_plnt);
        final Button close = view.findViewById(R.id.close);
        AlertDialog.Builder alertDialogBuilder =new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        arrayAdapter = new ArrayAdapter<String>(getActivity(),R.layout.warranty_complaint_spin,sendthrough);
        dialog = alertDialogBuilder.create();
        spinner.setAdapter(arrayAdapter);
        dialog.show();

        sendToPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(dialog.isShowing()){
                    dialog.dismiss();
                }else{
                    dialog.dismiss();

                }*/
                sendTOServer();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.isShowing()){
                    dialog.dismiss();

                }else{
                    dialog.dismiss();

                }
                Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String selectstring =spinner.getSelectedItem().toString();
                if(selectstring.equals("Courier")){
                    linearLayout.setVisibility(View.VISIBLE);

                }else{
                    linearLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    public void sendTOServer()
    {
        if(common.isNetworkConnected()){
            common.showLoad(true);
            HashMap<String,String> map = new HashMap<>();
            map.put("userid",LocalSession.getUserInfo(getActivity(),KEY_ID));
            map.put("complaintid",complntId);
            map.put("send_through",spinner.getSelectedItem().toString());
            if(spinner.getSelectedItem().equals("Direct")){
                Log.v("spinnerSelectedItem",""+spinner.getSelectedItem());
            }else{
                if(docket.getText() == null ||  docket.getText().toString().matches("")) {
                    docket.setError("Required");
                }else{
                    if (vide.getText().toString().matches("") || vide.getText() == null) {
                        vide.setError("Required");

                    } else {
                        if (drMobileNo.getText().toString().matches("") ||
                                drMobileNo.getText() == null  || drMobileNo.getText().length() !=10) {
                            drMobileNo.setError("Required");
                        } else {
                            if (driverName.getText().toString().matches("") || driverName.getText() == null) {
                                driverName.setError("Required");
                            } else {
                                map.put("docket", docket.getText().toString());
                                map.put("vide", vide.getText().toString());
                                map.put("Driver_phone_no", drMobileNo.getText().toString());
                                map.put("driver_name", driverName.getText().toString());
                            }
                        }
                    }
                }
            }
            System.out.println("Params" + map);
            common.callApiRequest("sendforservice","POST",map,0);
        }else{
            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();        }
    }

    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message, boolean successs) throws JSONException, IOException {
        common.hideLoad();
        if(objResponse != null){
            if(method.equals("warrantycomplaints")){
                warrantycomplaintsArrayList = new Gson().fromJson(objResponse.toString(), new TypeToken<ArrayList<Warrantycomplaint>>(){}.getType());
                warrantyComplaintsAdapter =new WarrantyComplaintsAdapter(getActivity(),warrantycomplaintsArrayList,this);
                warrantyComplaintsList.setAdapter(warrantyComplaintsAdapter);

            }

        }else if(method.equals("sendforservice")){
            if(successs){
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                if(dialog.isShowing()){
                    dialog.dismiss();
                }else{
                    dialog.dismiss();

                }
                startActivity(new Intent(getActivity(), WarrantyHome.class));
            }else{
                Log.v("info",""+successs);
            }

        }
    }
    @Override
    public void connectposition(int position,int listSize) {
        String status=warrantycomplaintsArrayList.get(position).getStatus();
        String cmpletests=warrantycomplaintsArrayList.get(position).getComplaintstatus();
        String replacement_type=warrantycomplaintsArrayList.get(position).getReplacementType();
        complntId=warrantycomplaintsArrayList.get(position).getId().toString();
        System.out.println("Status:"+status);
        System.out.println("Status complainryt:"+cmpletests);
        System.out.println("Status complainryt replacement_type:"+replacement_type);
        if(replacement_type.equals("Service Replacement") && status.equals("Approved")&& cmpletests.equals("Send to Plant")){
            popmenu(position);
            warrantyComplaintsAdapter.notifyDataSetChanged();
        }else if(replacement_type.equals("")){
          //  popmenu(position);
        }
    }


    public void onBackPressed() {
        if (Objects.requireNonNull(getFragmentManager()).getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            dialog.onBackPressed();
    }
}
