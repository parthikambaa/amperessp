
package com.ampere.bike.ampere.model.warranty;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("warrantyinvoice")
    private List<Warrantyinvoice> mWarrantyinvoice;

    public List<Warrantyinvoice> getWarrantyinvoice() {
        return mWarrantyinvoice;
    }

    public void setWarrantyinvoice(List<Warrantyinvoice> warrantyinvoice) {
        mWarrantyinvoice = warrantyinvoice;
    }

}
